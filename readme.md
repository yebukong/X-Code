## 【X-Code】-  密码管理小程序

### 1.基本功能
+ 基于Microsoft .NET Framework4.0运行

+ 具备简单的用户登录认证,密码修改功能

+ 可以进行基本的密码项操作(增、删、改、查)

+ 支持一些基本功能设置

+ 支持生成随机密码

+ 对本地数据信息进行加密处理


### 2.显示效果
+ 登录  

![登录](http://git.oschina.net/uploads/images/2017/0307/120727_8f851548_884684.png "登录")

+ 展示窗体  

![展示窗体](http://git.oschina.net/uploads/images/2017/0307/115953_28ddf174_884684.png "展示窗体")

+ 新增窗体  

![新增窗体](http://git.oschina.net/uploads/images/2017/0307/120228_15fd749f_884684.jpeg "新增窗体")

+ 修改窗体  

![修改窗体](http://git.oschina.net/uploads/images/2017/0307/120244_9b589e45_884684.png "修改窗体")

+ 密码项显示窗体  

![密码项显示窗体](http://git.oschina.net/uploads/images/2017/0307/120315_90d7d083_884684.png "密码项显示窗体")

+ 密码随机生成

![密码生成](https://images.gitee.com/uploads/images/2019/0627/183529_b65a90be_884684.png "密码随机生成.png")

+ 设置窗体  

![设置窗体](http://git.oschina.net/uploads/images/2017/0307/120330_63a6fbbc_884684.png "设置窗体")

+ 帮助和说明窗体  

![说明](https://images.gitee.com/uploads/images/2019/0627/183508_fe80088d_884684.png "说明.png")

+ 关于  

![关于](http://git.oschina.net/uploads/images/2017/0307/120410_ee57a1f8_884684.png "关于")

+ 程序运行文件清单  

![文件清单](http://git.oschina.net/uploads/images/2017/0307/120426_7ab16886_884684.png "文件清单")

### 3.DMSkin

+ 项目基于DMSkin for WinForm
+ 网址:http://www.dmskin.com/

### 4.示例程序  

+ 见项目附件 [X-Code.7z](https://git.oschina.net/yebukong/X-Code/attach_files)


:alien: 