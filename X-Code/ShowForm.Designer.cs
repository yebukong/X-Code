﻿namespace X_Code
{
    partial class ShowForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            X_Code.BodyItem bodyItem1 = new X_Code.BodyItem();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ShowForm));
            this.codeControl1 = new X_Code.CodeControl();
            this.dmIcon2 = new DMSkin.Controls.DM.DMIcon();
            this.searchTextBox = new System.Windows.Forms.TextBox();
            this.dmIcon1 = new DMSkin.Controls.DM.DMIcon();
            this.setDmIcon = new DMSkin.Controls.DM.DMIcon();
            this.addDmIcon = new DMSkin.Controls.DM.DMIcon();
            this.moreMenuStrip = new System.Windows.Forms.MenuStrip();
            this.moreToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.新增项ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.用户设置ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.随机密码生成StripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.帮助ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.关于ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.updateIcon = new DMSkin.Controls.DM.DMIcon();
            this.infoTip = new System.Windows.Forms.ToolTip(this.components);
            this.dmIcon4 = new DMSkin.Controls.DM.DMIcon();
            this.messagelabel = new System.Windows.Forms.Label();
            this.messageTimer = new System.Windows.Forms.Timer(this.components);
            this.itemContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.查看ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.修改ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.删除ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addButton = new DMSkin.Controls.DMButton();
            this.nameLabel = new System.Windows.Forms.Label();
            this.moreMenuStrip.SuspendLayout();
            this.itemContextMenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // codeControl1
            // 
            this.codeControl1.ArrowColor = System.Drawing.Color.Empty;
            this.codeControl1.BackColor = System.Drawing.Color.Transparent;
            this.codeControl1.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.codeControl1.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.codeControl1.ItemColor = System.Drawing.Color.Empty;
            this.codeControl1.ItemMouseOnColor = System.Drawing.Color.Empty;
            bodyItem1.Bounds = new System.Drawing.Rectangle(0, 0, 2500, 50);
            bodyItem1.Font = new System.Drawing.Font("微软雅黑", 12F);
            bodyItem1.ForeColor = System.Drawing.Color.Black;
            bodyItem1.Height = 50;
            bodyItem1.ID = 0;
            bodyItem1.Key = null;
            bodyItem1.MouseBackColor = System.Drawing.Color.Gray;
            bodyItem1.Num = 1;
            bodyItem1.OwnerChatListBox = this.codeControl1;
            bodyItem1.Text = "加载中···";
            bodyItem1.Width = 2500;
            this.codeControl1.Items.AddRange(new DMSkin.Controls.DMControlItem[] {
            bodyItem1});
            this.codeControl1.Location = new System.Drawing.Point(-2, 80);
            this.codeControl1.Margin = new System.Windows.Forms.Padding(0);
            this.codeControl1.Name = "codeControl1";
            this.codeControl1.ScrollArrowBackColor = System.Drawing.Color.OrangeRed;
            this.codeControl1.ScrollBackColor = System.Drawing.Color.Transparent;
            this.codeControl1.ScrollSliderDefaultColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.codeControl1.ScrollSliderDownColor = System.Drawing.Color.Tomato;
            this.codeControl1.SelectItem = null;
            this.codeControl1.Size = new System.Drawing.Size(652, 345);
            this.codeControl1.SubItemColor = System.Drawing.Color.Empty;
            this.codeControl1.SubItemMouseOnColor = System.Drawing.Color.Empty;
            this.codeControl1.SubItemSelectColor = System.Drawing.Color.Empty;
            this.codeControl1.TabIndex = 0;
            this.codeControl1.Text = "codeControl1";
            this.codeControl1.ItemClick += new System.EventHandler(this.codeControl1_ItemClick);
            // 
            // dmIcon2
            // 
            this.dmIcon2.BackColor = System.Drawing.Color.Transparent;
            this.dmIcon2.DM_Color = System.Drawing.Color.OrangeRed;
            this.dmIcon2.DM_Font_Size = 30F;
            this.dmIcon2.DM_Key = DMSkin.Controls.DM.DMIcon.DMIconKey.computer;
            this.dmIcon2.DM_Text = " X-Code";
            this.dmIcon2.Font = new System.Drawing.Font("隶书", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.dmIcon2.Location = new System.Drawing.Point(6, 29);
            this.dmIcon2.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.dmIcon2.Name = "dmIcon2";
            this.dmIcon2.Size = new System.Drawing.Size(638, 48);
            this.dmIcon2.TabIndex = 7;
            this.dmIcon2.Text = "dmIcon2";
            this.infoTip.SetToolTip(this.dmIcon2, "X-Code");
            // 
            // searchTextBox
            // 
            this.searchTextBox.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.searchTextBox.ForeColor = System.Drawing.Color.Silver;
            this.searchTextBox.Location = new System.Drawing.Point(494, 7);
            this.searchTextBox.Name = "searchTextBox";
            this.searchTextBox.Size = new System.Drawing.Size(85, 21);
            this.searchTextBox.TabIndex = 9;
            this.searchTextBox.Text = "查找";
            this.infoTip.SetToolTip(this.searchTextBox, "输入关键字查找");
            this.searchTextBox.Enter += new System.EventHandler(this.searchTextBox_Enter);
            this.searchTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.searchTextBox_KeyPress);
            this.searchTextBox.Leave += new System.EventHandler(this.searchTextBox_Leave);
            // 
            // dmIcon1
            // 
            this.dmIcon1.BackColor = System.Drawing.Color.White;
            this.dmIcon1.DM_Color = System.Drawing.Color.DimGray;
            this.dmIcon1.DM_Font_Size = 10F;
            this.dmIcon1.DM_Key = DMSkin.Controls.DM.DMIcon.DMIconKey.搜索;
            this.dmIcon1.DM_Text = "";
            this.dmIcon1.Location = new System.Drawing.Point(560, 10);
            this.dmIcon1.Margin = new System.Windows.Forms.Padding(0);
            this.dmIcon1.Name = "dmIcon1";
            this.dmIcon1.Size = new System.Drawing.Size(16, 17);
            this.dmIcon1.TabIndex = 10;
            this.dmIcon1.Text = "dmIcon1";
            this.infoTip.SetToolTip(this.dmIcon1, "点击查询");
            this.dmIcon1.Click += new System.EventHandler(this.dmIcon1_Click);
            this.dmIcon1.MouseEnter += new System.EventHandler(this.dmIcon1_MouseEnter);
            this.dmIcon1.MouseLeave += new System.EventHandler(this.dmIcon1_MouseLeave);
            // 
            // setDmIcon
            // 
            this.setDmIcon.BackColor = System.Drawing.Color.Transparent;
            this.setDmIcon.DM_Color = System.Drawing.Color.Black;
            this.setDmIcon.DM_Font_Size = 12F;
            this.setDmIcon.DM_Key = DMSkin.Controls.DM.DMIcon.DMIconKey.设置;
            this.setDmIcon.DM_Text = "";
            this.setDmIcon.Location = new System.Drawing.Point(465, 8);
            this.setDmIcon.Name = "setDmIcon";
            this.setDmIcon.Size = new System.Drawing.Size(20, 18);
            this.setDmIcon.TabIndex = 11;
            this.setDmIcon.Text = "dmIcon3";
            this.infoTip.SetToolTip(this.setDmIcon, "设置");
            this.setDmIcon.Click += new System.EventHandler(this.setDmIcon_Click);
            this.setDmIcon.MouseEnter += new System.EventHandler(this.setDmIcon_MouseEnter);
            this.setDmIcon.MouseLeave += new System.EventHandler(this.setDmIcon_MouseLeave);
            // 
            // addDmIcon
            // 
            this.addDmIcon.BackColor = System.Drawing.Color.Transparent;
            this.addDmIcon.DM_Color = System.Drawing.Color.Black;
            this.addDmIcon.DM_Font_Size = 12F;
            this.addDmIcon.DM_Key = DMSkin.Controls.DM.DMIcon.DMIconKey.add;
            this.addDmIcon.DM_Text = "";
            this.addDmIcon.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.addDmIcon.Location = new System.Drawing.Point(442, 8);
            this.addDmIcon.Name = "addDmIcon";
            this.addDmIcon.Size = new System.Drawing.Size(20, 18);
            this.addDmIcon.TabIndex = 12;
            this.addDmIcon.Text = "addDmIcon";
            this.infoTip.SetToolTip(this.addDmIcon, "新增");
            this.addDmIcon.Click += new System.EventHandler(this.addDmIcon_Click);
            this.addDmIcon.MouseEnter += new System.EventHandler(this.addDmIcon_MouseEnter);
            this.addDmIcon.MouseLeave += new System.EventHandler(this.addDmIcon_MouseLeave);
            // 
            // moreMenuStrip
            // 
            this.moreMenuStrip.BackColor = System.Drawing.Color.Transparent;
            this.moreMenuStrip.Dock = System.Windows.Forms.DockStyle.None;
            this.moreMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.moreToolStripMenuItem});
            this.moreMenuStrip.Location = new System.Drawing.Point(6, 1);
            this.moreMenuStrip.Name = "moreMenuStrip";
            this.moreMenuStrip.Size = new System.Drawing.Size(35, 25);
            this.moreMenuStrip.TabIndex = 13;
            this.moreMenuStrip.Text = "menuStrip1";
            this.infoTip.SetToolTip(this.moreMenuStrip, "更多");
            // 
            // moreToolStripMenuItem
            // 
            this.moreToolStripMenuItem.BackColor = System.Drawing.Color.Transparent;
            this.moreToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.moreToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.新增项ToolStripMenuItem,
            this.用户设置ToolStripMenuItem,
            this.toolStripSeparator1,
            this.随机密码生成StripMenuItem,
            this.toolStripSeparator2,
            this.帮助ToolStripMenuItem,
            this.关于ToolStripMenuItem});
            this.moreToolStripMenuItem.ForeColor = System.Drawing.Color.DimGray;
            this.moreToolStripMenuItem.Name = "moreToolStripMenuItem";
            this.moreToolStripMenuItem.Padding = new System.Windows.Forms.Padding(0);
            this.moreToolStripMenuItem.Size = new System.Drawing.Size(27, 21);
            this.moreToolStripMenuItem.Text = "•••";
            // 
            // 新增项ToolStripMenuItem
            // 
            this.新增项ToolStripMenuItem.BackColor = System.Drawing.Color.Transparent;
            this.新增项ToolStripMenuItem.Name = "新增项ToolStripMenuItem";
            this.新增项ToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.新增项ToolStripMenuItem.ShowShortcutKeys = false;
            this.新增项ToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.新增项ToolStripMenuItem.Text = "新增项(&N)";
            this.新增项ToolStripMenuItem.Click += new System.EventHandler(this.新增项ToolStripMenuItem_Click);
            // 
            // 用户设置ToolStripMenuItem
            // 
            this.用户设置ToolStripMenuItem.Name = "用户设置ToolStripMenuItem";
            this.用户设置ToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.T)));
            this.用户设置ToolStripMenuItem.ShowShortcutKeys = false;
            this.用户设置ToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.用户设置ToolStripMenuItem.Text = "用户设置(&T)";
            this.用户设置ToolStripMenuItem.Click += new System.EventHandler(this.用户设置ToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(157, 6);
            // 
            // 随机密码生成StripMenuItem
            // 
            this.随机密码生成StripMenuItem.Name = "随机密码生成StripMenuItem";
            this.随机密码生成StripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.M)));
            this.随机密码生成StripMenuItem.ShowShortcutKeys = false;
            this.随机密码生成StripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.随机密码生成StripMenuItem.Text = "随机密码生成(&M)";
            this.随机密码生成StripMenuItem.Click += new System.EventHandler(this.随机密码生成StripMenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(157, 6);
            // 
            // 帮助ToolStripMenuItem
            // 
            this.帮助ToolStripMenuItem.Name = "帮助ToolStripMenuItem";
            this.帮助ToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.H)));
            this.帮助ToolStripMenuItem.ShowShortcutKeys = false;
            this.帮助ToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.帮助ToolStripMenuItem.Text = "说明+帮助(&H)";
            this.帮助ToolStripMenuItem.Click += new System.EventHandler(this.帮助ToolStripMenuItem_Click);
            // 
            // 关于ToolStripMenuItem
            // 
            this.关于ToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.关于ToolStripMenuItem.Name = "关于ToolStripMenuItem";
            this.关于ToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.I)));
            this.关于ToolStripMenuItem.ShowShortcutKeys = false;
            this.关于ToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.关于ToolStripMenuItem.Text = "关于(&I)";
            this.关于ToolStripMenuItem.Click += new System.EventHandler(this.关于ToolStripMenuItem_Click);
            // 
            // updateIcon
            // 
            this.updateIcon.BackColor = System.Drawing.Color.Transparent;
            this.updateIcon.DM_Color = System.Drawing.Color.Black;
            this.updateIcon.DM_Font_Size = 10F;
            this.updateIcon.DM_Key = DMSkin.Controls.DM.DMIcon.DMIconKey.刷新;
            this.updateIcon.DM_Text = "";
            this.updateIcon.Location = new System.Drawing.Point(422, 9);
            this.updateIcon.Name = "updateIcon";
            this.updateIcon.Padding = new System.Windows.Forms.Padding(5);
            this.updateIcon.Size = new System.Drawing.Size(18, 18);
            this.updateIcon.TabIndex = 14;
            this.updateIcon.Text = "dmIcon3";
            this.infoTip.SetToolTip(this.updateIcon, "刷新");
            this.updateIcon.Click += new System.EventHandler(this.updateIcon_Click);
            this.updateIcon.MouseEnter += new System.EventHandler(this.updateIcon_MouseEnter);
            this.updateIcon.MouseLeave += new System.EventHandler(this.updateIcon_MouseLeave);
            // 
            // dmIcon4
            // 
            this.dmIcon4.BackColor = System.Drawing.Color.Transparent;
            this.dmIcon4.DM_Color = System.Drawing.Color.SteelBlue;
            this.dmIcon4.DM_Font_Size = 14F;
            this.dmIcon4.DM_Key = DMSkin.Controls.DM.DMIcon.DMIconKey.用户;
            this.dmIcon4.DM_Text = "";
            this.dmIcon4.Location = new System.Drawing.Point(252, 47);
            this.dmIcon4.Name = "dmIcon4";
            this.dmIcon4.Size = new System.Drawing.Size(22, 18);
            this.dmIcon4.TabIndex = 18;
            this.dmIcon4.Text = "dmIcon4";
            this.infoTip.SetToolTip(this.dmIcon4, "用户名");
            this.dmIcon4.Visible = false;
            // 
            // messagelabel
            // 
            this.messagelabel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.messagelabel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(201)))), ((int)(((byte)(48)))), ((int)(((byte)(44)))));
            this.messagelabel.Font = new System.Drawing.Font("幼圆", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.messagelabel.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.messagelabel.Location = new System.Drawing.Point(201, 168);
            this.messagelabel.Margin = new System.Windows.Forms.Padding(0);
            this.messagelabel.Name = "messagelabel";
            this.messagelabel.Size = new System.Drawing.Size(240, 39);
            this.messagelabel.TabIndex = 15;
            this.messagelabel.Text = "提示信息";
            this.messagelabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.messagelabel.Visible = false;
            // 
            // messageTimer
            // 
            this.messageTimer.Interval = 1000;
            this.messageTimer.Tick += new System.EventHandler(this.messageTimer_Tick);
            // 
            // itemContextMenuStrip
            // 
            this.itemContextMenuStrip.AllowDrop = true;
            this.itemContextMenuStrip.AllowMerge = false;
            this.itemContextMenuStrip.AutoSize = false;
            this.itemContextMenuStrip.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.itemContextMenuStrip.Font = new System.Drawing.Font("幼圆", 12F);
            this.itemContextMenuStrip.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.itemContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.查看ToolStripMenuItem,
            this.修改ToolStripMenuItem,
            this.删除ToolStripMenuItem});
            this.itemContextMenuStrip.Name = "itemContextMenuStrip";
            this.itemContextMenuStrip.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.itemContextMenuStrip.ShowItemToolTips = false;
            this.itemContextMenuStrip.Size = new System.Drawing.Size(85, 83);
            this.itemContextMenuStrip.Closed += new System.Windows.Forms.ToolStripDropDownClosedEventHandler(this.itemContextMenuStrip_Closed);
            // 
            // 查看ToolStripMenuItem
            // 
            this.查看ToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("查看ToolStripMenuItem.Image")));
            this.查看ToolStripMenuItem.Name = "查看ToolStripMenuItem";
            this.查看ToolStripMenuItem.Size = new System.Drawing.Size(112, 26);
            this.查看ToolStripMenuItem.Text = "查看";
            this.查看ToolStripMenuItem.Click += new System.EventHandler(this.查看ToolStripMenuItem_Click);
            // 
            // 修改ToolStripMenuItem
            // 
            this.修改ToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("修改ToolStripMenuItem.Image")));
            this.修改ToolStripMenuItem.Name = "修改ToolStripMenuItem";
            this.修改ToolStripMenuItem.Size = new System.Drawing.Size(112, 26);
            this.修改ToolStripMenuItem.Text = "修改";
            this.修改ToolStripMenuItem.Click += new System.EventHandler(this.修改ToolStripMenuItem_Click);
            // 
            // 删除ToolStripMenuItem
            // 
            this.删除ToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("删除ToolStripMenuItem.Image")));
            this.删除ToolStripMenuItem.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.删除ToolStripMenuItem.Name = "删除ToolStripMenuItem";
            this.删除ToolStripMenuItem.ShortcutKeyDisplayString = "";
            this.删除ToolStripMenuItem.Size = new System.Drawing.Size(112, 26);
            this.删除ToolStripMenuItem.Text = "删除";
            this.删除ToolStripMenuItem.Click += new System.EventHandler(this.删除ToolStripMenuItem_Click);
            // 
            // addButton
            // 
            this.addButton.BackColor = System.Drawing.Color.Transparent;
            this.addButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.addButton.DM_DisabledColor = System.Drawing.Color.Empty;
            this.addButton.DM_DownColor = System.Drawing.Color.Salmon;
            this.addButton.DM_MoveColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.addButton.DM_NormalColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.addButton.DM_Radius = 1;
            this.addButton.Font = new System.Drawing.Font("华文宋体", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.addButton.ForeColor = System.Drawing.Color.OrangeRed;
            this.addButton.Image = null;
            this.addButton.Location = new System.Drawing.Point(208, 99);
            this.addButton.Name = "addButton";
            this.addButton.Size = new System.Drawing.Size(232, 36);
            this.addButton.TabIndex = 16;
            this.addButton.Text = "没有数据，点击添加 [+]";
            this.addButton.UseVisualStyleBackColor = false;
            this.addButton.Visible = false;
            this.addButton.Click += new System.EventHandler(this.addButton_Click);
            // 
            // nameLabel
            // 
            this.nameLabel.AutoEllipsis = true;
            this.nameLabel.AutoSize = true;
            this.nameLabel.Font = new System.Drawing.Font("华文行楷", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.nameLabel.ForeColor = System.Drawing.Color.CornflowerBlue;
            this.nameLabel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.nameLabel.Location = new System.Drawing.Point(280, 47);
            this.nameLabel.Name = "nameLabel";
            this.nameLabel.Size = new System.Drawing.Size(63, 19);
            this.nameLabel.TabIndex = 17;
            this.nameLabel.Text = "叶小空";
            this.nameLabel.Visible = false;
            // 
            // ShowForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(650, 425);
            this.Controls.Add(this.dmIcon4);
            this.Controls.Add(this.nameLabel);
            this.Controls.Add(this.dmIcon2);
            this.Controls.Add(this.moreMenuStrip);
            this.Controls.Add(this.addButton);
            this.Controls.Add(this.messagelabel);
            this.Controls.Add(this.updateIcon);
            this.Controls.Add(this.addDmIcon);
            this.Controls.Add(this.setDmIcon);
            this.Controls.Add(this.dmIcon1);
            this.Controls.Add(this.searchTextBox);
            this.Controls.Add(this.codeControl1);
            this.DM_CanResize = false;
            this.DM_howBorder = false;
            this.DM_Radius = 3;
            this.DM_ShadowColor = System.Drawing.Color.LightPink;
            this.DM_ShadowWidth = 10;
            this.DM_SystemButtonThemeColor = DMSkin.DMSkinForm.DMColor.Dark;
            this.DM_SystemDrawLine = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.Name = "ShowForm";
            this.Text = "";
            this.Activated += new System.EventHandler(this.ShowForm_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ShowForm_FormClosing);
            this.Load += new System.EventHandler(this.ShowForm_Load);
            this.Click += new System.EventHandler(this.ShowForm_Click);
            this.moreMenuStrip.ResumeLayout(false);
            this.moreMenuStrip.PerformLayout();
            this.itemContextMenuStrip.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DMSkin.Controls.DM.DMIcon dmIcon2;
        private CodeControl codeControl1;
        private System.Windows.Forms.TextBox searchTextBox;
        private DMSkin.Controls.DM.DMIcon dmIcon1;
        private DMSkin.Controls.DM.DMIcon setDmIcon;
        private DMSkin.Controls.DM.DMIcon addDmIcon;
        private System.Windows.Forms.MenuStrip moreMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem moreToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 关于ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 帮助ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 新增项ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 用户设置ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private DMSkin.Controls.DM.DMIcon updateIcon;
        private System.Windows.Forms.ToolTip infoTip;
        private System.Windows.Forms.Label messagelabel;
        private System.Windows.Forms.Timer messageTimer;
        private System.Windows.Forms.ContextMenuStrip itemContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem 修改ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 查看ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 删除ToolStripMenuItem;
        private DMSkin.Controls.DMButton addButton;
        private System.Windows.Forms.ToolStripMenuItem 随机密码生成StripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.Label nameLabel;
        private DMSkin.Controls.DM.DMIcon dmIcon4;

    }
}