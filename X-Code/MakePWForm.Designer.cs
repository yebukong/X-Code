﻿namespace X_Code
{
    partial class MakePWForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MakePWForm));
            this.titleIcon = new DMSkin.Controls.DM.DMIcon();
            this.dmButtonClose1 = new DMSkin.Controls.DMButtonClose();
            this.targetTextBox = new DMSkin.Controls.DMTextBox();
            this.makeButton = new DMSkin.Controls.DMButton();
            this.resetButton = new DMSkin.Controls.DMButton();
            this.capitalCheckBox = new DMSkin.Metro.Controls.MetroCheckBox();
            this.unCapitalCheckBox = new DMSkin.Metro.Controls.MetroCheckBox();
            this.numberCheckBox = new DMSkin.Metro.Controls.MetroCheckBox();
            this.otherCharsTextBox = new DMSkin.Controls.DMTextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.targetIcon = new DMSkin.Controls.DM.DMIcon();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.otherCheckBox = new DMSkin.Metro.Controls.MetroCheckBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.otherLenTextBox = new DMSkin.Controls.DMTextBox();
            this.otherRadioButton = new DMSkin.Metro.Controls.MetroRadioButton();
            this.len16RadioButton = new DMSkin.Metro.Controls.MetroRadioButton();
            this.len12RadioButton = new DMSkin.Metro.Controls.MetroRadioButton();
            this.len6RadioButton = new DMSkin.Metro.Controls.MetroRadioButton();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // titleIcon
            // 
            this.titleIcon.BackColor = System.Drawing.Color.Transparent;
            this.titleIcon.DM_Color = System.Drawing.Color.Peru;
            this.titleIcon.DM_Font_Size = 18F;
            this.titleIcon.DM_Key = DMSkin.Controls.DM.DMIcon.DMIconKey.雾霾;
            this.titleIcon.DM_Text = " 随机密码生成";
            this.titleIcon.Location = new System.Drawing.Point(7, 20);
            this.titleIcon.Name = "titleIcon";
            this.titleIcon.Size = new System.Drawing.Size(228, 27);
            this.titleIcon.TabIndex = 4;
            this.titleIcon.Text = "titleIcon";
            // 
            // dmButtonClose1
            // 
            this.dmButtonClose1.BackColor = System.Drawing.Color.Transparent;
            this.dmButtonClose1.Dock = System.Windows.Forms.DockStyle.Right;
            this.dmButtonClose1.Location = new System.Drawing.Point(380, 4);
            this.dmButtonClose1.MaximumSize = new System.Drawing.Size(30, 27);
            this.dmButtonClose1.MinimumSize = new System.Drawing.Size(30, 27);
            this.dmButtonClose1.Name = "dmButtonClose1";
            this.dmButtonClose1.Size = new System.Drawing.Size(30, 27);
            this.dmButtonClose1.TabIndex = 5;
            this.dmButtonClose1.Click += new System.EventHandler(this.dmButtonClose1_Click);
            // 
            // targetTextBox
            // 
            this.targetTextBox.BackColor = System.Drawing.Color.Snow;
            this.targetTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.targetTextBox.Font = new System.Drawing.Font("微软雅黑", 13F);
            this.targetTextBox.ForeColor = System.Drawing.Color.LightSlateGray;
            this.targetTextBox.Location = new System.Drawing.Point(18, 20);
            this.targetTextBox.Name = "targetTextBox";
            this.targetTextBox.Size = new System.Drawing.Size(303, 23);
            this.targetTextBox.TabIndex = 7;
            this.targetTextBox.TabStop = false;
            this.targetTextBox.Text = "N/A\r\n";
            this.targetTextBox.WaterText = "";
            this.targetTextBox.TextChanged += new System.EventHandler(this.targetTextBox_TextChanged);
            // 
            // makeButton
            // 
            this.makeButton.BackColor = System.Drawing.Color.Transparent;
            this.makeButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.makeButton.DM_DisabledColor = System.Drawing.Color.LightGray;
            this.makeButton.DM_DownColor = System.Drawing.Color.FromArgb(((int)(((byte)(9)))), ((int)(((byte)(140)))), ((int)(((byte)(188)))));
            this.makeButton.DM_MoveColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(195)))), ((int)(((byte)(245)))));
            this.makeButton.DM_NormalColor = System.Drawing.Color.FromArgb(((int)(((byte)(9)))), ((int)(((byte)(163)))), ((int)(((byte)(220)))));
            this.makeButton.DM_Radius = 5;
            this.makeButton.Font = new System.Drawing.Font("新宋体", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.makeButton.ForeColor = System.Drawing.SystemColors.Info;
            this.makeButton.Image = null;
            this.makeButton.Location = new System.Drawing.Point(18, 260);
            this.makeButton.Name = "makeButton";
            this.makeButton.Size = new System.Drawing.Size(169, 45);
            this.makeButton.TabIndex = 104;
            this.makeButton.Text = "密码生成";
            this.makeButton.UseVisualStyleBackColor = false;
            this.makeButton.Click += new System.EventHandler(this.makeButton_Click);
            // 
            // resetButton
            // 
            this.resetButton.BackColor = System.Drawing.Color.Transparent;
            this.resetButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.resetButton.DM_DisabledColor = System.Drawing.Color.Silver;
            this.resetButton.DM_DownColor = System.Drawing.Color.LightSalmon;
            this.resetButton.DM_MoveColor = System.Drawing.Color.PeachPuff;
            this.resetButton.DM_NormalColor = System.Drawing.Color.LightPink;
            this.resetButton.DM_Radius = 5;
            this.resetButton.Font = new System.Drawing.Font("新宋体", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.resetButton.ForeColor = System.Drawing.SystemColors.Info;
            this.resetButton.Image = null;
            this.resetButton.Location = new System.Drawing.Point(215, 260);
            this.resetButton.Name = "resetButton";
            this.resetButton.Size = new System.Drawing.Size(179, 45);
            this.resetButton.TabIndex = 105;
            this.resetButton.Text = "选项重置";
            this.resetButton.UseVisualStyleBackColor = false;
            this.resetButton.Click += new System.EventHandler(this.resetButton_Click);
            // 
            // capitalCheckBox
            // 
            this.capitalCheckBox.AutoSize = true;
            this.capitalCheckBox.DM_UseSelectable = true;
            this.capitalCheckBox.Location = new System.Drawing.Point(18, 20);
            this.capitalCheckBox.Name = "capitalCheckBox";
            this.capitalCheckBox.Size = new System.Drawing.Size(44, 17);
            this.capitalCheckBox.TabIndex = 106;
            this.capitalCheckBox.Text = "A-Z";
            // 
            // unCapitalCheckBox
            // 
            this.unCapitalCheckBox.AutoSize = true;
            this.unCapitalCheckBox.DM_UseSelectable = true;
            this.unCapitalCheckBox.Location = new System.Drawing.Point(73, 20);
            this.unCapitalCheckBox.Name = "unCapitalCheckBox";
            this.unCapitalCheckBox.Size = new System.Drawing.Size(42, 17);
            this.unCapitalCheckBox.TabIndex = 107;
            this.unCapitalCheckBox.Text = "a-z";
            // 
            // numberCheckBox
            // 
            this.numberCheckBox.AutoSize = true;
            this.numberCheckBox.DM_UseSelectable = true;
            this.numberCheckBox.Location = new System.Drawing.Point(126, 20);
            this.numberCheckBox.Name = "numberCheckBox";
            this.numberCheckBox.Size = new System.Drawing.Size(43, 17);
            this.numberCheckBox.TabIndex = 108;
            this.numberCheckBox.Text = "0-9";
            // 
            // otherCharsTextBox
            // 
            this.otherCharsTextBox.BackColor = System.Drawing.Color.Snow;
            this.otherCharsTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.otherCharsTextBox.Font = new System.Drawing.Font("微软雅黑", 10F);
            this.otherCharsTextBox.ForeColor = System.Drawing.Color.LightSlateGray;
            this.otherCharsTextBox.Location = new System.Drawing.Point(232, 20);
            this.otherCharsTextBox.Name = "otherCharsTextBox";
            this.otherCharsTextBox.Size = new System.Drawing.Size(119, 18);
            this.otherCharsTextBox.TabIndex = 110;
            this.otherCharsTextBox.TabStop = false;
            this.otherCharsTextBox.WaterText = "";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.targetIcon);
            this.groupBox1.Controls.Add(this.targetTextBox);
            this.groupBox1.ForeColor = System.Drawing.Color.DimGray;
            this.groupBox1.Location = new System.Drawing.Point(23, 70);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(371, 55);
            this.groupBox1.TabIndex = 112;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "生成结果";
            // 
            // targetIcon
            // 
            this.targetIcon.BackColor = System.Drawing.Color.Transparent;
            this.targetIcon.Cursor = System.Windows.Forms.Cursors.Hand;
            this.targetIcon.DM_Color = System.Drawing.SystemColors.ScrollBar;
            this.targetIcon.DM_Font_Size = 12F;
            this.targetIcon.DM_Key = DMSkin.Controls.DM.DMIcon.DMIconKey.清单;
            this.targetIcon.DM_Text = "";
            this.targetIcon.Location = new System.Drawing.Point(327, 23);
            this.targetIcon.Name = "targetIcon";
            this.targetIcon.Size = new System.Drawing.Size(20, 18);
            this.targetIcon.TabIndex = 17;
            this.targetIcon.Text = "dmIcon2";
            this.targetIcon.Click += new System.EventHandler(this.accountIcon_Click);
            this.targetIcon.MouseDown += new System.Windows.Forms.MouseEventHandler(this.targetIcon_MouseDown);
            this.targetIcon.MouseEnter += new System.EventHandler(this.targetIcon_MouseEnter);
            this.targetIcon.MouseLeave += new System.EventHandler(this.targetIcon_MouseLeave);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.otherCheckBox);
            this.groupBox2.Controls.Add(this.capitalCheckBox);
            this.groupBox2.Controls.Add(this.unCapitalCheckBox);
            this.groupBox2.Controls.Add(this.numberCheckBox);
            this.groupBox2.Controls.Add(this.otherCharsTextBox);
            this.groupBox2.ForeColor = System.Drawing.Color.DimGray;
            this.groupBox2.Location = new System.Drawing.Point(23, 136);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(371, 50);
            this.groupBox2.TabIndex = 113;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "字符范围";
            // 
            // otherCheckBox
            // 
            this.otherCheckBox.AutoSize = true;
            this.otherCheckBox.DM_UseSelectable = true;
            this.otherCheckBox.Location = new System.Drawing.Point(178, 20);
            this.otherCheckBox.Name = "otherCheckBox";
            this.otherCheckBox.Size = new System.Drawing.Size(48, 17);
            this.otherCheckBox.TabIndex = 111;
            this.otherCheckBox.Text = "其他";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.otherLenTextBox);
            this.groupBox3.Controls.Add(this.otherRadioButton);
            this.groupBox3.Controls.Add(this.len16RadioButton);
            this.groupBox3.Controls.Add(this.len12RadioButton);
            this.groupBox3.Controls.Add(this.len6RadioButton);
            this.groupBox3.ForeColor = System.Drawing.Color.DimGray;
            this.groupBox3.Location = new System.Drawing.Point(23, 197);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(371, 50);
            this.groupBox3.TabIndex = 114;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "密码长度";
            // 
            // otherLenTextBox
            // 
            this.otherLenTextBox.BackColor = System.Drawing.Color.Snow;
            this.otherLenTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.otherLenTextBox.Font = new System.Drawing.Font("微软雅黑", 10F);
            this.otherLenTextBox.ForeColor = System.Drawing.Color.LightSlateGray;
            this.otherLenTextBox.Location = new System.Drawing.Point(232, 19);
            this.otherLenTextBox.Name = "otherLenTextBox";
            this.otherLenTextBox.Size = new System.Drawing.Size(119, 18);
            this.otherLenTextBox.TabIndex = 112;
            this.otherLenTextBox.TabStop = false;
            this.otherLenTextBox.WaterText = "";
            // 
            // otherRadioButton
            // 
            this.otherRadioButton.AutoSize = true;
            this.otherRadioButton.DM_UseSelectable = true;
            this.otherRadioButton.Location = new System.Drawing.Point(178, 20);
            this.otherRadioButton.Name = "otherRadioButton";
            this.otherRadioButton.Size = new System.Drawing.Size(48, 17);
            this.otherRadioButton.TabIndex = 3;
            this.otherRadioButton.Text = "其他";
            // 
            // len16RadioButton
            // 
            this.len16RadioButton.AutoSize = true;
            this.len16RadioButton.DM_UseSelectable = true;
            this.len16RadioButton.Location = new System.Drawing.Point(126, 21);
            this.len16RadioButton.Name = "len16RadioButton";
            this.len16RadioButton.Size = new System.Drawing.Size(38, 17);
            this.len16RadioButton.TabIndex = 2;
            this.len16RadioButton.Text = "16";
            // 
            // len12RadioButton
            // 
            this.len12RadioButton.AutoSize = true;
            this.len12RadioButton.DM_UseSelectable = true;
            this.len12RadioButton.Location = new System.Drawing.Point(73, 21);
            this.len12RadioButton.Name = "len12RadioButton";
            this.len12RadioButton.Size = new System.Drawing.Size(38, 17);
            this.len12RadioButton.TabIndex = 1;
            this.len12RadioButton.Text = "12";
            // 
            // len6RadioButton
            // 
            this.len6RadioButton.AutoSize = true;
            this.len6RadioButton.DM_UseSelectable = true;
            this.len6RadioButton.Location = new System.Drawing.Point(18, 21);
            this.len6RadioButton.Name = "len6RadioButton";
            this.len6RadioButton.Size = new System.Drawing.Size(31, 17);
            this.len6RadioButton.TabIndex = 0;
            this.len6RadioButton.Text = "6";
            // 
            // MakePWForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(414, 323);
            this.ControlBox = false;
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.resetButton);
            this.Controls.Add(this.makeButton);
            this.Controls.Add(this.dmButtonClose1);
            this.Controls.Add(this.titleIcon);
            this.DM_CanResize = false;
            this.DM_howBorder = false;
            this.DM_Mobile = DMSkin.MobileStyle.TitleMobile;
            this.DM_Radius = 3;
            this.DM_ShadowColor = System.Drawing.Color.Sienna;
            this.DM_ShadowWidth = 10;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MakePWForm";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "MakePWForm";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MakePWForm_FormClosing);
            this.Load += new System.EventHandler(this.MakePWForm_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private DMSkin.Controls.DM.DMIcon titleIcon;
        private DMSkin.Controls.DMButtonClose dmButtonClose1;
        private DMSkin.Controls.DMTextBox targetTextBox;
        private DMSkin.Controls.DMButton makeButton;
        private DMSkin.Controls.DMButton resetButton;
        private DMSkin.Metro.Controls.MetroCheckBox capitalCheckBox;
        private DMSkin.Metro.Controls.MetroCheckBox unCapitalCheckBox;
        private DMSkin.Metro.Controls.MetroCheckBox numberCheckBox;
        private DMSkin.Controls.DMTextBox otherCharsTextBox;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private DMSkin.Metro.Controls.MetroRadioButton len16RadioButton;
        private DMSkin.Metro.Controls.MetroRadioButton len12RadioButton;
        private DMSkin.Metro.Controls.MetroRadioButton len6RadioButton;
        private DMSkin.Controls.DMTextBox otherLenTextBox;
        private DMSkin.Metro.Controls.MetroRadioButton otherRadioButton;
        private DMSkin.Metro.Controls.MetroCheckBox otherCheckBox;
        private DMSkin.Controls.DM.DMIcon targetIcon;
    }
}