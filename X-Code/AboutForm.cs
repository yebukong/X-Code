﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace X_Code
{
    public partial class AboutForm : DMSkin.Main
    {
        private Owner ownerInfo = null;
        public AboutForm(Owner ownerInfo)
        {
            this.ownerInfo = ownerInfo;
            InitializeComponent();
        }

        private void AboutForm_Load(object sender, EventArgs e)
        {
            //透明度
            this.Opacity = ownerInfo.FormOpacity;
            //窗口阴影
            this.DM_Shadow = ownerInfo.IsShowShadow;
            if (ownerInfo.IsShowAnimate)
            {
                Animate.AnimateWindow(this.Handle, 50, Animate.AW_ACTIVATE + Animate.AW_CENTER);//向外翻
            }
        }
        protected override void OnPaint(PaintEventArgs e)
        {
            Graphics g = e.Graphics;//获取绘制对象
            ///设置参数
            g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
            // Create pen.
            Pen pen = new Pen(Color.Salmon, 3);

            // Create points that define line.
            Point point1 = new Point(0, 55);
            Point point2 = new Point(500, 55);
            g.DrawLine(pen, point1, point2);

            base.OnPaint(e);
        }

        private void dmButtonClose1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
