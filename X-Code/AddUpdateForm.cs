﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using X_Code.service;

namespace X_Code
{
    public enum FormType
    {
        AddForm, UpdateForm
    }
    /// <summary>
    /// 新增密码项或者更新密码项的窗体
    /// </summary>
    public partial class AddUpdateForm : DMSkin.Main
    {
        private CodeItemService cis = new CodeItemService();
        private FormType one;
        private CodeItem item;
        public String resultStr = "";//返回操作结果,用于展示
        public Color resultColor = MineInfo.首要;//返回颜色,用于展示

        private Owner ownerInfo = null;

        public AddUpdateForm(Owner ownerInfo, FormType one,CodeItem item)
        {
            this.ownerInfo = ownerInfo;
            if (one == FormType.UpdateForm){
                if (item == null || String.IsNullOrEmpty(item.Title))
                {
                    throw new Exception("item非法。。。");
                }
                this.item = item;
                this.one = FormType.UpdateForm;
              
            }
            
            InitializeComponent();
        }
        private void AddUpdateForm_Load(object sender, EventArgs e)
        {
            //透明度
            this.Opacity = ownerInfo.FormOpacity;
            //窗口阴影
            this.DM_Shadow = ownerInfo.IsShowShadow;  

            if (this.one == FormType.UpdateForm)
            {
                addTitle.DM_Text = "";
                addTitle.DM_Key = DMSkin.Controls.DM.DMIcon.DMIconKey.编辑;
                addTitle.DM_Text += " 修改项";
                addButton.Text = "确认修改";
                addTitle.DM_Color = Color.Red;//标题颜色
                this.DM_ShadowColor = Color.Red;//窗体阴影
                addButton.DM_DownColor = Color.IndianRed;//按钮按下颜色
                addButton.DM_MoveColor = Color.DarkSalmon;//按钮鼠标移动颜色
                addButton.DM_NormalColor = Color.FromArgb(255, 128, 128);//按钮正常颜色                
                //初始化
                keyTextBox.Text = item.Title;
                accountTextBox.Text = item.Account;
                passwordTextBox.Text = item.PassWord;
                pathTextBox.Text = item.OpenPath;
                descTextBox.Text = item.Desc;

                keyTextBox.ReadOnly = true;//key只读
                keyTextBox.TabStop = false;
                accountTextBox.Select(accountTextBox.Text.Length, 0);
                openButton.Enabled = true;
                addButton.Enabled = true;
            }
            else
            {
                addTitle.DM_Text = "";
                addTitle.DM_Key = DMSkin.Controls.DM.DMIcon.DMIconKey.add;
                addTitle.DM_Text += " 新增项";
                addButton.Text = "确认添加";
                addTitle.DM_Color = Color.OrangeRed;//标题颜色
                this.DM_ShadowColor = Color.OrangeRed;//窗体阴影
                addButton.DM_DownColor = Color.Tomato;//按钮按下颜色
                addButton.DM_MoveColor = Color.Coral;//按钮鼠标移动颜色
                addButton.DM_NormalColor = Color.LightSalmon;//按钮正常颜色

                keyTextBox.ReadOnly = false;//key只读
            }

            if (ownerInfo.IsShowAnimate)
            {
                Animate.AnimateWindow(this.Handle, 50, Animate.AW_ACTIVATE + Animate.AW_CENTER);//向外翻
            }
        }
        
        protected override void OnPaint(PaintEventArgs e)
        {
            Graphics g = e.Graphics;//获取绘制对象
            ///设置参数
            g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
            // Create pen.
            Pen pen = new Pen(Color.Gainsboro, 3);

            // Create points that define line.
            Point point1 = new Point(0, 55);
            Point point2 = new Point(500, 55); 
            g.DrawLine(pen, point1, point2);
        }
        //确认按钮
        private void addButton_Click(object sender, EventArgs e)
        {
            //获取并校验文本框数据
            CodeItem one = new CodeItem();

           
            one.Title = keyTextBox.Text.Trim();//检验过了
            one.Account = accountTextBox.Text == null ? null : accountTextBox.Text.Trim();
            one.PassWord = passwordTextBox.Text == null ? null : passwordTextBox.Text.Trim();
            one.OpenPath = pathTextBox.Text == null ? null : pathTextBox.Text.Trim();
            one.Desc = descTextBox.Text == null ? null : descTextBox.Text.Trim();

            if (this.one == FormType.UpdateForm)
            {
                one.AddTime = item.AddTime;
                one.UpdateTime = DateTime.Now;
                update(one);
            }
            else
            {
                one.AddTime = DateTime.Now;
                one.UpdateTime = DateTime.Now;
                add(one);
            }
            
        }
        /// <summary>
        /// 执行更新
        /// </summary>
        private void update(CodeItem one)
        {
            try
            {
                if (cis.Update(one))
                {
                    resultStr = "更新成功！";
                    resultColor = MineInfo.成功;
                }
                else
                {
                    resultStr = "更新失败！";
                    resultColor = MineInfo.危险;
                }
            }
            catch (Exception ex)
            {
                resultStr = "更新异常:" + ex.Message;
                resultColor = MineInfo.警告;
            }
            this.Close();
        } 
        /// <summary>
        /// 执行添加
        /// </summary>
        private void add(CodeItem one)
        {
            try
            {
                //尝试添加
                if (cis.Add(one))
                {
                    resultStr = "添加成功！";
                    resultColor=MineInfo.成功;
                }
                else
                {
                    resultStr = "添加失败！";
                    resultColor=MineInfo.危险;
                }
            }
            catch(Exception ex)
            {
                resultStr = "添加异常:"+ex.Message;
                resultColor=MineInfo.警告;
            }
            this.Close();
            
        }
        private void closeButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void hideButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// 标题输入完成后校验信息
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void keyTextBox_Leave(object sender, EventArgs e)
        {
            if(keyTextBox.ReadOnly){
                return;
            }
            String key = keyTextBox.Text;
            if (String.IsNullOrWhiteSpace(key))//无效字符
            {
                this.showInfo(MineInfo.危险, "标题信息为必填项！");
                //keyTextBox.Clear();
                openButton.Enabled = false;
                addButton.Enabled = false;
                keyTextBox.Focus();
                return;
            }
            key = key.Trim();
            //验证标题是否唯一
            try
            {
                if (cis.FindOne(key) != null)
                {
                    this.showInfo(MineInfo.警告, "标题已存在！");
                    //keyTextBox.Clear();
                    keyTextBox.Focus();
                    keyTextBox.SelectAll();
                    openButton.Enabled = false;
                    addButton.Enabled = false;
                    return;
                }
            }
            catch(Exception one)
            {
                DMSkin.MetroMessageBox.Show(this, "" + one.Message, "发生异常:", MessageBoxButtons.OK, MessageBoxIcon.Error, 100);
                this.Close();
                
            }
            openButton.Enabled = true;
            addButton.Enabled = true;

        }
        

        static int MAX_TIMES = 3; //计时器时间
        int nowTimes = MAX_TIMES;
        //计时器方法
        private void messageTimer_Tick(object sender, EventArgs e)
        {

            if (nowTimes > 1)
            {
                nowTimes--;//自减 
            }
            else
            {
                //隐藏
                nowTimes = MAX_TIMES;
                Animate.AnimateWindow(this.infoLabel.Handle, 100, Animate.AW_HIDE + Animate.AW_SLIDE + Animate.AW_HOR_NEGATIVE);//信息滑出
                this.Invalidate();//触发窗口重绘
                this.messageTimer.Stop();
            }
        }
        private void showInfo(Color infoColor, String msg)
        {
            this.messageTimer.Stop();
            infoLabel.BackColor = infoColor;
            infoLabel.Text = msg;
            infoLabel.Visible = false;//确保先前信息的消失            
            //滑出
            Animate.AnimateWindow(this.infoLabel.Handle, 100, Animate.AW_SLIDE + Animate.AW_HOR_POSITIVE);
            infoLabel.Visible = true;

            this.messageTimer.Start();//开始计时器
        }

        private void AddUpdateForm_Click(object sender, EventArgs e)
        {
            addTitle.Focus();
        }

        private void keyTextBox_Enter(object sender, EventArgs e)
        {
            //不允许修改
            if (this.one == FormType.UpdateForm)
            {
                showInfo(MineInfo.首要, "提示:标题无法修改、");
            }
        }

        private void keyTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            //不允许修改
            if (this.one == FormType.UpdateForm)
            {
                showInfo(MineInfo.危险, "无法修改！");
            }
        }

        private void openButton_Click(object sender, EventArgs e)
        {
            OpenFileDialog one = new OpenFileDialog();// 文件选择框类型
            one.Title = "选择本机程序路径";
            one.Filter = " 可执行文件(*.exe)|*.exe|批处理文件(*.dat)|*.dat|所有文件(*.*)|*.*";
            // one.ShowDialog();  //展示
            string url = "";
            if (one.ShowDialog() == DialogResult.OK)
            {
                url = one.FileName; //获取文件名
            }
            if (url.EndsWith(".exe", StringComparison.OrdinalIgnoreCase) || url.EndsWith(".bat", StringComparison.OrdinalIgnoreCase))
            {
                pathTextBox.Text = url;
            }
        }
        
    }
}
