﻿namespace X_Code
{
    partial class DescPWForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DescPWForm));
            this.dmButtonClose1 = new DMSkin.Controls.DMButtonClose();
            this.titleIcon = new DMSkin.Controls.DM.DMIcon();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.openPath = new MaterialSkin.Controls.MaterialFlatButton();
            this.itemCount = new System.Windows.Forms.Label();
            this.fileDate = new System.Windows.Forms.Label();
            this.fileSize = new System.Windows.Forms.Label();
            this.fileName = new System.Windows.Forms.Label();
            this.userName = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.infoPanel = new DMSkin.Metro.Controls.MetroPanel();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.infoToolTip = new System.Windows.Forms.ToolTip(this.components);
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.infoPanel.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // dmButtonClose1
            // 
            this.dmButtonClose1.BackColor = System.Drawing.Color.Transparent;
            this.dmButtonClose1.Dock = System.Windows.Forms.DockStyle.Right;
            this.dmButtonClose1.Location = new System.Drawing.Point(476, 4);
            this.dmButtonClose1.MaximumSize = new System.Drawing.Size(30, 27);
            this.dmButtonClose1.MinimumSize = new System.Drawing.Size(30, 27);
            this.dmButtonClose1.Name = "dmButtonClose1";
            this.dmButtonClose1.Size = new System.Drawing.Size(30, 27);
            this.dmButtonClose1.TabIndex = 0;
            this.dmButtonClose1.Click += new System.EventHandler(this.dmButtonClose1_Click);
            // 
            // titleIcon
            // 
            this.titleIcon.BackColor = System.Drawing.Color.Transparent;
            this.titleIcon.DM_Color = System.Drawing.Color.CornflowerBlue;
            this.titleIcon.DM_Font_Size = 18F;
            this.titleIcon.DM_Key = DMSkin.Controls.DM.DMIcon.DMIconKey.sanyuan;
            this.titleIcon.DM_Text = " 帮助和说明";
            this.titleIcon.Location = new System.Drawing.Point(7, 20);
            this.titleIcon.Name = "titleIcon";
            this.titleIcon.Size = new System.Drawing.Size(171, 27);
            this.titleIcon.TabIndex = 3;
            this.titleIcon.Text = "titleIcon";
            this.infoToolTip.SetToolTip(this.titleIcon, "标题");
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Location = new System.Drawing.Point(13, 15);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(480, 131);
            this.groupBox2.TabIndex = 10;
            this.groupBox2.TabStop = false;
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("幼圆", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label4.Location = new System.Drawing.Point(121, 17);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(341, 102);
            this.label4.TabIndex = 5;
            this.label4.Text = "a.具备简单的用户登录认证,密码修改功能\r\n\r\nb.可以进行基本的密码项操作(增、删、改、查)\r\n\r\nc.支持一些基本功能设置\r\n\r\nd.对本地数据信息进行加密处" +
    "理\r\n";
            this.infoToolTip.SetToolTip(this.label4, "未实现功能：\r\n   ☞ 密码项列表详细显示\r\n   ☞ 将.dll文件嵌入程序");
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("幼圆", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label2.ForeColor = System.Drawing.Color.LightSeaGreen;
            this.label2.Location = new System.Drawing.Point(6, 21);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(114, 19);
            this.label2.TabIndex = 1;
            this.label2.Text = "功能信息：";
            this.infoToolTip.SetToolTip(this.label2, "未实现功能：\r\n   ☞ 密码项列表详细显示\r\n   ☞ 密码随机生成功能\r\n   ☞ 将.dll文件嵌入程序");
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.openPath);
            this.groupBox1.Controls.Add(this.itemCount);
            this.groupBox1.Controls.Add(this.fileDate);
            this.groupBox1.Controls.Add(this.fileSize);
            this.groupBox1.Controls.Add(this.fileName);
            this.groupBox1.Controls.Add(this.userName);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(13, 262);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(480, 169);
            this.groupBox1.TabIndex = 11;
            this.groupBox1.TabStop = false;
            // 
            // openPath
            // 
            this.openPath.AutoSize = true;
            this.openPath.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.openPath.BackColor = System.Drawing.Color.PaleGoldenrod;
            this.openPath.CausesValidation = false;
            this.openPath.Cursor = System.Windows.Forms.Cursors.Hand;
            this.openPath.Depth = 0;
            this.openPath.Font = new System.Drawing.Font("微软雅黑", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.openPath.ForeColor = System.Drawing.Color.LightSalmon;
            this.openPath.Location = new System.Drawing.Point(10, 73);
            this.openPath.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.openPath.MouseState = MaterialSkin.MouseState.HOVER;
            this.openPath.Name = "openPath";
            this.openPath.Primary = true;
            this.openPath.Size = new System.Drawing.Size(81, 36);
            this.openPath.TabIndex = 11;
            this.openPath.TabStop = false;
            this.openPath.Text = "目录中打开";
            this.infoToolTip.SetToolTip(this.openPath, "打开用户数据文件所在目录");
            this.openPath.UseVisualStyleBackColor = false;
            this.openPath.Click += new System.EventHandler(this.openPath_Click);
            // 
            // itemCount
            // 
            this.itemCount.AutoSize = true;
            this.itemCount.Font = new System.Drawing.Font("幼圆", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.itemCount.Location = new System.Drawing.Point(246, 145);
            this.itemCount.Name = "itemCount";
            this.itemCount.Size = new System.Drawing.Size(28, 14);
            this.itemCount.TabIndex = 10;
            this.itemCount.Text = "N/A";
            // 
            // fileDate
            // 
            this.fileDate.AutoSize = true;
            this.fileDate.Font = new System.Drawing.Font("幼圆", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.fileDate.Location = new System.Drawing.Point(246, 115);
            this.fileDate.Name = "fileDate";
            this.fileDate.Size = new System.Drawing.Size(28, 14);
            this.fileDate.TabIndex = 9;
            this.fileDate.Text = "N/A";
            // 
            // fileSize
            // 
            this.fileSize.AutoSize = true;
            this.fileSize.Font = new System.Drawing.Font("幼圆", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.fileSize.Location = new System.Drawing.Point(246, 85);
            this.fileSize.Name = "fileSize";
            this.fileSize.Size = new System.Drawing.Size(28, 14);
            this.fileSize.TabIndex = 8;
            this.fileSize.Text = "N/A";
            // 
            // fileName
            // 
            this.fileName.AutoSize = true;
            this.fileName.Font = new System.Drawing.Font("幼圆", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.fileName.Location = new System.Drawing.Point(246, 55);
            this.fileName.Name = "fileName";
            this.fileName.Size = new System.Drawing.Size(28, 14);
            this.fileName.TabIndex = 7;
            this.fileName.Text = "N/A";
            // 
            // userName
            // 
            this.userName.AutoSize = true;
            this.userName.Font = new System.Drawing.Font("幼圆", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.userName.Location = new System.Drawing.Point(246, 26);
            this.userName.Name = "userName";
            this.userName.Size = new System.Drawing.Size(28, 14);
            this.userName.TabIndex = 6;
            this.userName.Text = "N/A";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("幼圆", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label11.Location = new System.Drawing.Point(121, 145);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(91, 14);
            this.label11.TabIndex = 5;
            this.label11.Text = "密码项信息数";
            this.infoToolTip.SetToolTip(this.label11, "用户添加密码项数目");
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("幼圆", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label10.Location = new System.Drawing.Point(121, 115);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(91, 14);
            this.label10.TabIndex = 4;
            this.label10.Text = "文件创建时间";
            this.infoToolTip.SetToolTip(this.label10, "文件创建时间");
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("幼圆", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label9.Location = new System.Drawing.Point(121, 55);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(91, 14);
            this.label9.TabIndex = 3;
            this.label9.Text = "用户数据文件";
            this.infoToolTip.SetToolTip(this.label9, "用户数据文件");
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("幼圆", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label6.Location = new System.Drawing.Point(121, 85);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(63, 14);
            this.label6.TabIndex = 2;
            this.label6.Text = "文件大小";
            this.infoToolTip.SetToolTip(this.label6, "文件大小");
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("幼圆", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label5.Location = new System.Drawing.Point(121, 25);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(49, 14);
            this.label5.TabIndex = 1;
            this.label5.Text = "用户名";
            this.infoToolTip.SetToolTip(this.label5, "用户名");
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("幼圆", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.ForeColor = System.Drawing.Color.OrangeRed;
            this.label1.Location = new System.Drawing.Point(6, 21);
            this.label1.MaximumSize = new System.Drawing.Size(114, 19);
            this.label1.MinimumSize = new System.Drawing.Size(114, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(114, 19);
            this.label1.TabIndex = 0;
            this.label1.Text = "数据信息：";
            this.infoToolTip.SetToolTip(this.label1, "数据文件基本信息,擅自修改文件将导致数据丢失！");
            // 
            // infoPanel
            // 
            this.infoPanel.AutoScroll = true;
            this.infoPanel.Controls.Add(this.groupBox3);
            this.infoPanel.Controls.Add(this.groupBox2);
            this.infoPanel.Controls.Add(this.groupBox1);
            this.infoPanel.DM_HorizontalScrollbar = true;
            this.infoPanel.DM_HorizontalScrollbarBarColor = true;
            this.infoPanel.DM_HorizontalScrollbarDM_HighlightOnWheel = true;
            this.infoPanel.DM_HorizontalScrollbarSize = 5;
            this.infoPanel.DM_ThumbColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.infoPanel.DM_ThumbNormalColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.infoPanel.DM_UseBarColor = true;
            this.infoPanel.DM_UseCustomBackColor = true;
            this.infoPanel.DM_UseCustomForeColor = true;
            this.infoPanel.DM_UseStyleColors = true;
            this.infoPanel.DM_VerticalScrollbar = true;
            this.infoPanel.DM_VerticalScrollbarBarColor = true;
            this.infoPanel.DM_VerticalScrollbarDM_HighlightOnWheel = true;
            this.infoPanel.DM_VerticalScrollbarSize = 5;
            this.infoPanel.Location = new System.Drawing.Point(0, 57);
            this.infoPanel.Margin = new System.Windows.Forms.Padding(0);
            this.infoPanel.Name = "infoPanel";
            this.infoPanel.Size = new System.Drawing.Size(510, 272);
            this.infoPanel.Style = DMSkin.Metro.MetroColorStyle.Purple;
            this.infoPanel.TabIndex = 12;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Location = new System.Drawing.Point(13, 152);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(480, 104);
            this.groupBox3.TabIndex = 12;
            this.groupBox3.TabStop = false;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("幼圆", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label3.Location = new System.Drawing.Point(308, 17);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(154, 54);
            this.label3.TabIndex = 7;
            this.label3.Text = "Ctrl+H   打开帮助窗口\r\n\r\nCtrl+I   打开关于窗口";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("幼圆", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label7.ForeColor = System.Drawing.Color.ForestGreen;
            this.label7.Location = new System.Drawing.Point(6, 21);
            this.label7.MaximumSize = new System.Drawing.Size(114, 19);
            this.label7.MinimumSize = new System.Drawing.Size(114, 19);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(114, 19);
            this.label7.TabIndex = 0;
            this.label7.Text = "帮助信息：";
            this.infoToolTip.SetToolTip(this.label7, "帮助信息");
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("幼圆", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label8.Location = new System.Drawing.Point(121, 17);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(165, 81);
            this.label8.TabIndex = 6;
            this.label8.Text = "Ctrl+N   添加密码项\r\n\r\nCtrl+T   打开设置窗口\r\n\r\nF5       刷新密码项\r\n";
            // 
            // infoToolTip
            // 
            this.infoToolTip.AutoPopDelay = 8000;
            this.infoToolTip.InitialDelay = 500;
            this.infoToolTip.ReshowDelay = 100;
            // 
            // DescPWForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(510, 341);
            this.ControlBox = false;
            this.Controls.Add(this.infoPanel);
            this.Controls.Add(this.titleIcon);
            this.Controls.Add(this.dmButtonClose1);
            this.DM_CanResize = false;
            this.DM_howBorder = false;
            this.DM_Mobile = DMSkin.MobileStyle.TitleMobile;
            this.DM_Radius = 3;
            this.DM_ShadowColor = System.Drawing.Color.CornflowerBlue;
            this.DM_ShadowWidth = 10;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "DescPWForm";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "帮助和说明";
            this.TransparencyKey = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.Load += new System.EventHandler(this.DescForm_Load);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.infoPanel.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private DMSkin.Controls.DMButtonClose dmButtonClose1;
        private DMSkin.Controls.DM.DMIcon titleIcon;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private DMSkin.Metro.Controls.MetroPanel infoPanel;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label itemCount;
        private System.Windows.Forms.Label fileDate;
        private System.Windows.Forms.Label fileSize;
        private System.Windows.Forms.Label fileName;
        private System.Windows.Forms.Label userName;
        private System.Windows.Forms.Label label11;
        private MaterialSkin.Controls.MaterialFlatButton openPath;
        private System.Windows.Forms.ToolTip infoToolTip;
    }
}