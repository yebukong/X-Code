﻿namespace X_Code
{
    partial class OneForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dmButtonClose1 = new DMSkin.Controls.DMButtonClose();
            this.closeButton = new DMSkin.Controls.DMButton();
            this.topIcon = new DMSkin.Controls.DM.DMIcon();
            this.title = new System.Windows.Forms.Label();
            this.infoToolTip = new System.Windows.Forms.ToolTip(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.pwIcon = new DMSkin.Controls.DM.DMIcon();
            this.accountIcon = new DMSkin.Controls.DM.DMIcon();
            this.showPwIcon = new DMSkin.Controls.DM.DMIcon();
            this.openIcon = new DMSkin.Controls.DM.DMIcon();
            this.accountTextBox = new DMSkin.Controls.DMTextBox();
            this.pwTextBox = new DMSkin.Controls.DMTextBox();
            this.descTextBox = new DMSkin.Controls.DMTextBox();
            this.timeIcon = new DMSkin.Controls.DM.DMIcon();
            this.SuspendLayout();
            // 
            // dmButtonClose1
            // 
            this.dmButtonClose1.AutoSize = true;
            this.dmButtonClose1.BackColor = System.Drawing.Color.Transparent;
            this.dmButtonClose1.Dock = System.Windows.Forms.DockStyle.Right;
            this.dmButtonClose1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.dmButtonClose1.Location = new System.Drawing.Point(327, 4);
            this.dmButtonClose1.MaximumSize = new System.Drawing.Size(30, 27);
            this.dmButtonClose1.MinimumSize = new System.Drawing.Size(30, 27);
            this.dmButtonClose1.Name = "dmButtonClose1";
            this.dmButtonClose1.Size = new System.Drawing.Size(30, 27);
            this.dmButtonClose1.TabIndex = 0;
            this.dmButtonClose1.Click += new System.EventHandler(this.dmButtonClose1_Click);
            // 
            // closeButton
            // 
            this.closeButton.BackColor = System.Drawing.Color.Transparent;
            this.closeButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.closeButton.DM_DisabledColor = System.Drawing.Color.Empty;
            this.closeButton.DM_DownColor = System.Drawing.Color.FromArgb(((int)(((byte)(9)))), ((int)(((byte)(140)))), ((int)(((byte)(188)))));
            this.closeButton.DM_MoveColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(195)))), ((int)(((byte)(245)))));
            this.closeButton.DM_NormalColor = System.Drawing.Color.FromArgb(((int)(((byte)(9)))), ((int)(((byte)(163)))), ((int)(((byte)(220)))));
            this.closeButton.DM_Radius = 5;
            this.closeButton.Image = null;
            this.closeButton.Location = new System.Drawing.Point(385, 9);
            this.closeButton.Name = "closeButton";
            this.closeButton.Size = new System.Drawing.Size(10, 10);
            this.closeButton.TabIndex = 1;
            this.closeButton.Text = "关闭";
            this.closeButton.UseVisualStyleBackColor = false;
            this.closeButton.Click += new System.EventHandler(this.closeButton_Click);
            // 
            // topIcon
            // 
            this.topIcon.BackColor = System.Drawing.Color.Transparent;
            this.topIcon.DM_Color = System.Drawing.Color.OrangeRed;
            this.topIcon.DM_Font_Size = 30F;
            this.topIcon.DM_Key = DMSkin.Controls.DM.DMIcon.DMIconKey.发布;
            this.topIcon.DM_Text = "";
            this.topIcon.Location = new System.Drawing.Point(8, 11);
            this.topIcon.Name = "topIcon";
            this.topIcon.Size = new System.Drawing.Size(47, 36);
            this.topIcon.TabIndex = 2;
            this.topIcon.Text = "dmIcon1";
            this.infoToolTip.SetToolTip(this.topIcon, "标题");
            // 
            // title
            // 
            this.title.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.title.AutoEllipsis = true;
            this.title.Cursor = System.Windows.Forms.Cursors.Default;
            this.title.Font = new System.Drawing.Font("微软雅黑 Light", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.title.ForeColor = System.Drawing.Color.Tomato;
            this.title.Location = new System.Drawing.Point(61, 20);
            this.title.Name = "title";
            this.title.Size = new System.Drawing.Size(247, 27);
            this.title.TabIndex = 3;
            this.title.Text = "标题文字";
            this.infoToolTip.SetToolTip(this.title, "标题文字");
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("幼圆", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.label1.Location = new System.Drawing.Point(24, 78);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(42, 16);
            this.label1.TabIndex = 11;
            this.label1.Text = "账号";
            this.infoToolTip.SetToolTip(this.label1, "账号");
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("幼圆", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.label2.Location = new System.Drawing.Point(24, 121);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(42, 16);
            this.label2.TabIndex = 12;
            this.label2.Text = "密码";
            this.infoToolTip.SetToolTip(this.label2, "密码");
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("幼圆", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.label3.Location = new System.Drawing.Point(24, 172);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(42, 16);
            this.label3.TabIndex = 13;
            this.label3.Text = "说明";
            this.infoToolTip.SetToolTip(this.label3, "说明");
            // 
            // pwIcon
            // 
            this.pwIcon.BackColor = System.Drawing.Color.Transparent;
            this.pwIcon.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pwIcon.DM_Color = System.Drawing.SystemColors.ScrollBar;
            this.pwIcon.DM_Font_Size = 12F;
            this.pwIcon.DM_Key = DMSkin.Controls.DM.DMIcon.DMIconKey.清单;
            this.pwIcon.DM_Text = "";
            this.pwIcon.Enabled = false;
            this.pwIcon.Location = new System.Drawing.Point(287, 121);
            this.pwIcon.Name = "pwIcon";
            this.pwIcon.Size = new System.Drawing.Size(20, 18);
            this.pwIcon.TabIndex = 15;
            this.pwIcon.Text = "dmIcon1";
            this.infoToolTip.SetToolTip(this.pwIcon, "复制密码(可能有风险)");
            this.pwIcon.Click += new System.EventHandler(this.pwIcon_Click);
            this.pwIcon.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pwIcon_MouseDown);
            this.pwIcon.MouseEnter += new System.EventHandler(this.pwIcon_MouseEnter);
            this.pwIcon.MouseLeave += new System.EventHandler(this.pwIcon_MouseLeave);
            // 
            // accountIcon
            // 
            this.accountIcon.BackColor = System.Drawing.Color.Transparent;
            this.accountIcon.Cursor = System.Windows.Forms.Cursors.Hand;
            this.accountIcon.DM_Color = System.Drawing.SystemColors.ScrollBar;
            this.accountIcon.DM_Font_Size = 12F;
            this.accountIcon.DM_Key = DMSkin.Controls.DM.DMIcon.DMIconKey.清单;
            this.accountIcon.DM_Text = "";
            this.accountIcon.Enabled = false;
            this.accountIcon.Location = new System.Drawing.Point(287, 76);
            this.accountIcon.Name = "accountIcon";
            this.accountIcon.Size = new System.Drawing.Size(20, 18);
            this.accountIcon.TabIndex = 16;
            this.accountIcon.Text = "dmIcon2";
            this.infoToolTip.SetToolTip(this.accountIcon, "复制账号");
            this.accountIcon.Click += new System.EventHandler(this.accountIcon_Click);
            this.accountIcon.MouseDown += new System.Windows.Forms.MouseEventHandler(this.accountIcon_MouseDown);
            this.accountIcon.MouseEnter += new System.EventHandler(this.accountIcon_MouseEnter);
            this.accountIcon.MouseLeave += new System.EventHandler(this.accountIcon_MouseLeave);
            // 
            // showPwIcon
            // 
            this.showPwIcon.BackColor = System.Drawing.Color.Transparent;
            this.showPwIcon.Cursor = System.Windows.Forms.Cursors.Hand;
            this.showPwIcon.DM_Color = System.Drawing.SystemColors.ScrollBar;
            this.showPwIcon.DM_Font_Size = 12F;
            this.showPwIcon.DM_Key = DMSkin.Controls.DM.DMIcon.DMIconKey.雾;
            this.showPwIcon.DM_Text = "";
            this.showPwIcon.Enabled = false;
            this.showPwIcon.Location = new System.Drawing.Point(318, 121);
            this.showPwIcon.Name = "showPwIcon";
            this.showPwIcon.Size = new System.Drawing.Size(20, 18);
            this.showPwIcon.TabIndex = 18;
            this.showPwIcon.Text = "dmIcon1";
            this.infoToolTip.SetToolTip(this.showPwIcon, "明文查看");
            this.showPwIcon.Click += new System.EventHandler(this.showPwIcon_Click);
            this.showPwIcon.MouseDown += new System.Windows.Forms.MouseEventHandler(this.showPwIcon_MouseDown);
            this.showPwIcon.MouseUp += new System.Windows.Forms.MouseEventHandler(this.showPwIcon_MouseUp);
            // 
            // openIcon
            // 
            this.openIcon.BackColor = System.Drawing.Color.Transparent;
            this.openIcon.Cursor = System.Windows.Forms.Cursors.Hand;
            this.openIcon.DM_Color = System.Drawing.SystemColors.ScrollBar;
            this.openIcon.DM_Font_Size = 12F;
            this.openIcon.DM_Key = DMSkin.Controls.DM.DMIcon.DMIconKey.地图;
            this.openIcon.DM_Text = "";
            this.openIcon.Enabled = false;
            this.openIcon.Location = new System.Drawing.Point(318, 76);
            this.openIcon.Name = "openIcon";
            this.openIcon.Size = new System.Drawing.Size(20, 18);
            this.openIcon.TabIndex = 19;
            this.openIcon.Text = "dmIcon1";
            this.infoToolTip.SetToolTip(this.openIcon, "立即访问");
            this.openIcon.Click += new System.EventHandler(this.openIcon_Click);
            this.openIcon.MouseDown += new System.Windows.Forms.MouseEventHandler(this.openIcon_MouseDown);
            this.openIcon.MouseUp += new System.Windows.Forms.MouseEventHandler(this.openIcon_MouseUp);
            // 
            // accountTextBox
            // 
            this.accountTextBox.BackColor = System.Drawing.Color.Snow;
            this.accountTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.accountTextBox.Enabled = false;
            this.accountTextBox.Font = new System.Drawing.Font("微软雅黑", 14F);
            this.accountTextBox.ForeColor = System.Drawing.Color.LightSlateGray;
            this.accountTextBox.Location = new System.Drawing.Point(80, 72);
            this.accountTextBox.Name = "accountTextBox";
            this.accountTextBox.ReadOnly = true;
            this.accountTextBox.Size = new System.Drawing.Size(201, 25);
            this.accountTextBox.TabIndex = 4;
            this.accountTextBox.TabStop = false;
            this.accountTextBox.Text = "N/A\r\n";
            this.accountTextBox.WaterText = "";
            this.accountTextBox.MouseDown += new System.Windows.Forms.MouseEventHandler(this.accountTextBox_MouseDown);
            // 
            // pwTextBox
            // 
            this.pwTextBox.BackColor = System.Drawing.Color.Snow;
            this.pwTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.pwTextBox.Enabled = false;
            this.pwTextBox.Font = new System.Drawing.Font("微软雅黑", 14F);
            this.pwTextBox.ForeColor = System.Drawing.Color.LightSlateGray;
            this.pwTextBox.Location = new System.Drawing.Point(80, 117);
            this.pwTextBox.Name = "pwTextBox";
            this.pwTextBox.ReadOnly = true;
            this.pwTextBox.Size = new System.Drawing.Size(201, 25);
            this.pwTextBox.TabIndex = 9;
            this.pwTextBox.TabStop = false;
            this.pwTextBox.Text = "N/A\r\n";
            this.pwTextBox.WaterText = "";
            this.pwTextBox.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pwTextBox_MouseDown);
            // 
            // descTextBox
            // 
            this.descTextBox.BackColor = System.Drawing.Color.Snow;
            this.descTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.descTextBox.Enabled = false;
            this.descTextBox.Font = new System.Drawing.Font("华文楷体", 13F);
            this.descTextBox.ForeColor = System.Drawing.Color.LightSteelBlue;
            this.descTextBox.Location = new System.Drawing.Point(80, 172);
            this.descTextBox.Multiline = true;
            this.descTextBox.Name = "descTextBox";
            this.descTextBox.ReadOnly = true;
            this.descTextBox.Size = new System.Drawing.Size(258, 78);
            this.descTextBox.TabIndex = 10;
            this.descTextBox.TabStop = false;
            this.descTextBox.Text = "N/A";
            this.descTextBox.WaterText = "";
            this.descTextBox.MouseDown += new System.Windows.Forms.MouseEventHandler(this.descTextBox_MouseDown);
            // 
            // timeIcon
            // 
            this.timeIcon.BackColor = System.Drawing.Color.Transparent;
            this.timeIcon.DM_Color = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.timeIcon.DM_Font_Size = 10F;
            this.timeIcon.DM_Key = DMSkin.Controls.DM.DMIcon.DMIconKey.时间;
            this.timeIcon.DM_Text = "  0000-00-00 00:00";
            this.timeIcon.Location = new System.Drawing.Point(204, 256);
            this.timeIcon.Name = "timeIcon";
            this.timeIcon.Size = new System.Drawing.Size(134, 18);
            this.timeIcon.TabIndex = 14;
            this.timeIcon.Text = "dmIcon1";
            // 
            // OneForm
            // 
            this.AcceptButton = this.closeButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Snow;
            this.CancelButton = this.closeButton;
            this.ClientSize = new System.Drawing.Size(361, 288);
            this.Controls.Add(this.openIcon);
            this.Controls.Add(this.showPwIcon);
            this.Controls.Add(this.accountIcon);
            this.Controls.Add(this.pwIcon);
            this.Controls.Add(this.timeIcon);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.descTextBox);
            this.Controls.Add(this.pwTextBox);
            this.Controls.Add(this.accountTextBox);
            this.Controls.Add(this.title);
            this.Controls.Add(this.topIcon);
            this.Controls.Add(this.dmButtonClose1);
            this.Controls.Add(this.closeButton);
            this.DM_CanResize = false;
            this.DM_howBorder = false;
            this.DM_Mobile = DMSkin.MobileStyle.TitleMobile;
            this.DM_Radius = 3;
            this.DM_ShadowColor = System.Drawing.Color.DarkSeaGreen;
            this.DM_ShadowWidth = 10;
            this.Name = "OneForm";
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "OneForm";
            this.TransparencyKey = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.Load += new System.EventHandler(this.OneForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DMSkin.Controls.DMButtonClose dmButtonClose1;
        private DMSkin.Controls.DMButton closeButton;
        private DMSkin.Controls.DM.DMIcon topIcon;
        private System.Windows.Forms.ToolTip infoToolTip;
        private System.Windows.Forms.Label title;
        private DMSkin.Controls.DMTextBox accountTextBox;
        private DMSkin.Controls.DMTextBox pwTextBox;
        private DMSkin.Controls.DMTextBox descTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private DMSkin.Controls.DM.DMIcon timeIcon;
        private DMSkin.Controls.DM.DMIcon pwIcon;
        private DMSkin.Controls.DM.DMIcon accountIcon;
        private DMSkin.Controls.DM.DMIcon showPwIcon;
        private DMSkin.Controls.DM.DMIcon openIcon;
    }
}