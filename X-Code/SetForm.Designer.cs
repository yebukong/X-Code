﻿namespace X_Code
{
    partial class SetForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.setButClose = new DMSkin.Controls.DMButtonClose();
            this.titleIcon = new DMSkin.Controls.DM.DMIcon();
            this.setTabControl = new DMSkin.Controls.DMTabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.descTextBox = new DMSkin.Controls.DMTextBox();
            this.nameTextBox = new DMSkin.Controls.DMTextBox();
            this.nameShowToggle = new DMSkin.Metro.Controls.MetroToggle();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.pwShowTypeComboBox = new DMSkin.Metro.Controls.MetroComboBox();
            this.itemsShowTypeComboBox = new DMSkin.Metro.Controls.MetroComboBox();
            this.quicklyCloseToggle = new DMSkin.Metro.Controls.MetroToggle();
            this.label7 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.label13 = new System.Windows.Forms.Label();
            this.newPwIcon = new DMSkin.Controls.DM.DMIcon();
            this.reNewPwIcon = new DMSkin.Controls.DM.DMIcon();
            this.oldPwIcon = new DMSkin.Controls.DM.DMIcon();
            this.againPwTextBox = new DMSkin.Controls.DMTextBox();
            this.newPwTextBox = new DMSkin.Controls.DMTextBox();
            this.oldPwTextBox = new DMSkin.Controls.DMTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.fromshadowToggle = new DMSkin.Metro.Controls.MetroToggle();
            this.formAnimateToggle = new DMSkin.Metro.Controls.MetroToggle();
            this.formOpacityBar = new DMSkin.Metro.Controls.MetroTrackBar();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.cancelButton = new DMSkin.Controls.DMButton();
            this.okButton = new DMSkin.Controls.DMButton();
            this.infoToolTip = new System.Windows.Forms.ToolTip(this.components);
            this.infoLabel = new System.Windows.Forms.Label();
            this.messageTimer = new System.Windows.Forms.Timer(this.components);
            this.yesButton = new DMSkin.Controls.DMButton();
            this.setTabControl.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.SuspendLayout();
            // 
            // setButClose
            // 
            this.setButClose.BackColor = System.Drawing.Color.Transparent;
            this.setButClose.Dock = System.Windows.Forms.DockStyle.Right;
            this.setButClose.Location = new System.Drawing.Point(419, 4);
            this.setButClose.MaximumSize = new System.Drawing.Size(30, 27);
            this.setButClose.MinimumSize = new System.Drawing.Size(30, 27);
            this.setButClose.Name = "setButClose";
            this.setButClose.Size = new System.Drawing.Size(30, 27);
            this.setButClose.TabIndex = 0;
            this.setButClose.Click += new System.EventHandler(this.dmButtonClose1_Click);
            // 
            // titleIcon
            // 
            this.titleIcon.BackColor = System.Drawing.Color.Transparent;
            this.titleIcon.DM_Color = System.Drawing.Color.DarkCyan;
            this.titleIcon.DM_Font_Size = 18F;
            this.titleIcon.DM_Key = DMSkin.Controls.DM.DMIcon.DMIconKey.设置;
            this.titleIcon.DM_Text = " 设置";
            this.titleIcon.Location = new System.Drawing.Point(8, 22);
            this.titleIcon.Name = "titleIcon";
            this.titleIcon.Size = new System.Drawing.Size(135, 27);
            this.titleIcon.TabIndex = 1;
            this.titleIcon.Text = "titleIcon";
            // 
            // setTabControl
            // 
            this.setTabControl.Alignment = System.Windows.Forms.TabAlignment.Left;
            this.setTabControl.BackColor = System.Drawing.Color.Transparent;
            this.setTabControl.Controls.Add(this.tabPage1);
            this.setTabControl.Controls.Add(this.tabPage2);
            this.setTabControl.Controls.Add(this.tabPage3);
            this.setTabControl.Controls.Add(this.tabPage4);
            this.setTabControl.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.setTabControl.DMHeight = 3;
            this.setTabControl.DMNormalBackColor = System.Drawing.SystemColors.Info;
            this.setTabControl.Font = new System.Drawing.Font("微软雅黑 Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.setTabControl.ItemSize = new System.Drawing.Size(40, 100);
            this.setTabControl.Location = new System.Drawing.Point(0, 55);
            this.setTabControl.Multiline = true;
            this.setTabControl.Name = "setTabControl";
            this.setTabControl.SelectedIndex = 0;
            this.setTabControl.Size = new System.Drawing.Size(431, 200);
            this.setTabControl.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.setTabControl.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tabPage1.Controls.Add(this.descTextBox);
            this.tabPage1.Controls.Add(this.nameTextBox);
            this.tabPage1.Controls.Add(this.nameShowToggle);
            this.tabPage1.Controls.Add(this.label6);
            this.tabPage1.Controls.Add(this.label5);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Font = new System.Drawing.Font("微软雅黑 Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tabPage1.Location = new System.Drawing.Point(104, 4);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(323, 192);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "用户";
            this.tabPage1.Click += new System.EventHandler(this.tabPage1_Click);
            // 
            // descTextBox
            // 
            this.descTextBox.BackColor = System.Drawing.Color.Snow;
            this.descTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.descTextBox.Font = new System.Drawing.Font("微软雅黑", 11F);
            this.descTextBox.ForeColor = System.Drawing.Color.LightSlateGray;
            this.descTextBox.Location = new System.Drawing.Point(123, 114);
            this.descTextBox.Multiline = true;
            this.descTextBox.Name = "descTextBox";
            this.descTextBox.Size = new System.Drawing.Size(194, 72);
            this.descTextBox.TabIndex = 25;
            this.descTextBox.TabStop = false;
            this.descTextBox.Text = "N/A";
            this.descTextBox.WaterText = "";
            this.descTextBox.Leave += new System.EventHandler(this.descTextBox_Leave);
            // 
            // nameTextBox
            // 
            this.nameTextBox.BackColor = System.Drawing.Color.Snow;
            this.nameTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.nameTextBox.Font = new System.Drawing.Font("微软雅黑", 13F);
            this.nameTextBox.ForeColor = System.Drawing.Color.LightSlateGray;
            this.nameTextBox.Location = new System.Drawing.Point(123, 14);
            this.nameTextBox.Name = "nameTextBox";
            this.nameTextBox.Size = new System.Drawing.Size(194, 23);
            this.nameTextBox.TabIndex = 6;
            this.nameTextBox.TabStop = false;
            this.nameTextBox.Text = "N/A\r\n";
            this.nameTextBox.WaterText = "";
            this.nameTextBox.Leave += new System.EventHandler(this.nameTextBox_Leave);
            // 
            // nameShowToggle
            // 
            this.nameShowToggle.AutoSize = true;
            this.nameShowToggle.Cursor = System.Windows.Forms.Cursors.Hand;
            this.nameShowToggle.DM_UseCustomBackColor = true;
            this.nameShowToggle.DM_UseSelectable = true;
            this.nameShowToggle.Location = new System.Drawing.Point(123, 63);
            this.nameShowToggle.Name = "nameShowToggle";
            this.nameShowToggle.Size = new System.Drawing.Size(80, 25);
            this.nameShowToggle.TabIndex = 24;
            this.nameShowToggle.Text = "Off";
            this.nameShowToggle.CheckedChanged += new System.EventHandler(this.nameShowToggle_CheckedChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("幼圆", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label6.ForeColor = System.Drawing.Color.LightSteelBlue;
            this.label6.Location = new System.Drawing.Point(23, 113);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(51, 19);
            this.label6.TabIndex = 18;
            this.label6.Text = "说明";
            this.infoToolTip.SetToolTip(this.label6, "用户说明");
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("幼圆", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label5.ForeColor = System.Drawing.Color.LightSteelBlue;
            this.label5.Location = new System.Drawing.Point(24, 67);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(84, 18);
            this.label5.TabIndex = 17;
            this.label5.Text = "用户显示";
            this.infoToolTip.SetToolTip(this.label5, "用于选择用户名是否在登录界面/主界面显示");
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("幼圆", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label4.ForeColor = System.Drawing.Color.LightSteelBlue;
            this.label4.Location = new System.Drawing.Point(24, 18);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 18);
            this.label4.TabIndex = 16;
            this.label4.Text = "用户名";
            this.infoToolTip.SetToolTip(this.label4, "用户名");
            // 
            // tabPage2
            // 
            this.tabPage2.AutoScroll = true;
            this.tabPage2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tabPage2.Controls.Add(this.pwShowTypeComboBox);
            this.tabPage2.Controls.Add(this.itemsShowTypeComboBox);
            this.tabPage2.Controls.Add(this.quicklyCloseToggle);
            this.tabPage2.Controls.Add(this.label7);
            this.tabPage2.Controls.Add(this.label12);
            this.tabPage2.Controls.Add(this.label8);
            this.tabPage2.Location = new System.Drawing.Point(104, 4);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(323, 192);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "X-Code";
            this.tabPage2.Click += new System.EventHandler(this.tabPage2_Click);
            // 
            // pwShowTypeComboBox
            // 
            this.pwShowTypeComboBox.DM_UseSelectable = true;
            this.pwShowTypeComboBox.FormattingEnabled = true;
            this.pwShowTypeComboBox.ItemHeight = 24;
            this.pwShowTypeComboBox.Items.AddRange(new object[] {
            "按下弹起切换",
            "点击切换"});
            this.pwShowTypeComboBox.Location = new System.Drawing.Point(123, 62);
            this.pwShowTypeComboBox.Name = "pwShowTypeComboBox";
            this.pwShowTypeComboBox.Size = new System.Drawing.Size(170, 30);
            this.pwShowTypeComboBox.TabIndex = 26;
            this.pwShowTypeComboBox.TabStop = false;
            this.pwShowTypeComboBox.SelectedIndexChanged += new System.EventHandler(this.pwShowTypeComboBox_SelectedIndexChanged);
            // 
            // itemsShowTypeComboBox
            // 
            this.itemsShowTypeComboBox.DM_UseSelectable = true;
            this.itemsShowTypeComboBox.FormattingEnabled = true;
            this.itemsShowTypeComboBox.ItemHeight = 24;
            this.itemsShowTypeComboBox.Items.AddRange(new object[] {
            "简单模式",
            "炒鸡模式(未实现)"});
            this.itemsShowTypeComboBox.Location = new System.Drawing.Point(123, 13);
            this.itemsShowTypeComboBox.Name = "itemsShowTypeComboBox";
            this.itemsShowTypeComboBox.Size = new System.Drawing.Size(170, 30);
            this.itemsShowTypeComboBox.TabIndex = 25;
            this.itemsShowTypeComboBox.TabStop = false;
            this.itemsShowTypeComboBox.SelectedIndexChanged += new System.EventHandler(this.itemsShowTypeComboBox_SelectedIndexChanged);
            // 
            // quicklyCloseToggle
            // 
            this.quicklyCloseToggle.AutoSize = true;
            this.quicklyCloseToggle.Cursor = System.Windows.Forms.Cursors.Hand;
            this.quicklyCloseToggle.DM_UseCustomBackColor = true;
            this.quicklyCloseToggle.DM_UseSelectable = true;
            this.quicklyCloseToggle.Location = new System.Drawing.Point(123, 113);
            this.quicklyCloseToggle.Name = "quicklyCloseToggle";
            this.quicklyCloseToggle.Size = new System.Drawing.Size(80, 25);
            this.quicklyCloseToggle.TabIndex = 24;
            this.quicklyCloseToggle.TabStop = false;
            this.quicklyCloseToggle.Text = "Off";
            this.quicklyCloseToggle.CheckedChanged += new System.EventHandler(this.quicklyCloseToggle_CheckedChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("幼圆", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label7.ForeColor = System.Drawing.Color.LightSteelBlue;
            this.label7.Location = new System.Drawing.Point(24, 113);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(84, 18);
            this.label7.TabIndex = 20;
            this.label7.Text = "快速关闭";
            this.infoToolTip.SetToolTip(this.label7, "是否点击空白快速关闭密码项显示窗口");
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("幼圆", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label12.ForeColor = System.Drawing.Color.LightSteelBlue;
            this.label12.Location = new System.Drawing.Point(24, 67);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(84, 18);
            this.label12.TabIndex = 19;
            this.label12.Text = "密码查看";
            this.infoToolTip.SetToolTip(this.label12, "选择密码明文显示的方式(不适用与登录界面)");
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("幼圆", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label8.ForeColor = System.Drawing.Color.LightSteelBlue;
            this.label8.Location = new System.Drawing.Point(24, 18);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(84, 18);
            this.label8.TabIndex = 18;
            this.label8.Text = "列表展示";
            this.infoToolTip.SetToolTip(this.label8, "选择密码项显示列表的模式");
            // 
            // tabPage3
            // 
            this.tabPage3.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tabPage3.Controls.Add(this.label13);
            this.tabPage3.Controls.Add(this.newPwIcon);
            this.tabPage3.Controls.Add(this.reNewPwIcon);
            this.tabPage3.Controls.Add(this.oldPwIcon);
            this.tabPage3.Controls.Add(this.againPwTextBox);
            this.tabPage3.Controls.Add(this.newPwTextBox);
            this.tabPage3.Controls.Add(this.oldPwTextBox);
            this.tabPage3.Controls.Add(this.label3);
            this.tabPage3.Controls.Add(this.label2);
            this.tabPage3.Controls.Add(this.label1);
            this.tabPage3.Location = new System.Drawing.Point(104, 4);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(323, 192);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "密码修改";
            this.tabPage3.Click += new System.EventHandler(this.tabPage3_Click);
            // 
            // label13
            // 
            this.label13.Font = new System.Drawing.Font("楷体", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label13.ForeColor = System.Drawing.Color.DarkGray;
            this.label13.Location = new System.Drawing.Point(3, 151);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(317, 45);
            this.label13.TabIndex = 22;
            this.label13.Text = "点击查看初始密码";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label13.Click += new System.EventHandler(this.label13_Click);
            this.label13.MouseEnter += new System.EventHandler(this.label13_MouseEnter);
            // 
            // newPwIcon
            // 
            this.newPwIcon.BackColor = System.Drawing.Color.Transparent;
            this.newPwIcon.Cursor = System.Windows.Forms.Cursors.Hand;
            this.newPwIcon.DM_Color = System.Drawing.Color.OrangeRed;
            this.newPwIcon.DM_Font_Size = 12F;
            this.newPwIcon.DM_Key = DMSkin.Controls.DM.DMIcon.DMIconKey.雾;
            this.newPwIcon.DM_Text = "";
            this.newPwIcon.Location = new System.Drawing.Point(303, 67);
            this.newPwIcon.Name = "newPwIcon";
            this.newPwIcon.Size = new System.Drawing.Size(20, 18);
            this.newPwIcon.TabIndex = 21;
            this.newPwIcon.Text = "dmIcon1";
            this.infoToolTip.SetToolTip(this.newPwIcon, "明文查看");
            this.newPwIcon.Click += new System.EventHandler(this.newPwIcon_Click);
            this.newPwIcon.MouseDown += new System.Windows.Forms.MouseEventHandler(this.newPwIcon_MouseDown);
            this.newPwIcon.MouseEnter += new System.EventHandler(this.newPwIcon_MouseEnter);
            this.newPwIcon.MouseLeave += new System.EventHandler(this.newPwIcon_MouseLeave);
            this.newPwIcon.MouseUp += new System.Windows.Forms.MouseEventHandler(this.newPwIcon_MouseUp);
            // 
            // reNewPwIcon
            // 
            this.reNewPwIcon.BackColor = System.Drawing.Color.Transparent;
            this.reNewPwIcon.Cursor = System.Windows.Forms.Cursors.Hand;
            this.reNewPwIcon.DM_Color = System.Drawing.Color.OrangeRed;
            this.reNewPwIcon.DM_Font_Size = 12F;
            this.reNewPwIcon.DM_Key = DMSkin.Controls.DM.DMIcon.DMIconKey.雾;
            this.reNewPwIcon.DM_Text = "";
            this.reNewPwIcon.Location = new System.Drawing.Point(303, 114);
            this.reNewPwIcon.Name = "reNewPwIcon";
            this.reNewPwIcon.Size = new System.Drawing.Size(20, 18);
            this.reNewPwIcon.TabIndex = 20;
            this.reNewPwIcon.Text = "dmIcon1";
            this.infoToolTip.SetToolTip(this.reNewPwIcon, "明文查看");
            this.reNewPwIcon.Click += new System.EventHandler(this.reNewPwIcon_Click);
            this.reNewPwIcon.MouseDown += new System.Windows.Forms.MouseEventHandler(this.reNewPwIcon_MouseDown);
            this.reNewPwIcon.MouseEnter += new System.EventHandler(this.reNewPwIcon_MouseEnter);
            this.reNewPwIcon.MouseLeave += new System.EventHandler(this.reNewPwIcon_MouseLeave);
            this.reNewPwIcon.MouseUp += new System.Windows.Forms.MouseEventHandler(this.reNewPwIcon_MouseUp);
            // 
            // oldPwIcon
            // 
            this.oldPwIcon.BackColor = System.Drawing.Color.Transparent;
            this.oldPwIcon.Cursor = System.Windows.Forms.Cursors.Hand;
            this.oldPwIcon.DM_Color = System.Drawing.Color.OrangeRed;
            this.oldPwIcon.DM_Font_Size = 12F;
            this.oldPwIcon.DM_Key = DMSkin.Controls.DM.DMIcon.DMIconKey.雾;
            this.oldPwIcon.DM_Text = "";
            this.oldPwIcon.Location = new System.Drawing.Point(303, 18);
            this.oldPwIcon.Name = "oldPwIcon";
            this.oldPwIcon.Size = new System.Drawing.Size(20, 18);
            this.oldPwIcon.TabIndex = 19;
            this.oldPwIcon.Text = "dmIcon1";
            this.infoToolTip.SetToolTip(this.oldPwIcon, "明文查看");
            this.oldPwIcon.Click += new System.EventHandler(this.oldPwIcon_Click);
            this.oldPwIcon.MouseDown += new System.Windows.Forms.MouseEventHandler(this.oldPwIcon_MouseDown);
            this.oldPwIcon.MouseEnter += new System.EventHandler(this.oldPwIcon_MouseEnter);
            this.oldPwIcon.MouseLeave += new System.EventHandler(this.oldPwIcon_MouseLeave);
            this.oldPwIcon.MouseUp += new System.Windows.Forms.MouseEventHandler(this.oldPwIcon_MouseUp);
            // 
            // againPwTextBox
            // 
            this.againPwTextBox.BackColor = System.Drawing.Color.Snow;
            this.againPwTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.againPwTextBox.Enabled = false;
            this.againPwTextBox.Font = new System.Drawing.Font("微软雅黑", 13F);
            this.againPwTextBox.ForeColor = System.Drawing.Color.LightSlateGray;
            this.againPwTextBox.Location = new System.Drawing.Point(123, 107);
            this.againPwTextBox.Name = "againPwTextBox";
            this.againPwTextBox.Size = new System.Drawing.Size(171, 30);
            this.againPwTextBox.TabIndex = 18;
            this.againPwTextBox.TabStop = false;
            this.infoToolTip.SetToolTip(this.againPwTextBox, "重新输入新密码");
            this.againPwTextBox.UseSystemPasswordChar = true;
            this.againPwTextBox.WaterText = "";
            this.againPwTextBox.TextChanged += new System.EventHandler(this.againPwTextBox_TextChanged);
            this.againPwTextBox.Enter += new System.EventHandler(this.againPwTextBox_Enter);
            this.againPwTextBox.Leave += new System.EventHandler(this.againPwTextBox_Leave);
            // 
            // newPwTextBox
            // 
            this.newPwTextBox.BackColor = System.Drawing.Color.Snow;
            this.newPwTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.newPwTextBox.Enabled = false;
            this.newPwTextBox.Font = new System.Drawing.Font("微软雅黑", 13F);
            this.newPwTextBox.ForeColor = System.Drawing.Color.LightSlateGray;
            this.newPwTextBox.Location = new System.Drawing.Point(123, 61);
            this.newPwTextBox.Name = "newPwTextBox";
            this.newPwTextBox.Size = new System.Drawing.Size(171, 30);
            this.newPwTextBox.TabIndex = 17;
            this.newPwTextBox.TabStop = false;
            this.infoToolTip.SetToolTip(this.newPwTextBox, "输入新密码(3-20)");
            this.newPwTextBox.UseSystemPasswordChar = true;
            this.newPwTextBox.WaterText = "";
            this.newPwTextBox.TextChanged += new System.EventHandler(this.newPwTextBox_TextChanged);
            this.newPwTextBox.Enter += new System.EventHandler(this.newPwTextBox_Enter);
            this.newPwTextBox.Leave += new System.EventHandler(this.newPwTextBox_Leave);
            // 
            // oldPwTextBox
            // 
            this.oldPwTextBox.BackColor = System.Drawing.Color.Snow;
            this.oldPwTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.oldPwTextBox.Font = new System.Drawing.Font("微软雅黑", 13F);
            this.oldPwTextBox.ForeColor = System.Drawing.Color.LightSlateGray;
            this.oldPwTextBox.Location = new System.Drawing.Point(123, 12);
            this.oldPwTextBox.Name = "oldPwTextBox";
            this.oldPwTextBox.Size = new System.Drawing.Size(171, 30);
            this.oldPwTextBox.TabIndex = 16;
            this.oldPwTextBox.TabStop = false;
            this.infoToolTip.SetToolTip(this.oldPwTextBox, "输入旧密码");
            this.oldPwTextBox.UseSystemPasswordChar = true;
            this.oldPwTextBox.WaterText = "";
            this.oldPwTextBox.TextChanged += new System.EventHandler(this.oldPwTextBox_TextChanged);
            this.oldPwTextBox.Leave += new System.EventHandler(this.oldPwTextBox_Leave);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("幼圆", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label3.ForeColor = System.Drawing.Color.LightSteelBlue;
            this.label3.Location = new System.Drawing.Point(24, 18);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 18);
            this.label3.TabIndex = 15;
            this.label3.Text = "旧密码";
            this.infoToolTip.SetToolTip(this.label3, "旧密码");
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("幼圆", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label2.ForeColor = System.Drawing.Color.LightSteelBlue;
            this.label2.Location = new System.Drawing.Point(24, 67);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 18);
            this.label2.TabIndex = 14;
            this.label2.Text = "新密码";
            this.infoToolTip.SetToolTip(this.label2, "新密码");
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("幼圆", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.ForeColor = System.Drawing.Color.LightSteelBlue;
            this.label1.Location = new System.Drawing.Point(24, 113);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 18);
            this.label1.TabIndex = 13;
            this.label1.Text = "再一次";
            this.infoToolTip.SetToolTip(this.label1, "再输一次新密码");
            // 
            // tabPage4
            // 
            this.tabPage4.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tabPage4.Controls.Add(this.fromshadowToggle);
            this.tabPage4.Controls.Add(this.formAnimateToggle);
            this.tabPage4.Controls.Add(this.formOpacityBar);
            this.tabPage4.Controls.Add(this.label11);
            this.tabPage4.Controls.Add(this.label10);
            this.tabPage4.Controls.Add(this.label9);
            this.tabPage4.Location = new System.Drawing.Point(104, 4);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(323, 192);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "其他";
            this.tabPage4.Click += new System.EventHandler(this.tabPage4_Click);
            // 
            // fromshadowToggle
            // 
            this.fromshadowToggle.AutoSize = true;
            this.fromshadowToggle.Cursor = System.Windows.Forms.Cursors.Hand;
            this.fromshadowToggle.DM_UseCustomBackColor = true;
            this.fromshadowToggle.DM_UseSelectable = true;
            this.fromshadowToggle.Location = new System.Drawing.Point(135, 63);
            this.fromshadowToggle.Name = "fromshadowToggle";
            this.fromshadowToggle.Size = new System.Drawing.Size(80, 25);
            this.fromshadowToggle.TabIndex = 23;
            this.fromshadowToggle.TabStop = false;
            this.fromshadowToggle.Text = "Off";
            this.fromshadowToggle.CheckedChanged += new System.EventHandler(this.fromshadowToggle_CheckedChanged);
            // 
            // formAnimateToggle
            // 
            this.formAnimateToggle.AutoSize = true;
            this.formAnimateToggle.Cursor = System.Windows.Forms.Cursors.Hand;
            this.formAnimateToggle.DM_UseCustomBackColor = true;
            this.formAnimateToggle.DM_UseSelectable = true;
            this.formAnimateToggle.Location = new System.Drawing.Point(135, 14);
            this.formAnimateToggle.Name = "formAnimateToggle";
            this.formAnimateToggle.Size = new System.Drawing.Size(80, 25);
            this.formAnimateToggle.TabIndex = 22;
            this.formAnimateToggle.TabStop = false;
            this.formAnimateToggle.Text = "Off";
            this.formAnimateToggle.CheckedChanged += new System.EventHandler(this.formAnimateToggle_CheckedChanged);
            // 
            // formOpacityBar
            // 
            this.formOpacityBar.BackColor = System.Drawing.Color.WhiteSmoke;
            this.formOpacityBar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.formOpacityBar.DM_UseCustomBackColor = true;
            this.formOpacityBar.LargeChange = 1;
            this.formOpacityBar.Location = new System.Drawing.Point(135, 113);
            this.formOpacityBar.MouseWheelBarPartitions = 100;
            this.formOpacityBar.Name = "formOpacityBar";
            this.formOpacityBar.Size = new System.Drawing.Size(173, 23);
            this.formOpacityBar.TabIndex = 21;
            this.formOpacityBar.TabStop = false;
            this.formOpacityBar.Text = "metroTrackBar1";
            this.formOpacityBar.Value = 100;
            this.formOpacityBar.ValueChanged += new System.EventHandler(this.formOpacityBar_ValueChanged);
            this.formOpacityBar.Leave += new System.EventHandler(this.formOpacityBar_Leave);
            this.formOpacityBar.MouseEnter += new System.EventHandler(this.formOpacityBar_MouseEnter);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("幼圆", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label11.ForeColor = System.Drawing.Color.LightSteelBlue;
            this.label11.Location = new System.Drawing.Point(24, 113);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(103, 18);
            this.label11.TabIndex = 20;
            this.label11.Text = "窗口透明度";
            this.infoToolTip.SetToolTip(this.label11, "选择窗口透明度");
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("幼圆", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label10.ForeColor = System.Drawing.Color.LightSteelBlue;
            this.label10.Location = new System.Drawing.Point(24, 67);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(84, 18);
            this.label10.TabIndex = 19;
            this.label10.Text = "窗口阴影";
            this.infoToolTip.SetToolTip(this.label10, "选择是否展示窗口阴影");
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("幼圆", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label9.ForeColor = System.Drawing.Color.LightSteelBlue;
            this.label9.Location = new System.Drawing.Point(24, 18);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(84, 18);
            this.label9.TabIndex = 18;
            this.label9.Text = "窗口动画";
            this.infoToolTip.SetToolTip(this.label9, "选择是否展示窗口动画");
            // 
            // cancelButton
            // 
            this.cancelButton.BackColor = System.Drawing.Color.Transparent;
            this.cancelButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelButton.DM_DisabledColor = System.Drawing.Color.Silver;
            this.cancelButton.DM_DownColor = System.Drawing.Color.LightSalmon;
            this.cancelButton.DM_MoveColor = System.Drawing.Color.PeachPuff;
            this.cancelButton.DM_NormalColor = System.Drawing.Color.LightPink;
            this.cancelButton.DM_Radius = 1;
            this.cancelButton.Font = new System.Drawing.Font("新宋体", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.cancelButton.ForeColor = System.Drawing.SystemColors.Info;
            this.cancelButton.Image = null;
            this.cancelButton.Location = new System.Drawing.Point(370, 272);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(60, 30);
            this.cancelButton.TabIndex = 4;
            this.cancelButton.Text = "取消";
            this.cancelButton.UseVisualStyleBackColor = false;
            // 
            // okButton
            // 
            this.okButton.BackColor = System.Drawing.Color.Transparent;
            this.okButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.okButton.DM_DisabledColor = System.Drawing.Color.LightGray;
            this.okButton.DM_DownColor = System.Drawing.Color.Chocolate;
            this.okButton.DM_MoveColor = System.Drawing.Color.DarkGoldenrod;
            this.okButton.DM_NormalColor = System.Drawing.Color.Goldenrod;
            this.okButton.DM_Radius = 1;
            this.okButton.Font = new System.Drawing.Font("新宋体", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.okButton.ForeColor = System.Drawing.SystemColors.Info;
            this.okButton.Image = null;
            this.okButton.Location = new System.Drawing.Point(304, 272);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(60, 30);
            this.okButton.TabIndex = 5;
            this.okButton.Text = "应用";
            this.okButton.UseVisualStyleBackColor = false;
            this.okButton.Click += new System.EventHandler(this.okButton_Click);
            // 
            // infoLabel
            // 
            this.infoLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.infoLabel.AutoSize = true;
            this.infoLabel.BackColor = System.Drawing.Color.DarkCyan;
            this.infoLabel.Font = new System.Drawing.Font("幼圆", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.infoLabel.ForeColor = System.Drawing.Color.Ivory;
            this.infoLabel.Location = new System.Drawing.Point(42, 21);
            this.infoLabel.Margin = new System.Windows.Forms.Padding(3);
            this.infoLabel.Name = "infoLabel";
            this.infoLabel.Padding = new System.Windows.Forms.Padding(5, 5, 10, 5);
            this.infoLabel.Size = new System.Drawing.Size(86, 25);
            this.infoLabel.TabIndex = 102;
            this.infoLabel.Text = "提示信息";
            this.infoLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.infoLabel.Visible = false;
            // 
            // messageTimer
            // 
            this.messageTimer.Interval = 1000;
            this.messageTimer.Tick += new System.EventHandler(this.messageTimer_Tick);
            // 
            // yesButton
            // 
            this.yesButton.BackColor = System.Drawing.Color.Transparent;
            this.yesButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.yesButton.DM_DisabledColor = System.Drawing.Color.LightGray;
            this.yesButton.DM_DownColor = System.Drawing.Color.FromArgb(((int)(((byte)(9)))), ((int)(((byte)(140)))), ((int)(((byte)(188)))));
            this.yesButton.DM_MoveColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(195)))), ((int)(((byte)(245)))));
            this.yesButton.DM_NormalColor = System.Drawing.Color.FromArgb(((int)(((byte)(9)))), ((int)(((byte)(163)))), ((int)(((byte)(220)))));
            this.yesButton.DM_Radius = 1;
            this.yesButton.Font = new System.Drawing.Font("新宋体", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.yesButton.ForeColor = System.Drawing.SystemColors.Info;
            this.yesButton.Image = null;
            this.yesButton.Location = new System.Drawing.Point(238, 272);
            this.yesButton.Name = "yesButton";
            this.yesButton.Size = new System.Drawing.Size(60, 30);
            this.yesButton.TabIndex = 103;
            this.yesButton.Text = "确定";
            this.yesButton.UseVisualStyleBackColor = false;
            this.yesButton.Click += new System.EventHandler(this.yesButton_Click);
            // 
            // SetForm
            // 
            this.AcceptButton = this.cancelButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.CancelButton = this.cancelButton;
            this.ClientSize = new System.Drawing.Size(453, 311);
            this.ControlBox = false;
            this.Controls.Add(this.yesButton);
            this.Controls.Add(this.infoLabel);
            this.Controls.Add(this.okButton);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.setTabControl);
            this.Controls.Add(this.titleIcon);
            this.Controls.Add(this.setButClose);
            this.DM_CanResize = false;
            this.DM_howBorder = false;
            this.DM_Radius = 3;
            this.DM_ShadowColor = System.Drawing.Color.DarkCyan;
            this.DM_ShadowWidth = 10;
            this.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SetForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "q";
            this.TransparencyKey = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.Load += new System.EventHandler(this.SetForm_Load);
            this.Click += new System.EventHandler(this.SetForm_Click);
            this.setTabControl.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DMSkin.Controls.DMButtonClose setButClose;
        private DMSkin.Controls.DM.DMIcon titleIcon;
        private DMSkin.Controls.DMTabControl setTabControl;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private DMSkin.Controls.DMButton cancelButton;
        private DMSkin.Controls.DMButton okButton;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label8;
        private DMSkin.Metro.Controls.MetroToggle fromshadowToggle;
        private DMSkin.Metro.Controls.MetroToggle formAnimateToggle;
        private DMSkin.Metro.Controls.MetroTrackBar formOpacityBar;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label12;
        private DMSkin.Metro.Controls.MetroToggle nameShowToggle;
        private System.Windows.Forms.Label label7;
        private DMSkin.Metro.Controls.MetroToggle quicklyCloseToggle;
        private DMSkin.Metro.Controls.MetroComboBox itemsShowTypeComboBox;
        private DMSkin.Metro.Controls.MetroComboBox pwShowTypeComboBox;
        private DMSkin.Controls.DMTextBox descTextBox;
        private DMSkin.Controls.DMTextBox nameTextBox;
        private DMSkin.Controls.DMTextBox againPwTextBox;
        private DMSkin.Controls.DMTextBox newPwTextBox;
        private DMSkin.Controls.DMTextBox oldPwTextBox;
        private System.Windows.Forms.ToolTip infoToolTip;
        private DMSkin.Controls.DM.DMIcon newPwIcon;
        private DMSkin.Controls.DM.DMIcon reNewPwIcon;
        private DMSkin.Controls.DM.DMIcon oldPwIcon;
        private System.Windows.Forms.Label infoLabel;
        private System.Windows.Forms.Timer messageTimer;
        private System.Windows.Forms.Label label13;
        private DMSkin.Controls.DMButton yesButton;
    }
}