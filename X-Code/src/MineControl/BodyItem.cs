﻿/********************************************************************************
** auth： Mine
** date： 2017/1/8 17:52:32
** desc： 尚未编写描述
** Ver.:  V1.0.0
*********************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DMSkin.Controls;
using System.Drawing;

namespace X_Code
{
    class BodyItem : DMSkin.Controls.DMControlItem
    {
        private Color mouseBackColor = Color.Gray;

        public Color MouseBackColor
        {
            get { return mouseBackColor; }
            set { mouseBackColor = value; }
        }

        public string Key { get; set; }
        public string Text { get; set; }
        public int Num { get; set; }
     
    }
}
