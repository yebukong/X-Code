﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Drawing2D;

namespace X_Code
{
    [ToolboxItem(true)]
    public partial class CodeControl : DMSkin.DMControl  //DMSkin.Controls.DMControl
    {

        public CodeControl()
        {
            InitializeComponent();
            this.SetStyle(
                ControlStyles.AllPaintingInWmPaint |
                ControlStyles.OptimizedDoubleBuffer |
                ControlStyles.ResizeRedraw |
                ControlStyles.Selectable |
                ControlStyles.DoubleBuffer |
                ControlStyles.SupportsTransparentBackColor |
                ControlStyles.UserPaint, true);
            this.SetStyle(ControlStyles.Opaque, false);
            base.BackColor = Color.Transparent;
            this.UpdateStyles();
            if (items == null)
            {
                items = new DMSkin.Controls.DMControl.DMControlItemCollection(this);
            }
        }

        [Editor(typeof(CommandCollectionEditor), typeof(System.Drawing.Design.UITypeEditor))]
        public override DMSkin.Controls.DMControl.DMControlItemCollection Items
        {
            get
            {
                if (items == null)
                    items = new DMControlItemCollection(this);
                return items;
            }
        }

       

        protected override void DrawItem(Graphics g, DMSkin.Controls.DMControlItem _item, Rectangle rectItem)
        {
            
            if (Items != null && Items.Count <= 0)
            {
                return;
            }
            //设置高质量插值法
            g.InterpolationMode = InterpolationMode.Bilinear;
            //设置高质量,低速度呈现平滑程度
            g.SmoothingMode = SmoothingMode.HighQuality;
            g.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAlias;

            if (_item is BodyItem)
            {
                BodyItem it = (BodyItem)_item;
                Rectangle rec = new Rectangle(it.Bounds.X, it.Bounds.Y, it.Bounds.Width, it.Bounds.Height + 1);
                if (m_mouseOnItem != null && m_mouseOnItem.Equals(it) && it.MouseBackColor != null)
                {
                    g.FillRectangle(new SolidBrush(it.MouseBackColor), it.Bounds);
                    g.FillRectangle(new SolidBrush(Color.FromArgb(243, 83, 54)), it.Bounds.X, it.Bounds.Y, 12, it.Bounds.Height);

                    g.DrawString(it.Num.ToString(), it.Font, new SolidBrush(Color.White), 20, it.Bounds.Y + 15);
                    g.DrawString(it.Text, it.Font, new SolidBrush(Color.White), 50, it.Bounds.Y + 15);
                    
                   
                }
                else
                {
                    if (one != null && one.Text.Equals(it.Text))
                    {
                        g.FillRectangle(new SolidBrush(it.MouseBackColor), it.Bounds);
                        g.FillRectangle(new SolidBrush(Color.FromArgb(243, 83, 54)), it.Bounds.X, it.Bounds.Y, 12, it.Bounds.Height);

                        g.DrawString(it.Num.ToString(), it.Font, new SolidBrush(Color.White), 20, it.Bounds.Y + 15);
                        g.DrawString(it.Text, it.Font, new SolidBrush(Color.White), 50, it.Bounds.Y + 15);
                        one = null;
                    }
                    else {

                        g.FillRectangle(new SolidBrush(Color.Transparent), it.Bounds);
                        g.DrawString(it.Num.ToString(), it.Font, new SolidBrush(Color.Black), 20, it.Bounds.Y + 15);
                        g.DrawString(it.Text, it.Font, new SolidBrush(Color.Black), 50, it.Bounds.Y + 15);

                       
                    }
                   
                      
              }
               
                //OrangeRed
                g.DrawLine(new Pen(Color.FromArgb(236, 236, 236)), it.Bounds.X, it.Bounds.Y, it.Bounds.X + it.Bounds.Width, it.Bounds.Y);
                g.DrawLine(new Pen(Color.FromArgb(236, 236, 236)), it.Bounds.X, it.Bounds.Y + it.Bounds.Height, it.Bounds.X + it.Bounds.Width, it.Bounds.Y + it.Bounds.Height);

            }
            else
            { 
                base.DrawItem(g, _item, rectItem);
            }
            
        }
        private BodyItem one=null;
        public event EventHandler ItemClick;
        protected override void OnClick(EventArgs e)
        {
            foreach (var item in Items)
            {
                if (item is BodyItem)
                {
                    BodyItem it = (BodyItem)item;
                    if (it.Bounds.Contains(m_ptMousePos))
                    {
                        if (ItemClick != null)
                        {
                            one = it;
                            //触发重绘
                            
                            ItemClick(it, e);
                            this.Invalidate();
                        }
                        break;
                    }

                }
                if (item is DMSkin.DMControlFileItem)
                {
                    DMSkin.DMControlFileItem it = (DMSkin.DMControlFileItem)item;
                    if (it.Bounds.Contains(m_ptMousePos))
                    {
                        if (ItemClick != null)
                        {

                            ItemClick(it, e);
                        }
                        break;
                    }

                }

            }
            base.OnClick(e);
        
        }

    }
}
