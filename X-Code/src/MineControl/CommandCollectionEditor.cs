﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Linq;
using System.Text;
//using DMSkin;

namespace X_Code
{
    public class CommandCollectionEditor : CollectionEditor
    {
        public CommandCollectionEditor(Type type)
            : base(type)
        { }
        protected override bool CanSelectMultipleInstances()
        {
            return true;
        }
        protected override Type[] CreateNewItemTypes()
        {
            return new Type[] { typeof(DMSkin.DMControlFileItem),  typeof(BodyItem)
            };
        }

        protected override object CreateInstance(Type itemType)
        {
            if (itemType == typeof(DMSkin.DMControlFileItem))
            {
                return (DMSkin.Controls.DMControlItem)(new DMSkin.DMControlFileItem());
            }
            if (itemType == typeof(BodyItem))
            {
                return (DMSkin.Controls.DMControlItem)(new BodyItem());
            }
            return null;
        }
    }
}
