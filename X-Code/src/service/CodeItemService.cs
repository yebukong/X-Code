﻿/********************************************************************************
** auth： Mine
** date： 2017/1/19 0:51:33
** desc： 密码项增删改查服务
** Ver.:  V1.0.0
*********************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace X_Code.service
{
    class CodeItemService
    {
        private DataService ds = null;
        private Dictionary<String, CodeItem> codeInfo = null;
        public CodeItemService() {
            if (ds == null)
            {
                ds = new DataService();
            }
        }
        
        /// <summary>
        /// 查询指定key
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public List<CodeItem> FindSomeByKey(String key)
        {
            codeInfo = ds.GetCodeInfo();
            if (codeInfo == null||codeInfo.Count()<1) {
                return null;
            }
            if (key == null||"".Equals(key)) {
                return codeInfo.Values.ToList();
            }
            List<CodeItem> target = new List<CodeItem>();

            List<String> keys = codeInfo.Keys.ToList();

            CodeItem one = null;
            String oneStr = null;
            foreach(String temp in keys){
                one = codeInfo[temp];
                oneStr = one.Title + "|" +one.Account+ "|" + one.Desc;
                //不区分大小写
                if (oneStr.ToUpper().IndexOf(key.ToUpper()) > -1) {
                    target.Add(one);
                }
            }
            return target;
        }
        /// <summary>
        /// 查询指定一条
        /// </summary>
        /// <returns></returns>
        public CodeItem FindOne(String title)
        {
            codeInfo = ds.GetCodeInfo();
            if (codeInfo == null || codeInfo.Count() < 1)
            {
                return null;
            }

            CodeItem one = null;
            codeInfo.TryGetValue(title,out one);
            
            return one;

        }
        /// <summary>
        /// 增加一条
        /// </summary>
        /// <param name="ci"></param>
        /// <returns></returns>
        public Boolean Add(CodeItem ci)
        {
            if (ci == null) {
                return false;
            }
            codeInfo = ds.GetCodeInfo();
            if (codeInfo == null )
            {
                codeInfo = new Dictionary<string, CodeItem>();
            }
            CodeItem one = null;
            if (codeInfo.TryGetValue(ci.Title,out one)) 
            {
                return false;
            }

            codeInfo.Add(ci.Title,ci);//添加
            return ds.UpdateCodeInfo(codeInfo);  //更新data并返回结果 
        }
        /// <summary>
        /// 更新一条
        /// </summary>
        /// <param name="ci"></param>
        /// <returns></returns>
        public Boolean Update(CodeItem ci)
        {
            if (ci == null)
            {
                return false;
            }
            codeInfo = ds.GetCodeInfo();
            if (codeInfo == null || codeInfo.Count() < 1)
            {
                return false;
            }

            CodeItem one = null;
            if (!codeInfo.TryGetValue(ci.Title, out one))
            {
                return false;
            }

            codeInfo[ci.Title] = ci;//更新
            return ds.UpdateCodeInfo(codeInfo);  //更新data并返回结果 
        }
        /// <summary>
        /// 删除数据指定title 密码数据
        /// </summary>
        /// <returns></returns>
        public Boolean Del(String title)
        {
            if (title == null || "".Equals(title)) {
                return false;
            }
            codeInfo = ds.GetCodeInfo();
            if (codeInfo == null || codeInfo.Count() < 1)
            {
                return false;
            }
            CodeItem one = null;
            if (!codeInfo.TryGetValue(title, out one))
            {
                return false;
            }
            if(codeInfo.Remove(title))//移除
            {
                return ds.UpdateCodeInfo(codeInfo);  //更新data并返回结果 
            }
            else 
            {
                return false;
            }

        }

        
    }
}
