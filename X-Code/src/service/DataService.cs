﻿/********************************************************************************
** auth： Mine
** date： 2017/1/18 0:23:42
** desc： 数据操作服务
** Ver.:  V1.0.0
*********************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using X_Code.io;
using X_Code.src.vo;
namespace X_Code.service
{
    class DataService
    {
        private MineDataIO dataIo = new MineDataIO();

        
        /// <summary>  
        /// 获取文件中的owner信息
        /// </summary>
        /// <returns></returns>
        public Owner GetOwner() {
            Dictionary<String, Object> data = dataIo.ReadWithDefaultFile();
            if (data != null) {
                Object temp = data[MineInfo.OWNER_KEY];
                //is操作符自动检查时局是否和类型兼容，并返回结果
                //as稍微不同，它检查引用对象是否兼容，如果不兼容则返回null，因此需要做null的判断
                if (temp != null && temp is Owner) {
                    return temp as Owner;
                }
            }
            return null;
        }
        /// <summary>
        /// 初始化文件中的owner信息
        /// </summary>
        /// <param name="owner"></param>
        /// <returns></returns>
        public Boolean InitOwner(Owner owner)
        {
            Dictionary<String, Object> data = dataIo.ReadWithDefaultFile();
            if (data == null)
            {
                data = new Dictionary<string, object>();
            }
            //通过设置 Dictionary 中不存在的键值，还可以使用 Item 属性添加新元素。设置属性值时
            //如果该键在 Dictionary 中，则分配的值将替换与该键关联的值。
            //如果该键不在 Dictionary 中，则将键和值添加到字典中。相比之下，Add 方法不修改现有元素。
            //data.Add(MineInfo.OWNER_KEY, owner);
            data[MineInfo.OWNER_KEY] = owner;
            return dataIo.WriteWithDefaultFile(data);
        }
        /// <summary>
        /// 更新文件中的owner信息
        /// </summary>
        /// <param name="owner"></param>
        /// <returns></returns>
        public Boolean UpdateOwner(Owner owner)
        {
            Dictionary<String, Object> data = dataIo.ReadWithDefaultFile();
            if (data == null)
            {
                return false;
            }

           // data.Add(MineInfo.OWNER_KEY,owner);
            data[MineInfo.OWNER_KEY] = owner;
            return dataIo.WriteWithDefaultFile(data);
        }

        /// <summary>
        /// 获取文件中的密码项Map信息
        /// </summary>
        /// <returns></returns>
        public Dictionary<String, CodeItem> GetCodeInfo()
        {
            Dictionary<String, Object> data = dataIo.ReadWithDefaultFile();
            if (data != null)
            {
                Object temp = null;
                data.TryGetValue(MineInfo.CODE_DATA_KEY,out temp);//尝试获取
                if (temp != null && temp is Dictionary<String, CodeItem>)
                {
                    return temp as Dictionary<String, CodeItem>;
                }
            }
            return null;
        }
        /// <summary>
        ///更新文件中的密码项信息 
        /// </summary>
        /// <param name="codeInfo"></param>
        /// <returns></returns>
        public Boolean UpdateCodeInfo(Dictionary<String, CodeItem> codeInfo)
        {
            Dictionary<String, Object> data = dataIo.ReadWithDefaultFile();
            if (data == null)
            {
                return false;//获取不到说明底层有问题
            }
           // data.Add(MineInfo.CODE_DATA_KEY, codeInfo);
           data[MineInfo.CODE_DATA_KEY] = codeInfo;
           return dataIo.WriteWithDefaultFile(data);
        }

        /// <summary>
        /// 获取文件中的密码生成选项
        /// </summary>
        /// <returns></returns>
        public MPWInfo GetMPWInfo()
        {
            Dictionary<String, Object> data = dataIo.ReadWithDefaultFile();
            if (data != null)
            {
                Object temp = null;
                data.TryGetValue(MineInfo.MPW_INFO, out temp);//尝试获取
                if (temp != null && temp is MPWInfo)
                {
                    return temp as MPWInfo;
                }
            }
            return null;
        }
        /// <summary>
        ///更新文件中的密码生成选项信息 
        /// </summary>
        /// <param name="codeInfo"></param>
        /// <returns></returns>
        public Boolean UpdateMPWInfo(MPWInfo mpwInfo)
        {
            Dictionary<String, Object> data = dataIo.ReadWithDefaultFile();
            if (data == null)
            {
                return false;//获取不到说明底层有问题
            }
            data[MineInfo.MPW_INFO] = mpwInfo;
            return dataIo.WriteWithDefaultFile(data);
        }


    }
}
