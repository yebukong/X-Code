﻿/********************************************************************************
** auth： Mine
** date： 2018/6/1 11:30:59
** desc： 尚未编写描述
** Ver.:  V1.0.0
*********************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace X_Code.src.vo
{
    [Serializable]
    class MPWInfo : System.Object, ICloneable
    {
        private String otherChars;//特殊字符

        private Int16 otherLen;//密码长度
        private Int16[] lenRadios;//长度单选框选中状态数组
        private Int16[] charChecks;//字符复选框选中状态数组

        public static MPWInfo CreatDefault() {
            MPWInfo one = new MPWInfo();
            one.charChecks=new Int16[]{1,1,1,1};
            one.lenRadios = new Int16[]{ 0, 1, 0, 0 };
            one.otherLen = 8;
            one.otherChars = ".";
            return one;
        }


        public String OtherChars
        {
            get { return otherChars; }
            set { otherChars = value; }
        }


        public Int16 OtherLen
        {
            get { return otherLen; }
            set { otherLen = value; }
        }


        public Int16[] LenRadios
        {
            get { return lenRadios; }
            set { lenRadios = value; }
        }


        public Int16[] CharChecks
        {
            get { return charChecks; }
            set { charChecks = value; }
        }
        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }
}
