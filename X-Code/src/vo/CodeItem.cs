﻿/********************************************************************************
** auth： Mine
** date： 2016/12/18 23:34:51
** desc： 账号项 类
** Ver.:  V1.0.0
*********************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace X_Code
{
    [Serializable]//表示可以序列化
   public  class CodeItem
    {
        private String title;//小标题 必填，作为key值 不能重复，长度暂时不限
        private String account;//账号 
        private String passWord;//密码 ,加密 及 解密在另外进行
        //private String urlPath;//账号填写相关地址 633 
        //private String exePath; //相关程序地址...

        private String openPath; //访问路径

        

        private DateTime addTime; //创建时间 
        private DateTime updateTime; //最后修改时间 
        private String desc;//说明 【邮箱/手机/其他】

        public String Title
        {
            get { return title; }
            set { title = value; }
        }

        public String Account  
        {
            get { return account; }
            set { account = value; }
        }

        public String PassWord
        {
            get { return passWord; }
            set 
            { 
                passWord = value;
                //调用加密方法
            }
        }

        public String OpenPath
        {
            get { return openPath; }
            set { openPath = value; }
        }

        public DateTime AddTime
        {
            get { return addTime; }
            set { addTime = value; }
        }
        public DateTime UpdateTime
        {
            get { return updateTime; }
            set { updateTime = value; }
        }
       

        public String Desc
        {
            get { return desc; }
            set { desc = value; }
        }

        public override string ToString()
        {
            return "标题：" + this.title + "\t说明：" + this.desc;
        }
        public string ShowInfo()
        {
            return "[" + string.Format("{0:yyyy-MM-dd HH:mm}", this.addTime) + "]  " + this.title;
        }
    }
}
