﻿/********************************************************************************
** auth： Mine
** date： 2016/12/14 23:07:16
** desc： 用来存放程序需要的一些静态变量 ：由于需要往码云上提交代码，所以把重要key保留在电脑吧
** Ver.:  V1.0.0
*********************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace X_Code
{
    class MineInfo
    {
        /// <summary>
        /// 缺省文件名
        /// </summary>
        public const String DEFAULT_FILE = "mine.data";
        /// <summary>
        /// 存放用户信息的key
        /// </summary>
        public const String OWNER_KEY = "OWNER_INFO";
        /// <summary>
        /// 存放密码数据的Key
        /// </summary>
        public const String CODE_DATA_KEY = "CODE_INFO";
        /// 存放【密码生成选项】的key
        /// </summary>
        public const String MPW_INFO = "MPW_INFO";
        /// <summary>
        /// 存放AES_CBC密码加密的Key  长度16
        /// </summary>
        public const String ENCRYPT_AES_CBC_PW_KEY = "";
        /// <summary>
        /// 存放AES_CBC密码加密的初始化向量 长度16
        /// </summary>
        public const String ENCRYPT_AES_CBC_PW_VI = "0123456789ABCDEF";
        /// <summary>
        /// 存放AES_CBC文本加密的Key  长度16
        /// </summary>
        public const String ENCRYPT_AES_CBC_FILE_KEY = "0123456789ABCDEF";
        /// <summary>
        /// 存放AES_CBC文本加密的初始化向量 长度16
        /// </summary>
        public const String ENCRYPT_AES_CBC_FILE_VI = "0123456789ABCDEF";
        /// <summary>
        /// 缺省密码
        /// </summary>
        public const String DEFAULT_PASSWORD = "10086";

        /// <summary>
        /// Primary
        /// </summary>
        public static Color 首要 = Color.FromArgb(40, 96, 144);
        /// <summary>
        /// Success
        /// </summary>
        public static Color 成功 = Color.FromArgb(68, 157, 68);
        /// <summary>
        /// Info
        /// </summary>
        public static Color 信息 = Color.FromArgb(49, 176, 213);
        /// <summary>
        /// Waring
        /// </summary>
        public static Color 警告 = Color.FromArgb(253, 151, 31);
        /// <summary>
        /// Danger
        /// </summary>
        public static Color 危险 = Color.FromArgb(201, 48, 44);
  }
}
