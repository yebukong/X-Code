﻿/********************************************************************************
** auth： Mine
** date： 2016/12/18 23:49:10
** desc： 用户信息类,用于存放密码,问题,等信息
** Ver.:  V1.0.0
*********************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace X_Code
{
    [Serializable]
    public class Owner :System.Object,ICloneable
    {
        private string name;//用户
        private string password;//密码
        private string question;//问题
        private string answer;//答案
        private string desc;//说明文字...

        private bool isShowName = false;// 是否在窗体上显示用户名
        private int itemsShowType = 0;// 列表展示模式 0 简单模式 1 其他 
        private int pwShowType = 0;//  明文密码查看模式  0 按下弹起 1 点击切换  
        private bool isQuicklyClose = true;// 是否点击空白处快速关闭密码项窗体 
        private bool isShowAnimate = true;// 显示窗口动画
        private double formOpacity = 1;// 显示窗口阴影
        private bool isShowShadow = true;// 窗体透明度

        /// <summary>
        /// 支持克隆
        /// </summary>
        /// <returns></returns>
        public object Clone()
        {
            return this.MemberwiseClone();
        }
        public static Owner CreatDefault()
        {
            Owner one = new Owner();
            
            one.name = "叶小空初号机";

            
            one.Password = EncryptUtils.getMineMD5(MineInfo.DEFAULT_PASSWORD);

            //密码问题功能未实现
            one.question = "Are you OK?";
            one.Answer = EncryptUtils.getMineMD5("你好吗?");



            one.desc = "取次花丛懒回顾，半缘修道半缘君。";

            one.isShowName = true;// 是否在窗体上显示用户名
            one.itemsShowType = 0;// 列表展示模式 0 简单模式 1 其他 
            one.pwShowType = 0;//  明文密码查看模式  0 按下弹起切换 1 点击切换  
            one.isQuicklyClose = true;// 是否点击空白处快速关闭密码项窗体 
            one.isShowAnimate = true;// 显示窗口动画
            one.isShowShadow = true;// 显示窗口阴影 
            one.formOpacity = 1;//窗体透明度

            return one;
        }
        
        public String Name
        {
            get { return name; }
            set { name = value; }
        }


        public String Question
        {
            get { return question; }
            set { question = value; }
        }


        public String Answer
        {
            get { return answer; }
            set { answer = value; }
        }


        public String Password
        {
            get { return password; }
            set { password = value; }
        }
        public String Desc
        {
            get { return desc; }
            set { desc = value; }
        }
        /// <summary>
        /// 是否在窗体上显示用户名
        /// </summary>
        public Boolean IsShowName
        {
            get { return isShowName; }
            set { isShowName = value; }
        }
        /// <summary>
        /// 列表展示模式 0 简单模式 1 其他 
        /// </summary>
        public int ItemsShowType
        {
            get { return itemsShowType; }
            set { itemsShowType = value; }
        }
        /// <summary>
        /// 明文密码查看模式  0 按下弹起 1 点击切换  
        /// </summary>
        public int PwShowType
        {
            get { return pwShowType; }
            set { pwShowType = value; }
        }
        /// <summary>
        /// 是否点击空白处快速关闭密码项窗体 
        /// </summary>
        public Boolean IsQuicklyClose
        {
            get { return isQuicklyClose; }
            set { isQuicklyClose = value; }
        }
        /// <summary>
        /// 显示窗口动画
        /// </summary>
        public Boolean IsShowAnimate
        {
            get { return isShowAnimate; }
            set { isShowAnimate = value; }
        }
        /// <summary>
        /// 显示窗口阴影
        /// </summary>
        public Boolean IsShowShadow
        {
            get { return isShowShadow; }
            set { isShowShadow = value; }
        }
        /// <summary>
        /// 窗体透明度
        /// </summary>
        public double FormOpacity
        {
            get { return formOpacity; }
            set { formOpacity = value; }
        }
       
        public override string ToString()
        {
            return "姓名：" + this.Name +
                "\t密码：" + this.Password +
                "\t问题：" + this.Question + 
                "\t答案：" + this.Answer +
                "\t说明：" + this.Desc;
        }
    }
}
