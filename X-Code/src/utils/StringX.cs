﻿/********************************************************************************
** auth： Mine
** date： 2018/6/1 10:08:03
** desc： 尚未编写描述
** Ver.:  V1.0.0
*********************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace X_Code.src.utils
{
    class StringX
    {
        /** 0-9 **/
        public static char[] BASE_NUMBERS = new char[] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };
        /** a-z **/
        public static char[] LOWER_CASE_ALPHABET = new char[] { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z' };
        /** A-Z **/
        public static char[] UPPER_CASE_ALPHABET = new char[] { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z' };
        private StringX()
        {
            throw new MemberAccessException("No StringX instances for you!");
        }

        /**
    * 随机生成指定长度的字符串，charPool为字符池,可指定多个
    */
        public static String randomForMultiCharPool(int len, params char[][] charPools)
        {
            if (len < 1)
            {
                throw new ArgumentException("非法的 len参数:" + len);
            }
            if (charPools == null || charPools.Length < 1)
            {
                throw new ArgumentException("空的 charPools参数");
            }
            // 计算字符池字符总个数,及统计字符池边界
            int poolCharSum = 0;
            int charPoolsLen = charPools.Length;
            int[] charPoolFlags = new int[charPoolsLen];
            char[] tempCharPool = null;
            for (int i = 0; i < charPoolsLen; i++)
            {
                tempCharPool = charPools[i];
                if (tempCharPool == null || tempCharPool.Length < 1)
                {
                    throw new ArgumentException("charPools中包含空的字符池...");
                }
                charPoolFlags[i] = poolCharSum;
                poolCharSum += tempCharPool.Length;
            }

            StringBuilder stb = new StringBuilder();
            Random rand = new Random();
            int randomNum = 0;
            for (int i = 0; i < len; i++)
            {
                randomNum = rand.Next(poolCharSum);
                // 定位随机数字所在字符池
                for (int j = 0; j < charPoolsLen; j++)
                {
                    if (randomNum >= charPoolFlags[j] && ((j + 1) == charPoolsLen || randomNum < charPoolFlags[j + 1]))
                    {
                        tempCharPool = charPools[j];
                        randomNum -= charPoolFlags[j];
                    }
                }
                stb.Append(tempCharPool[randomNum]);
            }
            return stb.ToString();
        }

        /**
         * 随机生成指定长度的字符串，范围限于字母,数字
         */
        public static String random(int len)
        {
            return randomForMultiCharPool(len, StringX.BASE_NUMBERS, StringX.LOWER_CASE_ALPHABET,
                StringX.UPPER_CASE_ALPHABET);
        }

        /**
         * 随机生成指定长度的字符串，范围限于字母,数字,指定特殊字符
         */
        public static String random(int len, char[] otherChars)
        {
            return randomForMultiCharPool(len, StringX.BASE_NUMBERS, StringX.LOWER_CASE_ALPHABET,
                StringX.UPPER_CASE_ALPHABET, otherChars);
        }
    }
}
