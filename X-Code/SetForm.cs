﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using X_Code.service;

namespace X_Code
{
    /// <summary>
    /// 用户设置窗体,包含用户信息，用户密码修改等,设置窗体不显示最大化框，默认双击不最大化
    /// </summary>
    public partial class SetForm : DMSkin.Main
    {
        DataService ds = new DataService();
        private Owner setOwnerInfo = null;//用户信息
        private Owner cloneOwnerInfo = null;//用户信息Clone
        
        public String msg = "";
        public SetForm(Owner oldOwnerInfo)
        {
                
            InitializeComponent();
        }


        private void SetForm_Load(object sender, EventArgs e)
        {
           
            try
            {
                setOwnerInfo = ds.GetOwner();
            }
            catch(Exception ex)
            {
                DMSkin.MetroMessageBox.Show(this, "异常！", ex.Message, MessageBoxButtons.OK, MessageBoxIcon.Warning, 100);
                this.Close();
            }
            cloneOwnerInfo = setOwnerInfo.Clone() as Owner;//应该克隆一个实例，保存原始信息  

            //透明度
            this.Opacity = setOwnerInfo.FormOpacity;
            //窗口阴影
            this.DM_Shadow = setOwnerInfo.IsShowShadow;

            setTabControl.MouseWheel += new MouseEventHandler(one_MouseWheel);

           

            //初始化设置信息
            this.nameTextBox.Text = setOwnerInfo.Name;//用户名
            if (String.IsNullOrEmpty(setOwnerInfo.Desc))//用户说明
            {
                nameTextBox.Text = "N/A";
            }
            else
            {
                this.descTextBox.Text = setOwnerInfo.Desc;
            }
            nameShowToggle.Checked = setOwnerInfo.IsShowName;//用户名是否窗口显示
            itemsShowTypeComboBox.PromptText = setOwnerInfo.ItemsShowType == 0
                ? "简单模式"
                : "炒鸡模式(未实现)";//列表展示模式
            pwShowTypeComboBox.PromptText = setOwnerInfo.PwShowType == 0
                ? "按下弹起切换"
                : "点击切换";//密码明文查看方式
            quicklyCloseToggle.Checked = setOwnerInfo.IsQuicklyClose;//是否快速关闭
            formAnimateToggle.Checked = setOwnerInfo.IsShowAnimate;//是否开启窗口动画
            fromshadowToggle.Checked = setOwnerInfo.IsShowShadow;//是否开启窗口阴影
            //透明度
            int one = (int)(((setOwnerInfo.FormOpacity-0.5)/0.5) * 100);
            formOpacityBar.Value = one;
            if (setOwnerInfo.IsShowAnimate)
            {
                Animate.AnimateWindow(this.Handle, 50, Animate.AW_ACTIVATE + Animate.AW_CENTER);//向外翻
            }
            infoToolTip.SetToolTip(nameShowToggle, "窗口显示用户名:" + nameShowToggle.Checked);
            infoToolTip.SetToolTip(quicklyCloseToggle, "快速关闭显示窗体:" + quicklyCloseToggle.Checked);
            infoToolTip.SetToolTip(fromshadowToggle, "窗口阴影:" + nameShowToggle.Checked);
            infoToolTip.SetToolTip(formAnimateToggle, "窗口动画:" + formAnimateToggle.Checked);
            infoToolTip.SetToolTip(formOpacityBar, "窗口透明度:" + formOpacityBar.Value+"%");
        }

        private void one_MouseWheel(Object sender, MouseEventArgs e)
        {
            
            int x = setTabControl.SelectedIndex;
            x = e.Delta < 0 ? (x + 1) : (x - 1);
            if (x < 0)
            {
                x = 0;
            }
            if (x > this.setTabControl.TabPages.Count)
            {
                x = this.setTabControl.TabPages.Count;
            }
            setTabControl.SelectedIndex=x;
            //Console.WriteLine(e.Delta+""+x);
        }
        protected override void OnPaint(PaintEventArgs e)
        {
            Graphics g = e.Graphics;//获取绘制对象
            ///设置参数
            g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
            // Create pen.
            Pen pen = new Pen(Color.Gainsboro, 3);

            // Create points that define line.
            Point point1 = new Point(0, 55);
            Point point2 = new Point(500, 55);
            g.DrawLine(pen, point1, point2);

            Point point3 = new Point(102, 55);
            Point point4 = new Point(102, 265);
           // Pen xpen = new Pen(Color.FromArgb(45, 151, 222), 3);
            g.DrawLine(pen, point3, point4);

            Point point5 = new Point(600, 265);
            g.DrawLine(pen, point5, point4);
        }

        private void dmButtonClose1_Click(object sender, EventArgs e)
        {
           // this.Hide();
            this.Close();

        }
        private void yesButton_Click(object sender, EventArgs e)
        {
            if (doUpdateSet(false))
            {
                msg = "修改设置成功,重启生效！";
                this.Close();
            }
        }
        private void okButton_Click(object sender, EventArgs e)
        {
            doUpdateSet(true);
            setTabControl.Focus();
        }

        private bool doUpdateSet(bool showMsg)
        {
            //验证用户名是否验证通过
            if (nameFlag != true)
            {
                setTabControl.SelectedIndex = 0;
                nameTextBox.Focus();
                showInfo(MineInfo.危险, "用户名修改未通过！");
                return false;
            }
            //验证密码修改
            if (pwFlag != true)
            {
                if (string.IsNullOrEmpty(oldPwTextBox.Text) &&
                    string.IsNullOrEmpty(newPwTextBox.Text) &&
                    string.IsNullOrEmpty(againPwTextBox.Text))
                {

                    showInfo(MineInfo.危险, "密码修改未通过！");
                    setTabControl.SelectedIndex = 2;
                    return false;
                }
            }

            //更新信息
            try
            {
                if (ds.UpdateOwner(setOwnerInfo))
                {
                    if (showMsg) {
                        showInfo(MineInfo.成功, "更新设置成功,重启生效！");
                    }
                    //克隆一个新设置信息
                    cloneOwnerInfo = setOwnerInfo.Clone() as Owner;//应该克隆一个实例，保存原始信息  
                }
                else
                {
                    showInfo(MineInfo.危险, "更新设置失败！");
                    return false;
                }

            }
            catch (Exception ex)
            {
                showInfo(MineInfo.警告, "异常:" + ex.Message);
                return false;
            }

            return true;
        }



        private void SetForm_Click(object sender, EventArgs e)
        {
            setTabControl.Focus();
        }

        private void tabPage2_Click(object sender, EventArgs e)
        {
            this.setTabControl.Focus();
        }

        private void tabPage1_Click(object sender, EventArgs e)
        {
            this.setTabControl.Focus();
        }

        private void tabPage3_Click(object sender, EventArgs e)
        {
            this.setTabControl.Focus();
        }

        private void tabPage4_Click(object sender, EventArgs e)
        {
            this.setTabControl.Focus();
        }

        static int MAX_TIMES = 3; //计时器时间
        int nowTimes = MAX_TIMES;
        //计时器方法
        private void messageTimer_Tick(object sender, EventArgs e)
        {

            if (nowTimes > 1)
            {
                nowTimes--;//自减 
            }
            else
            {
                //隐藏
                nowTimes = MAX_TIMES;
                Animate.AnimateWindow(this.infoLabel.Handle, 100, Animate.AW_HIDE + Animate.AW_SLIDE + Animate.AW_HOR_NEGATIVE);//信息滑出
                this.Invalidate();//触发窗口重绘
                this.messageTimer.Stop();
            }
        }
        private void showInfo(Color infoColor, String msg)
        {
            this.messageTimer.Stop();
            infoLabel.BackColor = infoColor;
            infoLabel.Text = msg;
            infoLabel.Visible = false;//确保先前信息的消失            
            //滑出
            Animate.AnimateWindow(this.infoLabel.Handle, 100, Animate.AW_SLIDE + Animate.AW_HOR_POSITIVE);
            infoLabel.Visible = true;

            this.messageTimer.Start();//开始计时器
        }




        private bool nameFlag = true; 
        //设置用户名
        private void nameTextBox_Leave(object sender, EventArgs e)
        {
            String setName=nameTextBox.Text;
            //判断修改后用户名是否合法
            if (String.IsNullOrWhiteSpace(setName)||setName.Length>20)
            {
                showInfo(MineInfo.危险, "新用户名非法！");
                nameFlag = false;
            }
            else
            {
                nameFlag = true;
                setOwnerInfo.Name = setName;
            }
        }

        private void nameShowToggle_CheckedChanged(object sender, EventArgs e)
        {
            infoToolTip.SetToolTip(nameShowToggle, "窗口显示用户名:" + nameShowToggle.Checked);
            setOwnerInfo.IsShowName = nameShowToggle.Checked;
        }

        private void descTextBox_Leave(object sender, EventArgs e)
        {
            setOwnerInfo.Desc = descTextBox.Text;
        }

        private void itemsShowTypeComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            //setOwnerInfo.ItemsShowType = itemsShowTypeComboBox.SelectedIndex;            
        }

        private void pwShowTypeComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            setOwnerInfo.PwShowType = pwShowTypeComboBox.SelectedIndex;  
        }

        private void quicklyCloseToggle_CheckedChanged(object sender, EventArgs e)
        {
            setOwnerInfo.IsQuicklyClose = quicklyCloseToggle.Checked;
            infoToolTip.SetToolTip(quicklyCloseToggle, "快速关闭显示窗体:" + quicklyCloseToggle.Checked);
        }

        private void formAnimateToggle_CheckedChanged(object sender, EventArgs e)
        {
            infoToolTip.SetToolTip(formAnimateToggle, "窗口动画:" + formAnimateToggle.Checked);
            
            setOwnerInfo.IsShowAnimate = formAnimateToggle.Checked;
        }

        private void fromshadowToggle_CheckedChanged(object sender, EventArgs e)
        {
            infoToolTip.SetToolTip(fromshadowToggle, "窗口阴影:" + fromshadowToggle.Checked);
            setOwnerInfo.IsShowShadow = fromshadowToggle.Checked;
        }

        private void formOpacityBar_ValueChanged(object sender, EventArgs e)
        {
            this.Opacity = 0.5 + ((double)formOpacityBar.Value / 200);
            
            this.Invalidate();//窗口重绘
        }

        private void formOpacityBar_MouseEnter(object sender, EventArgs e)
        {
            infoToolTip.SetToolTip(formOpacityBar, "透明度:" + (int)(Opacity * 100) + "%");
        }

        private void formOpacityBar_Leave(object sender, EventArgs e)
        {  
            setOwnerInfo.FormOpacity = Opacity;   
        }

        private void reNewPwIcon_MouseDown(object sender, MouseEventArgs e)
        {
            if (setOwnerInfo.PwShowType == 0)
            { 
                reNewPwIcon.DM_Key = DMSkin.Controls.DM.DMIcon.DMIconKey.晴;
                againPwTextBox.UseSystemPasswordChar = false;
            }
        }

        private void reNewPwIcon_MouseUp(object sender, MouseEventArgs e)
        {
            if (setOwnerInfo.PwShowType == 0)
            {
                reNewPwIcon.DM_Key = DMSkin.Controls.DM.DMIcon.DMIconKey.雾;
                againPwTextBox.UseSystemPasswordChar = true;
                againPwTextBox.Focus();
            }

        }
        private void reNewPwIcon_Click(object sender, EventArgs e)
        {
            if (setOwnerInfo.PwShowType == 1)
            {
                if (reNewPwIcon.DM_Key == DMSkin.Controls.DM.DMIcon.DMIconKey.晴)
                {
                    reNewPwIcon.DM_Key = DMSkin.Controls.DM.DMIcon.DMIconKey.雾;
                    againPwTextBox.UseSystemPasswordChar = true;
                }
                else
                {
                    againPwTextBox.UseSystemPasswordChar = false;
                    reNewPwIcon.DM_Key = DMSkin.Controls.DM.DMIcon.DMIconKey.晴;
                }

                againPwTextBox.Focus();
            }
        }
        private void newPwIcon_MouseDown(object sender, MouseEventArgs e)
        {
            if (setOwnerInfo.PwShowType == 0)
            { 
                newPwIcon.DM_Key = DMSkin.Controls.DM.DMIcon.DMIconKey.晴;
                newPwTextBox.UseSystemPasswordChar = false;
            }
        }

        private void newPwIcon_MouseUp(object sender, MouseEventArgs e)
        {
            if (setOwnerInfo.PwShowType == 0)
            { 
                newPwIcon.DM_Key = DMSkin.Controls.DM.DMIcon.DMIconKey.雾;
                newPwTextBox.UseSystemPasswordChar = true;
                newPwTextBox.Focus();
            }
        }

        private void newPwIcon_Click(object sender, EventArgs e)
        {
            if (setOwnerInfo.PwShowType == 1)
            {
                if (newPwIcon.DM_Key == DMSkin.Controls.DM.DMIcon.DMIconKey.晴)
                {
                    newPwIcon.DM_Key = DMSkin.Controls.DM.DMIcon.DMIconKey.雾;
                    newPwTextBox.UseSystemPasswordChar = true;
                }
                else
                {
                    newPwTextBox.UseSystemPasswordChar = false;
                    newPwIcon.DM_Key = DMSkin.Controls.DM.DMIcon.DMIconKey.晴;
                }

                newPwTextBox.Focus();
            }
        }
        private void oldPwIcon_MouseDown(object sender, MouseEventArgs e)
        {
            if (setOwnerInfo.PwShowType == 0)
            {
                oldPwIcon.DM_Key = DMSkin.Controls.DM.DMIcon.DMIconKey.晴;
                oldPwTextBox.UseSystemPasswordChar = false;

            }
        }

        private void oldPwIcon_MouseUp(object sender, MouseEventArgs e)
        {
            if (setOwnerInfo.PwShowType == 0)
            { 
                oldPwIcon.DM_Key = DMSkin.Controls.DM.DMIcon.DMIconKey.雾;
                oldPwTextBox.UseSystemPasswordChar = true;
                oldPwTextBox.Focus();
            }
        }
        private void oldPwIcon_Click(object sender, EventArgs e)
        {
            if (setOwnerInfo.PwShowType == 1)
            {
                if (oldPwIcon.DM_Key == DMSkin.Controls.DM.DMIcon.DMIconKey.晴)
                {
                    oldPwIcon.DM_Key = DMSkin.Controls.DM.DMIcon.DMIconKey.雾;
                    oldPwTextBox.UseSystemPasswordChar = true;
                }
                else
                {
                    oldPwTextBox.UseSystemPasswordChar = false;
                    oldPwIcon.DM_Key = DMSkin.Controls.DM.DMIcon.DMIconKey.晴;
                }

                oldPwTextBox.Focus();
            }
        }

      
        private void oldPwTextBox_Leave(object sender, EventArgs e)
        {
            if (showPwInt != -1)
            {
                return;
            }
            //焦点离开验证旧密码
            if (!string.IsNullOrEmpty(oldPwTextBox.Text))
            {
                if (validateOldPw())
                {
                    showInfo(MineInfo.成功, "旧密码验证通过！");
                    newPwTextBox.Enabled = true;
                   
                }
                else
                {
                    newPwTextBox.Clear();
                    againPwTextBox.Clear();
                    newPwTextBox.Enabled = false;
                }
            }
            else
            {
                pwFlag = true;
            }
           
            
        }
    
        private void newPwTextBox_Enter(object sender, EventArgs e)
        {
            //验证旧密码是否通过
            if (!validateOldPw())
            {
                oldPwTextBox.Focus();//旧密码框获取焦点
                showInfo(MineInfo.危险, "旧密码输入错误！");
                newPwTextBox.Enabled = false;
            }
        }
        private void newPwTextBox_Leave(object sender, EventArgs e)
        {
            if (showPwInt != -1)
            {
                return;
            }
            
            //焦点离开验证新密码
            if (!string.IsNullOrEmpty(newPwTextBox.Text))
            {
                if (validateNewPw())
                {
                    againPwTextBox.Enabled = true;
                    showInfo(MineInfo.成功, "新密码验证通过！");
                   
                }
                else
                {
                    againPwTextBox.Enabled = false;
                }
            }

        }
        private void againPwTextBox_Enter(object sender, EventArgs e)
        {            
            //验证旧密码是否通过
            if (!validateOldPw())
            {               
                showInfo(MineInfo.危险, "旧密码输入错误！");
                againPwTextBox.Enabled = false;
                return;
            }
            
            //验证新密码是否通过
            if (!validateNewPw())
            {               
                showInfo(MineInfo.危险, "新密码输入非法！");
                againPwTextBox.Enabled = false;
                return;
            }
        }

        private void againPwTextBox_Leave(object sender, EventArgs e)
        {
            if (showPwInt != -1)
            {
                return;
            }
            if (ValidateUpdatePw())
            {
                setOwnerInfo.Password = EncryptUtils.getMineMD5(againPwTextBox.Text);
                pwFlag = true;
                showInfo(MineInfo.成功, "新密码验证全部通过！");
            }
           

           

        }
        private Boolean ValidateUpdatePw()
        {
            //验证旧密码是否正确
            
            //验证新密码是否合法
            //验证重输密码是否相同
            return validateOldPw() && validateNewPw() && validateReNewPw();
        }
        //验证旧密码是否正确
        private Boolean validateOldPw()
        {
            String pw = oldPwTextBox.Text;
            if (String.IsNullOrWhiteSpace(pw))
            {
                showInfo(MineInfo.警告, "旧密码格式非法！");
                return false;
            }
            if(!cloneOwnerInfo.Password.Equals(EncryptUtils.getMineMD5(pw))){
                showInfo(MineInfo.危险, "旧密码输入错误！");
                return false;
            }
            return true;
        }
        //验证新密码是否合法
        private Boolean validateNewPw()
        {
            String pw = newPwTextBox.Text;
            if (String.IsNullOrEmpty(pw))
            {
                showInfo(MineInfo.警告, "请输入新密码！");
                return false;
            }
            if (String.IsNullOrWhiteSpace(pw))
            {
                showInfo(MineInfo.警告, "新密码格式非法！");
                return false;
            }
            if (pw.Length < 3 || pw.Length > 20)
            {
                showInfo(MineInfo.危险, "新密码长度非法(3-20)！");
                return false;
            }
            if (cloneOwnerInfo.Password.Equals(EncryptUtils.getMineMD5(pw)))
            {
                showInfo(MineInfo.警告, "新密码不能和旧密码相同！");
                return false;
            }
            return true;
        }
        //验证重输密码是否相同
        private Boolean validateReNewPw()
        {
            String newPw = newPwTextBox.Text;
            String reNewPw = againPwTextBox.Text;
            if (String.IsNullOrEmpty(reNewPw))
            {
                showInfo(MineInfo.警告, "重输密码不能为空！");
                return false;
            }
            if (String.IsNullOrWhiteSpace(newPw))
            {
                showInfo(MineInfo.警告, "重输密码格式非法！");
                return false;
            }
            if (String.IsNullOrWhiteSpace(reNewPw))
            {
                return false;
            }
            if (!newPw.Equals(reNewPw))
            {
                showInfo(MineInfo.危险, "两次密码输入不一致！");
                return false;
            }
            return true;
        }

        private int showPwInt = -1;
        private void oldPwIcon_MouseEnter(object sender, EventArgs e)
        {
            showPwInt = 1;
        }

        private void oldPwIcon_MouseLeave(object sender, EventArgs e)
        {
            showPwInt = -1;
        }

        private void newPwIcon_MouseEnter(object sender, EventArgs e)
        {
            showPwInt = 2;
        }

        private void newPwIcon_MouseLeave(object sender, EventArgs e)
        {
            showPwInt = -1;
        }

        private void reNewPwIcon_MouseEnter(object sender, EventArgs e)
        {
            showPwInt = 3;
        }

        private void reNewPwIcon_MouseLeave(object sender, EventArgs e)
        {
            showPwInt = -1;
        }

        private bool pwFlag = true;
        private void oldPwTextBox_TextChanged(object sender, EventArgs e)
        {
            pwFlag = false;
        }

        private void newPwTextBox_TextChanged(object sender, EventArgs e)
        {
            pwFlag = false;
        }

        private void againPwTextBox_TextChanged(object sender, EventArgs e)
        {
            pwFlag = false;
        }

        private void label13_MouseEnter(object sender, EventArgs e)
        {
            if (pwFlag == false)
            {
                if (ValidateUpdatePw())
                {
                    pwFlag = true;
                }
            }
        }
        private bool showDefaultPW = false;
        private void label13_Click(object sender, EventArgs e)
        {
            if (showDefaultPW)
            {
                label13.Text = "点击查看初始密码";
                label13.ForeColor = Color.DarkGray;
                showDefaultPW = false;
            }
            else
            {
                label13.Text = "点击隐藏："+MineInfo.DEFAULT_PASSWORD;
                label13.ForeColor = Color.OrangeRed;
                showDefaultPW = true;
            }
        }

        


       


        
      
    }
}
