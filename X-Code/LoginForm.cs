﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using X_Code.service;
using X_Code.io;
using System.IO;

namespace X_Code
{

    public partial class LoginForm :
        //DMSkin.Main
      DMSkin.DMSkinForm
    {
        static int MAX_TIMES = 4; //计时器时间
        int nowTimes = MAX_TIMES;

        //定义信息延时
        Boolean isDelay = false;
        //信息颜色定义
        static Color SUCCESS_COLOR = Color.FromArgb(64, 190, 1);
        static Color INFO_COLOR = Color.Teal;
        static Color ERROR_COLOR = Color.FromArgb(209, 50, 46);

        DataService ds = new DataService();
        //owner 信息
        public Owner ownerInfo = null;

        private bool isInitLogin = false;//是否是初始化登录

        public LoginForm()
        {  
            //妈蛋,dll的事情放后面吧,搞一天没搞出来啊0.0
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            bool x = true;
            
            try
            {
                x=ds.GetOwner().IsShowAnimate;
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex);
            }
            if (x)
            {
                Animate.AnimateWindow(this.Handle, 300, Animate.AW_VER_POSITIVE);//滑动进入
            }
            infoTip.SetToolTip(opacityBar, "透明度:" + (int)(Opacity * 100) + "%");
        }
        private void LoginForm_Shown(object sender, EventArgs e)
        {
            //用户文件是否存在,不存在则初始化
            if (!File.Exists(@MineDataIO.GetDefaultFilePath()))
            {
                //弹出对话框
                DialogResult result = DMSkin.MetroMessageBox.Show(this, "尚未进行初始化,是否初始化? 0,0", "提示！", MessageBoxButtons.OKCancel, MessageBoxIcon.Question, 100);
                if (result == DialogResult.OK)
                {
                    ownerInfo = X_Code.Owner.CreatDefault();
                    if (ds.InitOwner(ownerInfo))
                    {
                        this.showInfo(Color.OrangeRed, "Initial", "初始化成功,请登录...", false);
                        this.isInitLogin = true;
                        //填充默认密码
                        userPW.Text = MineInfo.DEFAULT_PASSWORD;
                        showNameLabel();
                        userPW.Focus();//密码框获取焦点            
                    }
                    else
                    {
                        this.showInfo(Color.OrangeRed, "Initial", "信息初始化失败...", false);
                        this.Close();
                       // System.Environment.Exit(0);
                    }
                }
                else
                {
                    this.Close();
                    //System.Environment.Exit(0);
                }
                return;//不要再去文件获取一次了
            }

            //文件存在尝试获取用户信息
            try
            {
                ownerInfo = ds.GetOwner();
                //Console.WriteLine(ownerInfo.ToString());
                //Console.WriteLine(EncryptUtils.AES_CBC_Encrypt("12580", MineInfo.ENCRYPT_AES_CBC_PW_KEY, MineInfo.ENCRYPT_AES_CBC_PW_VI));
                //Console.WriteLine(EncryptUtils.AES_CBC_Decrypt("l7Iiiuw3cWIR1LICopMG9g==", "chinashanghaijky", "1234567887654321"));
            }
            catch(Exception ex)
            {
                ownerInfo = null;
                Console.WriteLine(ex);
                DMSkin.MetroMessageBox.Show(this, "用户信息已损坏！", "异常 @_@", MessageBoxButtons.OK, MessageBoxIcon.Warning, 100);
                this.Close();
            }
            
            if (ownerInfo!=null)//判断data文件是否存在且已经初始化
            {
                this.showInfo(SUCCESS_COLOR, "Hello!", "请先进行登录 (^_^)", false); //感觉不延时更好一点唉  

                //userPW.Text = "PeiXiaXia";
                userPW.Focus();//密码框获取焦点
                //透明度
                this.Opacity = ownerInfo.FormOpacity;

                opacityBar.Value = (int)(((ownerInfo.FormOpacity - 0.5) / 0.5) * 100);
                infoTip.SetToolTip(opacityBar, "透明度:" + (int)(Opacity * 100) + "%");
                //窗口阴影
                this.DM_Shadow=ownerInfo.IsShowShadow;
                //设置用户名
                showNameLabel();
            }
            else
            {
                if (ds.GetCodeInfo() != null)
                {
                    DMSkin.MetroMessageBox.Show(this, "未知错误 @_@", "错误！", MessageBoxButtons.OK, MessageBoxIcon.Error, 100);
                    this.Close();
                }
                else//文件存在但仍然无法获取信息,有一次被捕获了
                {
                    DMSkin.MetroMessageBox.Show(this, "无法获取用户信息 @_@", "信息损坏！", MessageBoxButtons.OK, MessageBoxIcon.Error, 100);
                    this.Close();
                }
            }
           
        }
        private void opacityBar_MouseEnter(object sender, EventArgs e)
        {
            infoTip.SetToolTip(opacityBar, "透明度:" + (int)(Opacity * 100) + "%");
        }

        private void opacityBar_ValueChanged(object sender, EventArgs e)
        {
            this.Opacity = 0.5 + ((double)opacityBar.Value / 200);
            //userPW.Text = 0.5 + ((double)opacityBar.Value / 200) + "";
            this.Invalidate();//窗口重绘
        }

        private void userPW_TextChanged(object sender, EventArgs e)
        {
            if (userPW.Text.Length > 0)
            {
                lookPw.Visible = true;
            }
            else
            {
                lookPw.Visible = false;
            }
        }

        private void lookPw_MouseDown(object sender, MouseEventArgs e)
        {
            lookPw.DM_Text = "•,•";
            userPW.UseSystemPasswordChar = false;
            this.Invalidate();//触发窗口重绘
        }

        private void lookPw_MouseUp(object sender, MouseEventArgs e)
        {
            lookPw.DM_Text = "▪.▪";
            userPW.UseSystemPasswordChar = true;
            this.Invalidate();//触发窗口重绘

        }
        /// <summary>
        /// 清除输入
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void resetBtn_Click(object sender, EventArgs e)
        {
            //testRead();
            //Console.WriteLine(EncryptUtils.getMineMD5("peixiaxia"));
            userPW.Clear();
            userPW.Focus();//获取焦点
        }



        /// <summary>
        /// 登录验证
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void submitBtn_Click(object sender, EventArgs e)
        {
            //获取输入密码
            String pw = userPW.Text;
            if (pw == null || pw.Length < 1)
            {
                showInfo(INFO_COLOR, "Info!", "请先输入密码 (' ^ ')", false);
            }
            else
            {
                if (String.IsNullOrWhiteSpace(pw))
                {
                    showInfo(INFO_COLOR, "Error!", "空格密码无效 (' ^ ')", false);
                    return;
                }
                //获取输入密码md5值 并与正确md5值对比
                //String pwPro=
                if (ownerInfo.Password.Equals(EncryptUtils.getMineMD5(pw.Trim())))
                {
                    //showInfo(SUCCESS_COLOR, "Year!", "登录成功 (^‿^)", false);
                    //this.Close();//关闭登录窗口
                    this.Hide();
                    int LoginType = 1;
                    if (isInitLogin)
                    {
                        LoginType = 2;
                    }
                    
                    //开启主窗口
                    new ShowForm(this, LoginType).Show();
                }
                else
                {

                    showInfo(ERROR_COLOR, "Error!", "密码输入有误 (∪_∪)", false);
                }
            }

            //选中还是清除是个问题啊-_-?

            userPW.SelectAll();
            //userPW.Clear();   
            userPW.Focus();//获取焦点

        }

        private void dmButtonCloseLight1_Click(object sender, EventArgs e)
        {
            //infoPanel.Visible = false;
            Animate.AnimateWindow(this.infoPanel.Handle, 100, Animate.AW_HIDE + Animate.AW_SLIDE + Animate.AW_VER_NEGATIVE);//信息滑出
        }

        private void LoginForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (ownerInfo == null||ownerInfo.IsShowAnimate)
            {
                Animate.AnimateWindow(this.Handle, 300, Animate.AW_HIDE + Animate.AW_VER_NEGATIVE);//滑动退出
            }
        }

        private void infoTimer_Tick(object sender, EventArgs e)
        {
            if (isDelay && nowTimes == MAX_TIMES)
            {
                //滑出
                Animate.AnimateWindow(this.infoPanel.Handle, 100, Animate.AW_SLIDE + Animate.AW_VER_POSITIVE);
                infoPanel.Visible = true;
                isDelay = false;//默认不延时
                this.Invalidate();//触发窗口重绘
            }
            if (nowTimes > 1)
            {
                nowTimes--;//自减 
            }
            else
            {
                //隐藏
                nowTimes = MAX_TIMES;
                Animate.AnimateWindow(this.infoPanel.Handle, 100, Animate.AW_HIDE + Animate.AW_SLIDE + Animate.AW_VER_NEGATIVE);//信息滑出
                this.Invalidate();//触发窗口重绘
                this.infoTimer.Stop();
            }
           
        }

        private void showInfo(Color infoColor, String title, String msg, Boolean isDelay)
        {
            this.infoTimer.Stop();
            this.isDelay = isDelay; //设置全局的延时属性
            infoPanel.BackColor = infoColor;
            titleIconInfo.DM_Text = " " + title;
            //titleIconInfo.DM_Text = "" + title;
            infoLabel.Text = msg;
            infoPanel.Visible = false;//确保先前信息的消失
            if (!isDelay)
            {
                //滑出
                Animate.AnimateWindow(this.infoPanel.Handle, 100, Animate.AW_SLIDE + Animate.AW_VER_POSITIVE);
                infoPanel.Visible = true;
            }
            this.infoTimer.Start();//开始计时器

        }

        private Boolean checkData() {
            return false;
        }

        //选择是否显示用户名
        private void showNameLabel()
        {
            if (this.ownerInfo != null)
            {
                if (ownerInfo.IsShowName)//显示
                {
                   
                    nameLabel.Text = ownerInfo.Name;
                   
                    if (ownerInfo.IsShowAnimate)
                    {
                        Animate.AnimateWindow(this.dmIcon4.Handle, 100, Animate.AW_SLIDE + Animate.AW_VER_POSITIVE);
                        Animate.AnimateWindow(this.nameLabel.Handle, 200, Animate.AW_SLIDE + Animate.AW_VER_POSITIVE);
                    }
                   
                    nameLabel.Visible = true;
                    dmIcon4.Visible = true;
                    infoTip.SetToolTip(nameLabel, ownerInfo.Name);
                    infoTip.SetToolTip(dmIcon4, "用户名");
                }
                else
                {
                    nameLabel.Visible = false;
                    dmIcon4.Visible = false;
                }
            }
        }
    }
}
