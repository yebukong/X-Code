﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using X_Code.io;
using X_Code.service;
using System.IO;

namespace X_Code
{
    public partial class DescPWForm : DMSkin.Main
    {
        CodeItemService cis = new CodeItemService();
        
        private Owner ownerInfo = null;
        //默认文件信息
        private FileInfo fileInfo = null;
        public DescPWForm(Owner ownerInfo)
        {
            this.ownerInfo = ownerInfo;
            InitializeComponent();
        }

        private void DescForm_Load(object sender, EventArgs e)
        {
            fileInfo = null;
            String name = "N/A";
            int count = 0;
            try
            {
                //获取默认文件信息
                fileInfo=MineDataIO.GetDefaultFileInfo();
                //获取密码项数目
                List<CodeItem> x=cis.FindSomeByKey(null);
                count =    x == null ? 0 : x.Count;

                //用户名
                name=ownerInfo.Name;
            }
            catch
            {
                
            }
            if (fileInfo == null)
            {
                MessageBox.Show("发生异常。。。");
                this.Close();
            }

            this.userName.Text=name;
            this.fileName.Text = fileInfo.Name;
            //# 表示需要时格式化   0 表示任何时候格式化
            this.fileSize.Text = string.Format("{0:##,#.###}", ((double)(fileInfo.Length)) / (double)1024) + " KB";
            this.fileDate.Text = string.Format("{0:yyyy-MM-dd HH:mm}", fileInfo.CreationTime);
            this.itemCount.Text = count + " 项";

            infoToolTip.SetToolTip(fileName, fileInfo.FullName);
            infoToolTip.SetToolTip(fileSize, "大小："+string.Format("{0:#,##}",fileInfo.Length) + " B");
            infoToolTip.SetToolTip(fileDate, "创建时间："+string.Format("{0:yyyy-MM-dd HH:mm}", fileInfo.CreationTime)
                +"\n最后修改时间:"+string.Format("{0:yyyy-MM-dd HH:mm}", fileInfo.LastWriteTime));
            infoToolTip.SetToolTip(userName, "个人说明："+ownerInfo.Desc);
            infoToolTip.SetToolTip(itemCount, count+" 项");
            infoToolTip.SetToolTip(openPath, "打开："+fileInfo.DirectoryName);

            //透明度
            this.Opacity = ownerInfo.FormOpacity;
            //窗口阴影
            this.DM_Shadow = ownerInfo.IsShowShadow;
            if (ownerInfo.IsShowAnimate)
            {
                Animate.AnimateWindow(this.Handle, 50, Animate.AW_ACTIVATE + Animate.AW_CENTER);//向外翻
            }

            infoPanel.Focus();
        }
        protected override void OnPaint(PaintEventArgs e)
        {
            Graphics g = e.Graphics;//获取绘制对象
            ///设置参数
            g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
            // Create pen.
            Pen pen = new Pen(Color.LightSteelBlue, 3);

            // Create points that define line.
            Point point1 = new Point(0, 55);
            Point point2 = new Point(800, 55);
            g.DrawLine(pen, point1, point2);
        }

        private void dmButtonClose1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        //打开目录
        private void openPath_Click(object sender, EventArgs e)
        {
            try
            {
                System.Diagnostics.ProcessStartInfo info = new System.Diagnostics.ProcessStartInfo("explorer.exe");
                info.Arguments = "/e,/select," + fileInfo.FullName;
                System.Diagnostics.Process.Start(info);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                DMSkin.MetroMessageBox.Show(this, ex.Message, "打开目录失败！", MessageBoxButtons.OK, MessageBoxIcon.Error, 100);
               
            }

            this.infoPanel.Focus();
        }
    }
}
