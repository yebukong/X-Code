﻿namespace X_Code
{
    partial class LoginForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LoginForm));
            this.submitBtn = new DMSkin.Controls.DMButton();
            this.resetBtn = new DMSkin.Controls.DMButton();
            this.dmIcon1 = new DMSkin.Controls.DM.DMIcon();
            this.userPW = new DMSkin.Controls.DMTextBox();
            this.dmIcon2 = new DMSkin.Controls.DM.DMIcon();
            this.dmIcon3 = new DMSkin.Controls.DM.DMIcon();
            this.lookPw = new DMSkin.Controls.DM.DMIcon();
            this.opacityBar = new DMSkin.Metro.Controls.MetroTrackBar();
            this.infoTip = new DMSkin.Metro.Components.MetroToolTip();
            this.dmButtonCloseLight1 = new DMSkin.Controls.DMButtonCloseLight();
            this.dmIcon4 = new DMSkin.Controls.DM.DMIcon();
            this.infoLabel = new DMSkin.Metro.Controls.MetroLabel();
            this.titleIconInfo = new DMSkin.Controls.DM.DMIcon();
            this.infoPanel = new DMSkin.Metro.Controls.MetroPanel();
            this.infoTimer = new System.Windows.Forms.Timer(this.components);
            this.nameLabel = new System.Windows.Forms.Label();
            this.infoPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // submitBtn
            // 
            this.submitBtn.BackColor = System.Drawing.Color.Transparent;
            this.submitBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.submitBtn.DM_DisabledColor = System.Drawing.Color.Empty;
            this.submitBtn.DM_DownColor = System.Drawing.Color.FromArgb(((int)(((byte)(9)))), ((int)(((byte)(140)))), ((int)(((byte)(188)))));
            this.submitBtn.DM_MoveColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(195)))), ((int)(((byte)(245)))));
            this.submitBtn.DM_NormalColor = System.Drawing.Color.FromArgb(((int)(((byte)(9)))), ((int)(((byte)(163)))), ((int)(((byte)(220)))));
            this.submitBtn.DM_Radius = 5;
            this.submitBtn.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.submitBtn.ForeColor = System.Drawing.Color.White;
            this.submitBtn.Image = null;
            this.submitBtn.Location = new System.Drawing.Point(26, 211);
            this.submitBtn.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.submitBtn.Name = "submitBtn";
            this.submitBtn.Size = new System.Drawing.Size(145, 45);
            this.submitBtn.TabIndex = 0;
            this.submitBtn.Text = "登录";
            this.infoTip.SetToolTip(this.submitBtn, "登录");
            this.submitBtn.UseVisualStyleBackColor = false;
            this.submitBtn.Click += new System.EventHandler(this.submitBtn_Click);
            // 
            // resetBtn
            // 
            this.resetBtn.BackColor = System.Drawing.Color.Transparent;
            this.resetBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.resetBtn.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.resetBtn.DM_DisabledColor = System.Drawing.Color.Empty;
            this.resetBtn.DM_DownColor = System.Drawing.Color.Goldenrod;
            this.resetBtn.DM_MoveColor = System.Drawing.Color.Orange;
            this.resetBtn.DM_NormalColor = System.Drawing.Color.DarkOrange;
            this.resetBtn.DM_Radius = 5;
            this.resetBtn.Font = new System.Drawing.Font("新宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.resetBtn.ForeColor = System.Drawing.Color.White;
            this.resetBtn.Image = null;
            this.resetBtn.Location = new System.Drawing.Point(196, 211);
            this.resetBtn.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.resetBtn.Name = "resetBtn";
            this.resetBtn.Size = new System.Drawing.Size(145, 45);
            this.resetBtn.TabIndex = 1;
            this.resetBtn.Text = "清除";
            this.infoTip.SetToolTip(this.resetBtn, "清除");
            this.resetBtn.UseVisualStyleBackColor = false;
            this.resetBtn.Click += new System.EventHandler(this.resetBtn_Click);
            // 
            // dmIcon1
            // 
            this.dmIcon1.BackColor = System.Drawing.Color.Transparent;
            this.dmIcon1.DM_Color = System.Drawing.SystemColors.Highlight;
            this.dmIcon1.DM_Font_Size = 18F;
            this.dmIcon1.DM_Key = DMSkin.Controls.DM.DMIcon.DMIconKey.密码;
            this.dmIcon1.DM_Text = "密码：";
            this.dmIcon1.Location = new System.Drawing.Point(26, 128);
            this.dmIcon1.Name = "dmIcon1";
            this.dmIcon1.Size = new System.Drawing.Size(86, 29);
            this.dmIcon1.TabIndex = 3;
            this.dmIcon1.Text = "dmIcon1";
            // 
            // userPW
            // 
            this.userPW.AccessibleRole = System.Windows.Forms.AccessibleRole.Text;
            this.userPW.BackColor = System.Drawing.Color.WhiteSmoke;
            this.userPW.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.userPW.Font = new System.Drawing.Font("Consolas", 18F);
            this.userPW.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.userPW.ImeMode = System.Windows.Forms.ImeMode.On;
            this.userPW.Location = new System.Drawing.Point(117, 128);
            this.userPW.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.userPW.MaxLength = 18;
            this.userPW.Name = "userPW";
            this.userPW.Size = new System.Drawing.Size(216, 29);
            this.userPW.TabIndex = 0;
            this.userPW.Tag = "";
            this.infoTip.SetToolTip(this.userPW, "输入密码");
            this.userPW.UseSystemPasswordChar = true;
            this.userPW.WaterText = "";
            this.userPW.TextChanged += new System.EventHandler(this.userPW_TextChanged);
            // 
            // dmIcon2
            // 
            this.dmIcon2.BackColor = System.Drawing.Color.Transparent;
            this.dmIcon2.DM_Color = System.Drawing.Color.OrangeRed;
            this.dmIcon2.DM_Font_Size = 26F;
            this.dmIcon2.DM_Key = DMSkin.Controls.DM.DMIcon.DMIconKey.computer;
            this.dmIcon2.DM_Text = " X-Code";
            this.dmIcon2.Font = new System.Drawing.Font("隶书", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.dmIcon2.Location = new System.Drawing.Point(6, 35);
            this.dmIcon2.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.dmIcon2.Name = "dmIcon2";
            this.dmIcon2.Size = new System.Drawing.Size(358, 41);
            this.dmIcon2.TabIndex = 6;
            this.dmIcon2.Text = "dmIcon2";
            // 
            // dmIcon3
            // 
            this.dmIcon3.BackColor = System.Drawing.Color.Transparent;
            this.dmIcon3.DM_Color = System.Drawing.SystemColors.ScrollBar;
            this.dmIcon3.DM_Font_Size = 18F;
            this.dmIcon3.DM_Key = DMSkin.Controls.DM.DMIcon.DMIconKey._404;
            this.dmIcon3.DM_Text = "﹉﹉﹉﹉﹉﹉﹉﹉﹉﹉﹉﹉﹉﹉﹉﹉";
            this.dmIcon3.Location = new System.Drawing.Point(108, 160);
            this.dmIcon3.Name = "dmIcon3";
            this.dmIcon3.Size = new System.Drawing.Size(225, 11);
            this.dmIcon3.TabIndex = 7;
            this.dmIcon3.Text = "dmIcon3";
            // 
            // lookPw
            // 
            this.lookPw.BackColor = System.Drawing.Color.Transparent;
            this.lookPw.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lookPw.DM_Color = System.Drawing.Color.Red;
            this.lookPw.DM_Font_Size = 18F;
            this.lookPw.DM_Key = DMSkin.Controls.DM.DMIcon.DMIconKey.发布;
            this.lookPw.DM_Text = "▪.▪";
            this.lookPw.Location = new System.Drawing.Point(304, 128);
            this.lookPw.Name = "lookPw";
            this.lookPw.Size = new System.Drawing.Size(37, 29);
            this.lookPw.TabIndex = 8;
            this.lookPw.Text = "dmIcon4";
            this.infoTip.SetToolTip(this.lookPw, "按下查看输入密码");
            this.lookPw.Visible = false;
            this.lookPw.MouseDown += new System.Windows.Forms.MouseEventHandler(this.lookPw_MouseDown);
            this.lookPw.MouseUp += new System.Windows.Forms.MouseEventHandler(this.lookPw_MouseUp);
            // 
            // opacityBar
            // 
            this.opacityBar.BackColor = System.Drawing.Color.WhiteSmoke;
            this.opacityBar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.opacityBar.DM_UseCustomBackColor = true;
            this.opacityBar.ForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.opacityBar.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.opacityBar.LargeChange = 1;
            this.opacityBar.Location = new System.Drawing.Point(241, 14);
            this.opacityBar.MouseWheelBarPartitions = 100;
            this.opacityBar.Name = "opacityBar";
            this.opacityBar.Size = new System.Drawing.Size(58, 10);
            this.opacityBar.TabIndex = 9;
            this.opacityBar.Text = "metroTrackBar1";
            this.opacityBar.Theme = DMSkin.Metro.MetroThemeStyle.Light;
            this.infoTip.SetToolTip(this.opacityBar, "透明度调节");
            this.opacityBar.Value = 95;
            this.opacityBar.ValueChanged += new System.EventHandler(this.opacityBar_ValueChanged);
            this.opacityBar.MouseEnter += new System.EventHandler(this.opacityBar_MouseEnter);
            // 
            // infoTip
            // 
            this.infoTip.Style = DMSkin.Metro.MetroColorStyle.White;
            this.infoTip.StyleManager = null;
            this.infoTip.Theme = DMSkin.Metro.MetroThemeStyle.Light;
            // 
            // dmButtonCloseLight1
            // 
            this.dmButtonCloseLight1.BackColor = System.Drawing.Color.Transparent;
            this.dmButtonCloseLight1.Location = new System.Drawing.Point(333, 5);
            this.dmButtonCloseLight1.MaximumSize = new System.Drawing.Size(30, 27);
            this.dmButtonCloseLight1.MinimumSize = new System.Drawing.Size(30, 27);
            this.dmButtonCloseLight1.Name = "dmButtonCloseLight1";
            this.dmButtonCloseLight1.Size = new System.Drawing.Size(30, 27);
            this.dmButtonCloseLight1.TabIndex = 13;
            this.infoTip.SetToolTip(this.dmButtonCloseLight1, "清除");
            this.dmButtonCloseLight1.Click += new System.EventHandler(this.dmButtonCloseLight1_Click);
            // 
            // dmIcon4
            // 
            this.dmIcon4.BackColor = System.Drawing.Color.Transparent;
            this.dmIcon4.DM_Color = System.Drawing.Color.SteelBlue;
            this.dmIcon4.DM_Font_Size = 14F;
            this.dmIcon4.DM_Key = DMSkin.Controls.DM.DMIcon.DMIconKey.用户;
            this.dmIcon4.DM_Text = "";
            this.dmIcon4.Location = new System.Drawing.Point(199, 48);
            this.dmIcon4.Name = "dmIcon4";
            this.dmIcon4.Size = new System.Drawing.Size(22, 18);
            this.dmIcon4.TabIndex = 15;
            this.dmIcon4.Text = "dmIcon4";
            this.infoTip.SetToolTip(this.dmIcon4, "用户名");
            this.dmIcon4.Visible = false;
            // 
            // infoLabel
            // 
            this.infoLabel.BackColor = System.Drawing.Color.Transparent;
            this.infoLabel.DM_FontSize = DMSkin.Metro.MetroLabelSize.Tall;
            this.infoLabel.DM_FontWeight = DMSkin.Metro.MetroLabelWeight.Regular;
            this.infoLabel.DM_UseCustomBackColor = true;
            this.infoLabel.DM_UseCustomForeColor = true;
            this.infoLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.infoLabel.Location = new System.Drawing.Point(142, 0);
            this.infoLabel.Name = "infoLabel";
            this.infoLabel.Size = new System.Drawing.Size(191, 40);
            this.infoLabel.TabIndex = 11;
            this.infoLabel.Text = "请先输入密码 (˙＾˙)";
            this.infoLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.infoLabel.Theme = DMSkin.Metro.MetroThemeStyle.Light;
            // 
            // titleIconInfo
            // 
            this.titleIconInfo.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.titleIconInfo.BackColor = System.Drawing.Color.Transparent;
            this.titleIconInfo.DM_Color = System.Drawing.Color.WhiteSmoke;
            this.titleIconInfo.DM_Font_Size = 18F;
            this.titleIconInfo.DM_Key = DMSkin.Controls.DM.DMIcon.DMIconKey.安全;
            this.titleIconInfo.DM_Text = " Hello!";
            this.titleIconInfo.Location = new System.Drawing.Point(39, 7);
            this.titleIconInfo.Name = "titleIconInfo";
            this.titleIconInfo.Size = new System.Drawing.Size(97, 30);
            this.titleIconInfo.TabIndex = 12;
            this.titleIconInfo.Text = "dmIcon4";
            // 
            // infoPanel
            // 
            this.infoPanel.BackColor = System.Drawing.Color.Teal;
            this.infoPanel.Controls.Add(this.dmButtonCloseLight1);
            this.infoPanel.Controls.Add(this.titleIconInfo);
            this.infoPanel.Controls.Add(this.infoLabel);
            this.infoPanel.DM_HorizontalScrollbarBarColor = true;
            this.infoPanel.DM_HorizontalScrollbarDM_HighlightOnWheel = false;
            this.infoPanel.DM_HorizontalScrollbarSize = 10;
            this.infoPanel.DM_ThumbColor = System.Drawing.Color.DarkRed;
            this.infoPanel.DM_ThumbNormalColor = System.Drawing.Color.DarkRed;
            this.infoPanel.DM_UseBarColor = true;
            this.infoPanel.DM_UseCustomBackColor = true;
            this.infoPanel.DM_VerticalScrollbarBarColor = true;
            this.infoPanel.DM_VerticalScrollbarDM_HighlightOnWheel = false;
            this.infoPanel.DM_VerticalScrollbarSize = 10;
            this.infoPanel.Location = new System.Drawing.Point(0, 79);
            this.infoPanel.Name = "infoPanel";
            this.infoPanel.Size = new System.Drawing.Size(370, 40);
            this.infoPanel.TabIndex = 13;
            this.infoPanel.Visible = false;
            // 
            // infoTimer
            // 
            this.infoTimer.Interval = 1000;
            this.infoTimer.Tick += new System.EventHandler(this.infoTimer_Tick);
            // 
            // nameLabel
            // 
            this.nameLabel.AutoEllipsis = true;
            this.nameLabel.Font = new System.Drawing.Font("华文行楷", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.nameLabel.ForeColor = System.Drawing.Color.CornflowerBlue;
            this.nameLabel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.nameLabel.Location = new System.Drawing.Point(223, 50);
            this.nameLabel.Name = "nameLabel";
            this.nameLabel.Size = new System.Drawing.Size(128, 20);
            this.nameLabel.TabIndex = 14;
            this.nameLabel.Text = "叶小空";
            this.nameLabel.Visible = false;
            // 
            // LoginForm
            // 
            this.AcceptButton = this.submitBtn;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.CancelButton = this.resetBtn;
            this.ClientSize = new System.Drawing.Size(370, 280);
            this.Controls.Add(this.dmIcon4);
            this.Controls.Add(this.nameLabel);
            this.Controls.Add(this.infoPanel);
            this.Controls.Add(this.opacityBar);
            this.Controls.Add(this.lookPw);
            this.Controls.Add(this.dmIcon3);
            this.Controls.Add(this.dmIcon1);
            this.Controls.Add(this.resetBtn);
            this.Controls.Add(this.submitBtn);
            this.Controls.Add(this.userPW);
            this.Controls.Add(this.dmIcon2);
            this.DM_CanResize = false;
            this.DM_howBorder = false;
            this.DM_Radius = 5;
            this.DM_ShadowColor = System.Drawing.Color.LightPink;
            this.DM_ShadowWidth = 5;
            this.DM_SystemButtonThemeColor = DMSkin.DMSkinForm.DMColor.Dark;
            this.DM_SystemDrawLine = true;
            this.Font = new System.Drawing.Font("新宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.MaximumSize = new System.Drawing.Size(370, 280);
            this.MinimumSize = new System.Drawing.Size(370, 280);
            this.Name = "LoginForm";
            this.Opacity = 0.95D;
            this.Text = "登录窗口";
            this.TransparencyKey = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.LoginForm_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Shown += new System.EventHandler(this.LoginForm_Shown);
            this.infoPanel.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DMSkin.Controls.DMButton submitBtn;
        private DMSkin.Controls.DMButton resetBtn;
        private DMSkin.Controls.DM.DMIcon dmIcon1;
        private DMSkin.Controls.DMTextBox userPW;
        private DMSkin.Controls.DM.DMIcon dmIcon2;
        private DMSkin.Controls.DM.DMIcon dmIcon3;
        private DMSkin.Controls.DM.DMIcon lookPw;
        private DMSkin.Metro.Controls.MetroTrackBar opacityBar;
        private DMSkin.Metro.Components.MetroToolTip infoTip;
        private DMSkin.Metro.Controls.MetroLabel infoLabel;
        private DMSkin.Controls.DM.DMIcon titleIconInfo;
        private DMSkin.Metro.Controls.MetroPanel infoPanel;
        private DMSkin.Controls.DMButtonCloseLight dmButtonCloseLight1;
        private System.Windows.Forms.Timer infoTimer;
        private System.Windows.Forms.Label nameLabel;
        private DMSkin.Controls.DM.DMIcon dmIcon4;




    }
}

