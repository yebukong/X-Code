﻿using DMSkin.Metro.Controls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using X_Code.service;
using X_Code.src.utils;
using X_Code.src.vo;


namespace X_Code
{
    public partial class MakePWForm : DMSkin.Main
    {
        private DataService ds = null;
        private Owner ownerInfo = null;
        private MPWInfo mpwInfo = null;

        private MetroCheckBox[] charMetroCheckBoxs = null;

        private MetroRadioButton[] lenMetroRadioButtons = null;


        public MakePWForm(Owner ownerInfo)
        {
            this.ds = new DataService();
            this.ownerInfo = ownerInfo;
            mpwInfo = ds.GetMPWInfo();
            if (mpwInfo == null)
            {
                mpwInfo = MPWInfo.CreatDefault();
            }
            InitializeComponent();
        }

        private void MakePWForm_Load(object sender, EventArgs e)
        {
            //透明度
            this.Opacity = ownerInfo.FormOpacity;
            //窗口阴影
            this.DM_Shadow = ownerInfo.IsShowShadow;
            if (ownerInfo.IsShowAnimate)
            {
                Animate.AnimateWindow(this.Handle, 50, Animate.AW_ACTIVATE + Animate.AW_CENTER);//向外翻
            }
            charMetroCheckBoxs = new MetroCheckBox[] { this.capitalCheckBox, this.unCapitalCheckBox, this.numberCheckBox, this.otherCheckBox };
            lenMetroRadioButtons = new MetroRadioButton[] { this.len6RadioButton, this.len12RadioButton, this.len16RadioButton, this.otherRadioButton };
            //选项展示
            showMPWInfo();
        }

        private void dmButtonClose1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            mpwInfo = MPWInfo.CreatDefault();
            showMPWInfo();
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            Graphics g = e.Graphics;//获取绘制对象
            ///设置参数
            g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
            // Create pen.
            Pen pen = new Pen(Color.Peru, 3);

            // Create points that define line.
            Point point1 = new Point(0, 55);
            Point point2 = new Point(800, 55);
            g.DrawLine(pen, point1, point2);
        }

        private void accountIcon_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrWhiteSpace(targetTextBox.Text))
            {
                targetIcon.DM_Color = Color.OrangeRed;
                Clipboard.SetData(DataFormats.Text, targetTextBox.Text);//复制指定文本到剪贴板
                targetTextBox.SelectAll();
            }

        }

        private void targetIcon_MouseEnter(object sender, EventArgs e)
        {
            targetIcon.DM_Color = Color.Coral;
        }

        private void targetIcon_MouseLeave(object sender, EventArgs e)
        {
            targetIcon.DM_Color = Color.OrangeRed;
        }

        private void targetIcon_MouseDown(object sender, MouseEventArgs e)
        {
            targetIcon.DM_Color = Color.LightSalmon;
        }
        private void targetTextBox_TextChanged(object sender, EventArgs e)
        {
            if (!String.IsNullOrWhiteSpace(targetTextBox.Text))
            {
                targetIcon.DM_Color = Color.OrangeRed;
                targetIcon.Cursor = Cursors.Hand;
            }
        }

        private void makeButton_Click(object sender, EventArgs e)
        {
            syncMPWInfo();
            showMPWInfo();

            List<Char[]> mList = new List<Char[]>();
            if(capitalCheckBox.CheckState==CheckState.Checked){
                mList.Add(StringX.UPPER_CASE_ALPHABET);
            }
            if(unCapitalCheckBox.CheckState==CheckState.Checked){
                mList.Add(StringX.LOWER_CASE_ALPHABET);
            }
            if(numberCheckBox.CheckState==CheckState.Checked){
                mList.Add(StringX.BASE_NUMBERS);
            }
            if (otherCheckBox.CheckState == CheckState.Checked)
            {
                mList.Add(mpwInfo.OtherChars.ToCharArray());
            }
            int len = 0;
            try
            {
                for (int i = 0; i < mpwInfo.LenRadios.Length; i++)
                {
                    if (mpwInfo.LenRadios[i] == 1)
                    {
                        if ("其他".Equals(lenMetroRadioButtons[i].Text))
                        {
                            len = mpwInfo.OtherLen;
                        }
                        else {
                            Regex r = new Regex("[^0-9]");
                            len =  Int16.Parse(r.Replace(lenMetroRadioButtons[i].Text, ""));
                        }
                        break;
                    }
                }
            }
            catch (Exception)
            {                
                len = 12;
            }
            targetTextBox.Text = StringX.randomForMultiCharPool(len,mList.ToArray());
        }


        private void showMPWInfo()
        {
            otherCharsTextBox.Text = mpwInfo.OtherChars;
            otherLenTextBox.Text = mpwInfo.OtherLen + "";
            for (int i = 0; i < mpwInfo.CharChecks.Length; i++)
            {
                if (mpwInfo.CharChecks[i] == 0)
                {
                    charMetroCheckBoxs[i].CheckState = CheckState.Unchecked;
                }
                else
                {
                    charMetroCheckBoxs[i].CheckState = CheckState.Checked;
                }
            }
            for (int i = 0; i < mpwInfo.LenRadios.Length; i++)
            {
                if (mpwInfo.LenRadios[i] == 0)
                {
                    lenMetroRadioButtons[i].Checked = false;
                }
                else
                {
                    lenMetroRadioButtons[i].Checked = true;
                }
            }
        }

        private void syncMPWInfo()
        {
           
            Regex r = new Regex("[a-zA-Z0-9]");
            String temp = r.Replace(otherCharsTextBox.Text, "");
            HashSet<Char> set = new HashSet<Char>(temp);
            temp = "";
            foreach (Char x in set) {
                temp += x;
            }
            mpwInfo.OtherChars = temp;
            try
            {
                mpwInfo.OtherLen = Int16.Parse(otherLenTextBox.Text);
            }
            catch (Exception)
            {
                mpwInfo.OtherLen = 8;
            }
            for (int i = 0; i < charMetroCheckBoxs.Length; i++)
            {
                if (charMetroCheckBoxs[i].CheckState == CheckState.Checked)
                {
                    mpwInfo.CharChecks[i] = 1;
                }
                else
                {
                    mpwInfo.CharChecks[i] = 0;
                }
            }
            for (int i = 0; i < lenMetroRadioButtons.Length; i++)
            {
                if (lenMetroRadioButtons[i].Checked)
                {
                    mpwInfo.LenRadios[i] = 1;
                }
                else
                {
                    mpwInfo.LenRadios[i] = 0;
                }

            }
        }

        private void MakePWForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            syncMPWInfo();
            Thread vThread = new Thread(updateInfo);//异步操作
            vThread.Start(); //开始执行线程
        }

        private  void updateInfo() {
            ds.UpdateMPWInfo(mpwInfo);//选项保存到文件
        }

        private void resetButton_Click(object sender, EventArgs e)
        {
            mpwInfo = MPWInfo.CreatDefault();
            showMPWInfo();
        }
       

    }
}
