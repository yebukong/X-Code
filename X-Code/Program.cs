﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Reflection;
using System.IO;
using System.Diagnostics;


namespace X_Code
{
    static class Program 
    {
       
        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>
        [STAThread]
        static void Main()
        {
            ///防止多实例运行
            String moduleName = Process.GetCurrentProcess().MainModule.ModuleName; //获取当前活动进程模块名称
            String fileName = Path.GetFileNameWithoutExtension(moduleName);//返回指定模块的文件名
         //   Console.WriteLine(fileName + "|" + moduleName);
            Process[] myProcess = Process.GetProcessesByName(fileName);//根据文件名获取进程数组

            if (myProcess.Length > 1)
            {
                Form one = new TempForm();
                one.Show();
                DMSkin.MetroMessageBox.Show(one, "X-Code已经在运行 0,0", "错误！", MessageBoxButtons.OK, MessageBoxIcon.Error, 100);
               //MessageBox.Show("X-Code已经在运行0.0");
               one.Close();
            }
            else 
            {
               
                //路径测试
                //Console.WriteLine(1 + Process.GetCurrentProcess().MainModule.FileName);//获取模块的完整路径。
                //Console.WriteLine(2 + Environment.CurrentDirectory);// 获取和设置当前目录(该进程从中启动的目录)的完全限定目录
                //Console.WriteLine(3 + Directory.GetCurrentDirectory());//获取应用程序的当前工作目录。这个不一定是程序从中启动的目录啊，有可能程序放在C:\www 里,这个函数有可能返回C:\Documents
                //Console.WriteLine(4 + AppDomain.CurrentDomain.BaseDirectory);//获取程序的基目录。
                //Console.WriteLine(5 + AppDomain.CurrentDomain.SetupInformation.ApplicationBase);//获取和设置包括该应用程序的目录的名称。
                //Console.WriteLine(6 + Application.StartupPath);//获取启动了应用程序的可执行文件的路径。效果和2、5 一样。只是5 返回的字符串后面多了一个"\"而已
                //Console.WriteLine(7 + Application.ExecutablePath);//获取启动了应用程序的可执行文件的路径及文件名，效果和1 一样,可能不太一样

                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new LoginForm());
            }
           
        }
      
    }
}
