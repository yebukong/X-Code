﻿namespace X_Code
{
    partial class AddUpdateForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.closeButton = new DMSkin.Controls.DMButtonClose();
            this.addTitle = new DMSkin.Controls.DM.DMIcon();
            this.addButton = new DMSkin.Controls.DMButton();
            this.dmIcon5 = new DMSkin.Controls.DM.DMIcon();
            this.dmIcon7 = new DMSkin.Controls.DM.DMIcon();
            this.dmIcon8 = new DMSkin.Controls.DM.DMIcon();
            this.dmIcon9 = new DMSkin.Controls.DM.DMIcon();
            this.dmIcon1 = new DMSkin.Controls.DM.DMIcon();
            this.infoTip = new System.Windows.Forms.ToolTip(this.components);
            this.keyTextBox = new DMSkin.Metro.Controls.MetroTextBox();
            this.accountTextBox = new DMSkin.Metro.Controls.MetroTextBox();
            this.passwordTextBox = new DMSkin.Metro.Controls.MetroTextBox();
            this.pathTextBox = new DMSkin.Metro.Controls.MetroTextBox();
            this.descTextBox = new DMSkin.Metro.Controls.MetroTextBox();
            this.openButton = new DMSkin.Controls.DMButton();
            this.hideButton = new DMSkin.Controls.DMButton();
            this.infoLabel = new System.Windows.Forms.Label();
            this.messageTimer = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();
            // 
            // closeButton
            // 
            this.closeButton.BackColor = System.Drawing.Color.Transparent;
            this.closeButton.Dock = System.Windows.Forms.DockStyle.Right;
            this.closeButton.Location = new System.Drawing.Point(208, 4);
            this.closeButton.MaximumSize = new System.Drawing.Size(30, 27);
            this.closeButton.MinimumSize = new System.Drawing.Size(30, 27);
            this.closeButton.Name = "closeButton";
            this.closeButton.Size = new System.Drawing.Size(30, 27);
            this.closeButton.TabIndex = 6;
            this.closeButton.TabStop = false;
            this.infoTip.SetToolTip(this.closeButton, "取消");
            this.closeButton.Click += new System.EventHandler(this.closeButton_Click);
            // 
            // addTitle
            // 
            this.addTitle.BackColor = System.Drawing.Color.Transparent;
            this.addTitle.DM_Color = System.Drawing.Color.OrangeRed;
            this.addTitle.DM_Font_Size = 18F;
            this.addTitle.DM_Key = DMSkin.Controls.DM.DMIcon.DMIconKey.add;
            this.addTitle.DM_Text = " 新增项";
            this.addTitle.ForeColor = System.Drawing.Color.MediumSeaGreen;
            this.addTitle.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.addTitle.Location = new System.Drawing.Point(8, 22);
            this.addTitle.Name = "addTitle";
            this.addTitle.Size = new System.Drawing.Size(149, 28);
            this.addTitle.TabIndex = 100;
            this.addTitle.TabStop = false;
            this.infoTip.SetToolTip(this.addTitle, "Title");
            // 
            // addButton
            // 
            this.addButton.BackColor = System.Drawing.Color.Transparent;
            this.addButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.addButton.DialogResult = System.Windows.Forms.DialogResult.Yes;
            this.addButton.DM_DisabledColor = System.Drawing.Color.DarkSalmon;
            this.addButton.DM_DownColor = System.Drawing.Color.IndianRed;
            this.addButton.DM_MoveColor = System.Drawing.Color.Tomato;
            this.addButton.DM_NormalColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.addButton.DM_Radius = 5;
            this.addButton.Enabled = false;
            this.addButton.Font = new System.Drawing.Font("宋体", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.addButton.Image = null;
            this.addButton.Location = new System.Drawing.Point(13, 287);
            this.addButton.Margin = new System.Windows.Forms.Padding(5);
            this.addButton.Name = "addButton";
            this.addButton.Size = new System.Drawing.Size(213, 44);
            this.addButton.TabIndex = 5;
            this.addButton.Text = "确认添加";
            this.infoTip.SetToolTip(this.addButton, "确认");
            this.addButton.UseVisualStyleBackColor = false;
            this.addButton.Click += new System.EventHandler(this.addButton_Click);
            // 
            // dmIcon5
            // 
            this.dmIcon5.BackColor = System.Drawing.Color.Transparent;
            this.dmIcon5.DM_Color = System.Drawing.Color.SandyBrown;
            this.dmIcon5.DM_Font_Size = 12F;
            this.dmIcon5.DM_Key = DMSkin.Controls.DM.DMIcon.DMIconKey.安全;
            this.dmIcon5.DM_Text = "";
            this.dmIcon5.Location = new System.Drawing.Point(13, 148);
            this.dmIcon5.Name = "dmIcon5";
            this.dmIcon5.Size = new System.Drawing.Size(27, 24);
            this.dmIcon5.TabIndex = 8;
            this.dmIcon5.TabStop = false;
            this.dmIcon5.Text = "dmIcon5";
            this.infoTip.SetToolTip(this.dmIcon5, "密码");
            // 
            // dmIcon7
            // 
            this.dmIcon7.BackColor = System.Drawing.Color.Transparent;
            this.dmIcon7.DM_Color = System.Drawing.Color.SandyBrown;
            this.dmIcon7.DM_Font_Size = 12F;
            this.dmIcon7.DM_Key = DMSkin.Controls.DM.DMIcon.DMIconKey.账户;
            this.dmIcon7.DM_Text = "";
            this.dmIcon7.Location = new System.Drawing.Point(13, 109);
            this.dmIcon7.Name = "dmIcon7";
            this.dmIcon7.Size = new System.Drawing.Size(29, 23);
            this.dmIcon7.TabIndex = 10;
            this.dmIcon7.TabStop = false;
            this.dmIcon7.Text = "dmIcon7";
            this.infoTip.SetToolTip(this.dmIcon7, "账户");
            // 
            // dmIcon8
            // 
            this.dmIcon8.BackColor = System.Drawing.Color.Transparent;
            this.dmIcon8.DM_Color = System.Drawing.Color.DarkOrange;
            this.dmIcon8.DM_Font_Size = 12F;
            this.dmIcon8.DM_Key = DMSkin.Controls.DM.DMIcon.DMIconKey.更多;
            this.dmIcon8.DM_Text = "";
            this.dmIcon8.Location = new System.Drawing.Point(14, 242);
            this.dmIcon8.Name = "dmIcon8";
            this.dmIcon8.Size = new System.Drawing.Size(23, 23);
            this.dmIcon8.TabIndex = 11;
            this.dmIcon8.TabStop = false;
            this.dmIcon8.Text = "dmIcon8";
            this.infoTip.SetToolTip(this.dmIcon8, "更多");
            // 
            // dmIcon9
            // 
            this.dmIcon9.BackColor = System.Drawing.Color.Transparent;
            this.dmIcon9.DM_Color = System.Drawing.Color.DarkOrange;
            this.dmIcon9.DM_Font_Size = 12F;
            this.dmIcon9.DM_Key = DMSkin.Controls.DM.DMIcon.DMIconKey.收藏;
            this.dmIcon9.DM_Text = "";
            this.dmIcon9.Location = new System.Drawing.Point(13, 70);
            this.dmIcon9.Name = "dmIcon9";
            this.dmIcon9.Size = new System.Drawing.Size(27, 23);
            this.dmIcon9.TabIndex = 12;
            this.dmIcon9.TabStop = false;
            this.dmIcon9.Text = "dmIcon9";
            this.infoTip.SetToolTip(this.dmIcon9, "标题");
            // 
            // dmIcon1
            // 
            this.dmIcon1.BackColor = System.Drawing.Color.Transparent;
            this.dmIcon1.DM_Color = System.Drawing.Color.SandyBrown;
            this.dmIcon1.DM_Font_Size = 12F;
            this.dmIcon1.DM_Key = DMSkin.Controls.DM.DMIcon.DMIconKey.位置;
            this.dmIcon1.DM_Text = "";
            this.dmIcon1.Location = new System.Drawing.Point(14, 187);
            this.dmIcon1.Name = "dmIcon1";
            this.dmIcon1.Size = new System.Drawing.Size(26, 26);
            this.dmIcon1.TabIndex = 4;
            this.dmIcon1.TabStop = false;
            this.dmIcon1.Text = "dmIcon1";
            this.infoTip.SetToolTip(this.dmIcon1, "访问路径");
            // 
            // keyTextBox
            // 
            this.keyTextBox.BackColor = System.Drawing.Color.White;
            this.keyTextBox.DisplayIcon = false;
            this.keyTextBox.DM_UseCustomBackColor = true;
            this.keyTextBox.DM_UseSelectable = true;
            this.keyTextBox.Lines = new string[0];
            this.keyTextBox.Location = new System.Drawing.Point(41, 70);
            this.keyTextBox.MaxLength = 32767;
            this.keyTextBox.Name = "keyTextBox";
            this.keyTextBox.PasswordChar = '\0';
            this.keyTextBox.PromptText = "标题(必填+唯一)";
            this.keyTextBox.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.keyTextBox.SelectedText = "";
            this.keyTextBox.Size = new System.Drawing.Size(180, 23);
            this.keyTextBox.TabIndex = 0;
            this.infoTip.SetToolTip(this.keyTextBox, "此密码项的唯一标题");
            this.keyTextBox.Enter += new System.EventHandler(this.keyTextBox_Enter);
            this.keyTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.keyTextBox_KeyPress);
            this.keyTextBox.Leave += new System.EventHandler(this.keyTextBox_Leave);
            // 
            // accountTextBox
            // 
            this.accountTextBox.BackColor = System.Drawing.Color.White;
            this.accountTextBox.DM_UseCustomBackColor = true;
            this.accountTextBox.DM_UseSelectable = true;
            this.accountTextBox.Lines = new string[0];
            this.accountTextBox.Location = new System.Drawing.Point(41, 109);
            this.accountTextBox.MaxLength = 32767;
            this.accountTextBox.Name = "accountTextBox";
            this.accountTextBox.PasswordChar = '\0';
            this.accountTextBox.PromptText = "账号";
            this.accountTextBox.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.accountTextBox.SelectedText = "";
            this.accountTextBox.Size = new System.Drawing.Size(180, 23);
            this.accountTextBox.TabIndex = 1;
            this.infoTip.SetToolTip(this.accountTextBox, "输入账号,邮箱等");
            // 
            // passwordTextBox
            // 
            this.passwordTextBox.BackColor = System.Drawing.Color.White;
            this.passwordTextBox.DM_UseCustomBackColor = true;
            this.passwordTextBox.DM_UseSelectable = true;
            this.passwordTextBox.Lines = new string[0];
            this.passwordTextBox.Location = new System.Drawing.Point(41, 149);
            this.passwordTextBox.MaxLength = 32767;
            this.passwordTextBox.Name = "passwordTextBox";
            this.passwordTextBox.PasswordChar = '\0';
            this.passwordTextBox.PromptText = "密码";
            this.passwordTextBox.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.passwordTextBox.SelectedText = "";
            this.passwordTextBox.Size = new System.Drawing.Size(180, 23);
            this.passwordTextBox.TabIndex = 2;
            this.infoTip.SetToolTip(this.passwordTextBox, "输入密码");
            // 
            // pathTextBox
            // 
            this.pathTextBox.BackColor = System.Drawing.Color.White;
            this.pathTextBox.DM_UseCustomBackColor = true;
            this.pathTextBox.DM_UseSelectable = true;
            this.pathTextBox.Lines = new string[0];
            this.pathTextBox.Location = new System.Drawing.Point(41, 187);
            this.pathTextBox.MaxLength = 32767;
            this.pathTextBox.Name = "pathTextBox";
            this.pathTextBox.PasswordChar = '\0';
            this.pathTextBox.PromptText = "访问路径";
            this.pathTextBox.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.pathTextBox.SelectedText = "";
            this.pathTextBox.Size = new System.Drawing.Size(136, 23);
            this.pathTextBox.TabIndex = 3;
            this.infoTip.SetToolTip(this.pathTextBox, "输入相关程序路径或网站");
            // 
            // descTextBox
            // 
            this.descTextBox.BackColor = System.Drawing.Color.White;
            this.descTextBox.DM_UseCustomBackColor = true;
            this.descTextBox.DM_UseSelectable = true;
            this.descTextBox.Lines = new string[0];
            this.descTextBox.Location = new System.Drawing.Point(41, 226);
            this.descTextBox.MaxLength = 32767;
            this.descTextBox.Multiline = true;
            this.descTextBox.Name = "descTextBox";
            this.descTextBox.PasswordChar = '\0';
            this.descTextBox.PromptText = "附加信息";
            this.descTextBox.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.descTextBox.SelectedText = "";
            this.descTextBox.Size = new System.Drawing.Size(180, 53);
            this.descTextBox.TabIndex = 4;
            this.infoTip.SetToolTip(this.descTextBox, "输入更多相关信息");
            // 
            // openButton
            // 
            this.openButton.BackColor = System.Drawing.Color.Transparent;
            this.openButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.openButton.DM_DisabledColor = System.Drawing.Color.Silver;
            this.openButton.DM_DownColor = System.Drawing.Color.Tan;
            this.openButton.DM_MoveColor = System.Drawing.Color.Wheat;
            this.openButton.DM_NormalColor = System.Drawing.Color.Bisque;
            this.openButton.DM_Radius = 1;
            this.openButton.Enabled = false;
            this.openButton.FlatAppearance.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.openButton.FlatAppearance.BorderSize = 0;
            this.openButton.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.openButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.openButton.Image = null;
            this.openButton.Location = new System.Drawing.Point(178, 185);
            this.openButton.Margin = new System.Windows.Forms.Padding(0);
            this.openButton.Name = "openButton";
            this.openButton.Size = new System.Drawing.Size(45, 26);
            this.openButton.TabIndex = 102;
            this.openButton.Text = "选择";
            this.infoTip.SetToolTip(this.openButton, "选择一个本地程序");
            this.openButton.UseVisualStyleBackColor = false;
            this.openButton.Click += new System.EventHandler(this.openButton_Click);
            // 
            // hideButton
            // 
            this.hideButton.BackColor = System.Drawing.Color.Transparent;
            this.hideButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.hideButton.DM_DisabledColor = System.Drawing.Color.Empty;
            this.hideButton.DM_DownColor = System.Drawing.Color.FromArgb(((int)(((byte)(9)))), ((int)(((byte)(140)))), ((int)(((byte)(188)))));
            this.hideButton.DM_MoveColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(195)))), ((int)(((byte)(245)))));
            this.hideButton.DM_NormalColor = System.Drawing.Color.FromArgb(((int)(((byte)(9)))), ((int)(((byte)(163)))), ((int)(((byte)(220)))));
            this.hideButton.DM_Radius = 5;
            this.hideButton.Image = null;
            this.hideButton.Location = new System.Drawing.Point(216, 14);
            this.hideButton.Name = "hideButton";
            this.hideButton.Size = new System.Drawing.Size(10, 10);
            this.hideButton.TabIndex = 18;
            this.hideButton.TabStop = false;
            this.hideButton.Text = "X";
            this.hideButton.UseVisualStyleBackColor = false;
            this.hideButton.Click += new System.EventHandler(this.hideButton_Click);
            // 
            // infoLabel
            // 
            this.infoLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.infoLabel.AutoSize = true;
            this.infoLabel.BackColor = System.Drawing.Color.Salmon;
            this.infoLabel.Font = new System.Drawing.Font("幼圆", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.infoLabel.ForeColor = System.Drawing.Color.Ivory;
            this.infoLabel.Location = new System.Drawing.Point(41, 22);
            this.infoLabel.Margin = new System.Windows.Forms.Padding(3);
            this.infoLabel.Name = "infoLabel";
            this.infoLabel.Padding = new System.Windows.Forms.Padding(5, 5, 10, 5);
            this.infoLabel.Size = new System.Drawing.Size(78, 24);
            this.infoLabel.TabIndex = 101;
            this.infoLabel.Text = "提示信息";
            this.infoLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.infoLabel.Visible = false;
            // 
            // messageTimer
            // 
            this.messageTimer.Interval = 1000;
            this.messageTimer.Tick += new System.EventHandler(this.messageTimer_Tick);
            // 
            // AddUpdateForm
            // 
            this.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.CancelButton = this.hideButton;
            this.ClientSize = new System.Drawing.Size(242, 350);
            this.ControlBox = false;
            this.Controls.Add(this.openButton);
            this.Controls.Add(this.infoLabel);
            this.Controls.Add(this.descTextBox);
            this.Controls.Add(this.pathTextBox);
            this.Controls.Add(this.passwordTextBox);
            this.Controls.Add(this.accountTextBox);
            this.Controls.Add(this.keyTextBox);
            this.Controls.Add(this.dmIcon9);
            this.Controls.Add(this.dmIcon8);
            this.Controls.Add(this.dmIcon7);
            this.Controls.Add(this.dmIcon5);
            this.Controls.Add(this.dmIcon1);
            this.Controls.Add(this.addButton);
            this.Controls.Add(this.addTitle);
            this.Controls.Add(this.closeButton);
            this.Controls.Add(this.hideButton);
            this.DM_CanResize = false;
            this.DM_howBorder = false;
            this.DM_Radius = 3;
            this.DM_ShadowColor = System.Drawing.Color.Red;
            this.DM_ShadowWidth = 10;
            this.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AddUpdateForm";
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "添加项";
            this.TransparencyKey = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.Load += new System.EventHandler(this.AddUpdateForm_Load);
            this.Click += new System.EventHandler(this.AddUpdateForm_Click);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DMSkin.Controls.DMButtonClose closeButton;
        private DMSkin.Controls.DM.DMIcon addTitle;
        private DMSkin.Controls.DMButton addButton;
        private DMSkin.Controls.DM.DMIcon dmIcon5;
        private DMSkin.Controls.DM.DMIcon dmIcon7;
        private DMSkin.Controls.DM.DMIcon dmIcon8;
        private DMSkin.Controls.DM.DMIcon dmIcon9;
        private DMSkin.Controls.DM.DMIcon dmIcon1;
        private System.Windows.Forms.ToolTip infoTip;
        private DMSkin.Controls.DMButton hideButton;
        private DMSkin.Metro.Controls.MetroTextBox passwordTextBox;
        private DMSkin.Metro.Controls.MetroTextBox pathTextBox;
        private DMSkin.Metro.Controls.MetroTextBox descTextBox;
        private DMSkin.Metro.Controls.MetroTextBox keyTextBox;
        private DMSkin.Metro.Controls.MetroTextBox accountTextBox;
        private System.Windows.Forms.Label infoLabel;
        private System.Windows.Forms.Timer messageTimer;
        private DMSkin.Controls.DMButton openButton;
    }
}