﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DMSkin;
using System.IO;

namespace X_Code
{ 
    /// <summary>
    /// 展示密码项的窗体
    /// </summary>
    public partial class OneForm : DMSkin.Main
    {
        private CodeItem one = null;
        private Owner ownerInfo = null;
        private Boolean openPathIsFile = false;//要打开的路径为可执行文件
        public OneForm()
        {  
            InitializeComponent();
        }

        public OneForm(Owner ownerInfo,CodeItem one)
        {
            this.ownerInfo = ownerInfo;
            this.one = one;
            InitializeComponent();
        }

        private void OneForm_Load(object sender, EventArgs e)
        {
            
            if (one == null)
            {
                return;
            }
            if (String.IsNullOrWhiteSpace(one.Title))
            {
                throw new Exception("错误的参数");
            }
            title.Text = one.Title;
            infoToolTip.SetToolTip(title, one.Title);
            if (!String.IsNullOrWhiteSpace(one.Account))
            {
                accountTextBox.Text = one.Account;
                accountTextBox.Enabled = true;
                accountIcon.Enabled = true;
                //复制按钮颜色
                accountIcon.DM_Color = Color.OrangeRed;
            }
            if (!String.IsNullOrWhiteSpace(one.PassWord))
            {
                pwTextBox.Text = one.PassWord;
                pwTextBox.UseSystemPasswordChar = true;
                pwTextBox.Enabled = true;
                pwIcon.Enabled = true;
                showPwIcon.Enabled = true;
                //复制按钮,明文显示按钮颜色
                pwIcon.DM_Color = Color.OrangeRed;
                showPwIcon.DM_Color = Color.OrangeRed;
            }
            if (!String.IsNullOrWhiteSpace(one.OpenPath))
            {  
                openIcon.Enabled = true;
                //打开按钮颜色
                openIcon.DM_Color = Color.OrangeRed;
                //分析路径是否合法
                
                infoToolTip.SetToolTip(openIcon, "打开:"+one.OpenPath);
            }
            if (!String.IsNullOrWhiteSpace(one.Desc))
            {
                descTextBox.Text = one.Desc;
                descTextBox.Enabled = true;
            }
            //时间设置
            timeIcon.DM_Key = DMSkin.Controls.DM.DMIcon.DMIconKey.时间;
            timeIcon.DM_Text += "   "+string.Format("{0:yyyy-MM-dd HH:mm}", one.AddTime);
            infoToolTip.SetToolTip(timeIcon, "创建时间:" + string.Format("{0:yyyy-MM-dd HH:mm}", one.AddTime)+
                    "\n最后修改时间:" + string.Format("{0:yyyy-MM-dd HH:mm}", one.UpdateTime));
            //透明度
            this.Opacity = ownerInfo.FormOpacity;
            //窗口阴影
            this.DM_Shadow = ownerInfo.IsShowShadow;
            if (ownerInfo.IsShowAnimate)
            {
                Animate.AnimateWindow(this.Handle, 50, Animate.AW_ACTIVATE + Animate.AW_CENTER);//向外翻
            }
           
        }
        protected override void OnPaint(PaintEventArgs e)
        {
            Graphics g = e.Graphics;//获取绘制对象
            ///设置参数
            g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
            // Create pen.255, 192, 192
            Pen pen = new Pen(Color.MistyRose, 3);

            // Create points that define line.
            Point point1 = new Point(0, 55);
            Point point2 = new Point(500, 55);
            g.DrawLine(pen, point1, point2);
        }
        private void dmButtonClose1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void closeButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        //打开指定路径
        private void openIcon_Click(object sender, EventArgs e)
        {      
            try
            {
                System.Diagnostics.Process.Start(@one.OpenPath);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                DMSkin.MetroMessageBox.Show(this, ex.Message, "启动失败！", MessageBoxButtons.OK, MessageBoxIcon.Error, 100);
                //MessageBox.Show("程序启动失败！");
            }

            
      }
        //复制账户
        private void accountIcon_Click(object sender, EventArgs e)
        {
            accountIcon.DM_Color = Color.OrangeRed;
            Clipboard.SetData(DataFormats.Text, one.Account);//复制指定文本到剪贴板
        }
        //复制密码
        private void pwIcon_Click(object sender, EventArgs e)
        {
            pwIcon.DM_Color = Color.OrangeRed;
            Clipboard.SetData(DataFormats.Text, one.PassWord);
        }
        private void accountIcon_MouseEnter(object sender, EventArgs e)
        {
            accountIcon.DM_Color = Color.Coral;
        }

        private void accountIcon_MouseLeave(object sender, EventArgs e)
        {
            accountIcon.DM_Color = Color.OrangeRed;    
        }

        private void accountIcon_MouseDown(object sender, MouseEventArgs e)
        {
            accountIcon.DM_Color = Color.LightSalmon;
        }

        private void pwIcon_MouseDown(object sender, MouseEventArgs e)
        {
            pwIcon.DM_Color = Color.LightSalmon;
        }

        private void pwIcon_MouseEnter(object sender, EventArgs e)
        {
            pwIcon.DM_Color = Color.Coral;
        }

        private void pwIcon_MouseLeave(object sender, EventArgs e)
        {
            pwIcon.DM_Color = Color.OrangeRed;
        }

        private void showPwIcon_MouseDown(object sender, MouseEventArgs e)
        {
            if (ownerInfo.PwShowType == 0)
            {
                showPwIcon.DM_Key = DMSkin.Controls.DM.DMIcon.DMIconKey.晴;
                pwTextBox.UseSystemPasswordChar = false;

            }
        }

        private void showPwIcon_MouseUp(object sender, MouseEventArgs e)
        {
            if (ownerInfo.PwShowType == 0)
            {
                showPwIcon.DM_Key = DMSkin.Controls.DM.DMIcon.DMIconKey.雾;
                pwTextBox.UseSystemPasswordChar = true;
            }
           
        }
          private void showPwIcon_Click(object sender, EventArgs e)
        {
            if (ownerInfo.PwShowType == 1)
            {
                if (showPwIcon.DM_Key == DMSkin.Controls.DM.DMIcon.DMIconKey.晴)
                {
                    showPwIcon.DM_Key = DMSkin.Controls.DM.DMIcon.DMIconKey.雾;
                    pwTextBox.UseSystemPasswordChar = true;
                }
                else
                {
                    pwTextBox.UseSystemPasswordChar = false;
                    showPwIcon.DM_Key = DMSkin.Controls.DM.DMIcon.DMIconKey.晴;
                }


            }
        }
        private void openIcon_MouseDown(object sender, MouseEventArgs e)
        {
            openIcon.DM_Key = DMSkin.Controls.DM.DMIcon.DMIconKey.位置;
        }

        private void openIcon_MouseUp(object sender, MouseEventArgs e)
        {
            openIcon.DM_Key = DMSkin.Controls.DM.DMIcon.DMIconKey.地图;
        }

        private void accountTextBox_MouseDown(object sender, MouseEventArgs e)
        {
            Animate.HideCaret(((TextBox)sender).Handle);
        }

        private void pwTextBox_MouseDown(object sender, MouseEventArgs e)
        {
            Animate.HideCaret(((TextBox)sender).Handle);
        }

        private void descTextBox_MouseDown(object sender, MouseEventArgs e)
        {
            Animate.HideCaret(((TextBox)sender).Handle);
        }

      

        

      


       

        


       
    }
  
  
 
}
